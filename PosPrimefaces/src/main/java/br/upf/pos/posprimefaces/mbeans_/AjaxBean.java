/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.posprimefaces.mbeans_;

import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.faces.event.AjaxBehaviorEvent;
import javax.inject.Named;

/**
 *
 * @author pavan
 */
@Named
@RequestScoped
public class AjaxBean {
    private String nome;
    private int qtdChar;

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }    
    public void transformar() {
        this.nome = this.nome.toUpperCase();
        this.qtdChar = this.nome.length();
    }

    public int getQtdChar() {
        return qtdChar;
    }

    public void setQtdChar(int qtdChar) {
        this.qtdChar = qtdChar;
    }
    
    
}
