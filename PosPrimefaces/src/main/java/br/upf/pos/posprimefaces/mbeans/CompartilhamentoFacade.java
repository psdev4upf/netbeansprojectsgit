/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.posprimefaces.mbeans;

import br.upf.estacoes.model.beans.Compartilhamento;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Filipe
 */
@Stateless
public class CompartilhamentoFacade extends AbstractFacade<Compartilhamento> {
    @PersistenceContext(unitName = "PosPrimefacesPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CompartilhamentoFacade() {
        super(Compartilhamento.class);
    }
    
}
