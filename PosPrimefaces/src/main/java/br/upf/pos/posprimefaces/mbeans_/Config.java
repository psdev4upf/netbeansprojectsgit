/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.posprimefaces.mbeans_;

import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author pavan
 */
@Named
@Stateless
public class Config {
    private String theme="delta";
    
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }
    
}
