package br.upf.pos.posprimefaces.controller;

import br.upf.estacoes.model.beans.VariavelDaEstacao;
import br.upf.pos.posprimefaces.controller.util.JsfUtil;
import br.upf.pos.posprimefaces.controller.util.JsfUtil.PersistAction;
import br.upf.pos.posprimefaces.mbeans.VariavelDaEstacaoFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("variavelDaEstacaoController")
@SessionScoped
public class VariavelDaEstacaoController implements Serializable {

    @EJB
    private br.upf.pos.posprimefaces.mbeans.VariavelDaEstacaoFacade ejbFacade;
    private List<VariavelDaEstacao> items = null;
    private VariavelDaEstacao selected;

    public VariavelDaEstacaoController() {
    }

    public VariavelDaEstacao getSelected() {
        return selected;
    }

    public void setSelected(VariavelDaEstacao selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private VariavelDaEstacaoFacade getFacade() {
        return ejbFacade;
    }

    public VariavelDaEstacao prepareCreate() {
        selected = new VariavelDaEstacao();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("VariavelDaEstacaoCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("VariavelDaEstacaoUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("VariavelDaEstacaoDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<VariavelDaEstacao> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public VariavelDaEstacao getVariavelDaEstacao(java.lang.Long id) {
        return getFacade().find(id);
    }

    public List<VariavelDaEstacao> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<VariavelDaEstacao> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = VariavelDaEstacao.class)
    public static class VariavelDaEstacaoControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            VariavelDaEstacaoController controller = (VariavelDaEstacaoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "variavelDaEstacaoController");
            return controller.getVariavelDaEstacao(getKey(value));
        }

        java.lang.Long getKey(String value) {
            java.lang.Long key;
            key = Long.valueOf(value);
            return key;
        }

        String getStringKey(java.lang.Long value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof VariavelDaEstacao) {
                VariavelDaEstacao o = (VariavelDaEstacao) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), VariavelDaEstacao.class.getName()});
                return null;
            }
        }

    }

}
