package br.upf.pos.posprimefaces.controller;

import br.upf.estacoes.model.beans.ProgramaOperacao;
import br.upf.pos.posprimefaces.controller.util.JsfUtil;
import br.upf.pos.posprimefaces.controller.util.JsfUtil.PersistAction;
import br.upf.pos.posprimefaces.mbeans.ProgramaOperacaoFacade;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("programaOperacaoController")
@SessionScoped
public class ProgramaOperacaoController implements Serializable {

    @EJB
    private br.upf.pos.posprimefaces.mbeans.ProgramaOperacaoFacade ejbFacade;
    private List<ProgramaOperacao> items = null;
    private ProgramaOperacao selected;

    public ProgramaOperacaoController() {
    }

    public ProgramaOperacao getSelected() {
        return selected;
    }

    public void setSelected(ProgramaOperacao selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
    }

    protected void initializeEmbeddableKey() {
    }

    private ProgramaOperacaoFacade getFacade() {
        return ejbFacade;
    }

    public ProgramaOperacao prepareCreate() {
        selected = new ProgramaOperacao();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("ProgramaOperacaoCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("ProgramaOperacaoUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("ProgramaOperacaoDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<ProgramaOperacao> getItems() {
        if (items == null) {
            items = getFacade().findAll();
        }
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public ProgramaOperacao getProgramaOperacao(java.lang.String id) {
        return getFacade().find(id);
    }

    public List<ProgramaOperacao> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<ProgramaOperacao> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = ProgramaOperacao.class)
    public static class ProgramaOperacaoControllerConverter implements Converter {

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            ProgramaOperacaoController controller = (ProgramaOperacaoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "programaOperacaoController");
            return controller.getProgramaOperacao(getKey(value));
        }

        java.lang.String getKey(String value) {
            java.lang.String key;
            key = value;
            return key;
        }

        String getStringKey(java.lang.String value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value);
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof ProgramaOperacao) {
                ProgramaOperacao o = (ProgramaOperacao) object;
                return getStringKey(o.getId());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), ProgramaOperacao.class.getName()});
                return null;
            }
        }

    }

}
