/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.logic;

import br.upf.estacoes.model.beans.Pessoa;
import br.upf.estacoes.model.util.FactorySingleton;
import java.math.BigInteger;
import java.security.MessageDigest;
import javax.persistence.EntityManager;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

/**
 *
 * @author jaqson
 */
public class PessoaLogicImpl<T> extends GenericLogicImpl<T>{

    @Override
    @PrePersist @PreUpdate
    public void prePersistObjectLogic(T o) throws Exception {
        super.prePersistObjectLogic(o); //To change body of generated methods, choose Tools | Templates.
        Pessoa oNew = (Pessoa) o;
        
        /*
        Se informar login, deve informar também a senha. Se deixar em branco o login, a senha também deve ficar em branco.
        */
        if ((oNew.getLogin() != null) && (oNew.getLogin().length() > 0))
            if ((oNew.getSenha() == null) || (oNew.getSenha().length() <=0))
                throw new Exception("Se informar o login deve informar a senha.");
        
        if ((oNew.getSenha() != null) && (oNew.getSenha().length() > 0))
            if ((oNew.getLogin()== null) || (oNew.getLogin().length() <=0))
                throw new Exception("Se informar a senha deve informar o login.");

        /*
        Se a pessoa for do um perfil de administrador, deve possuir login e senha. 
        */
        if (oNew.getPerfil() == null){
           throw new Exception("Deve informar o perfil.");  
        }else if (oNew.getPerfil().isAdministrador()){
            if ((oNew.getLogin() == null) || (oNew.getLogin().length() <= 0) ||
                (oNew.getSenha() == null) || (oNew.getSenha().length() <= 0))
               throw new Exception("Para o perfil administrador deve informar o login e a senha.");            
        }
        
        /*
        A senha deve ser aplicado um algoritmo de MD5. Ver classe MessageDigest  -> MessageDigest.getInstance("MD5").
        */
        EntityManager em = FactorySingleton.getInstance().getEntityManager();
        Pessoa oOld = null;
        if (oNew.getId() != null)
           oOld = em.find(Pessoa.class, oNew.getId());
        if ((oOld == null) || (!oNew.getSenha().equals(oOld.getSenha()))){
                MessageDigest md = MessageDigest.getInstance("MD5");
                md.update(oNew.getSenha().getBytes(), 0, oNew.getSenha().length());
                oNew.setSenha(new BigInteger(1, md.digest()).toString(16));
        }
        if (em.isOpen())
           em.close();
        /*
        Adicionar atributo “quantidadeDeTelefonesSMS” na classe “Pessoa”. Sempre que persistir uma pessoa física ou empresa, deve atualizar este atributo com a quantidade de telefones SMS que a mesma contém.
        */
        if (oNew.getTelefones() == null)
           oNew.setQuantidadeDeTelefonesSMS(0); 
        else    
           oNew.setQuantidadeDeTelefonesSMS(oNew.getTelefones().size());
        
        
    }
    
    
}
