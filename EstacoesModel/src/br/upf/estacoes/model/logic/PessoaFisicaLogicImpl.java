/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.logic;

import br.upf.estacoes.model.beans.PessoaFisica;
import javax.persistence.EntityManager;
import javax.persistence.PrePersist;
import javax.persistence.Query;

/**
 *
 * @author jaqson
 */
public class PessoaFisicaLogicImpl<T> extends GenericLogicImpl<T>{

    @Override
    @PrePersist
    public void prePersistObjectLogic(T o) throws Exception {
        super.prePersistObjectLogic(o); //To change body of generated methods, choose Tools | Templates.
        PessoaFisica oNew = (PessoaFisica) o;
        /*
        Deve informar pelo menos um documento (ou cpf ou rg). 
        */
        if (((oNew.getRg() == null) || (oNew.getRg().length() <= 0) && (oNew.getCpf() == null))
            ||
            ((oNew.getRg() == null) || (oNew.getRg().length() <= 0) && (oNew.getCpf().length() <= 1)))        
           throw new Exception("Deve informar um documento (RG ou CPF).");

        
    }
  
    
    @Override
    public void postRemoveBusinessLogic(EntityManager em, T o) throws Exception {
        super.postRemoveBusinessLogic(em, o); //To change body of generated methods, choose Tools | Templates.
        /*
        Adicionar dois atributos “quantidadePessoas” e “quantidadeEmpresas” na classe Cidade. 
        Ao incluir, alterar ou remover pessoas físicas e empresas, esses atributos devem ser atualizados automaticamente com a quantidade de objetos associados em cada cidade respectivamente.  
        */
        PessoaFisica oOld = (PessoaFisica) o;
        if (oOld.getCidade() != null){
            Long qtd = (Long) em.createQuery(
                    "select count(o) from PessoaFisica o "+
                    " where o.cidade.id = "+oOld.getCidade().getId()).getSingleResult();
            Query qry = em.createNativeQuery("update Cidade set quantidadePessoas = :qtd where id = :id");
            qry.setParameter("qtd", qtd);
            qry.setParameter("id", oOld.getCidade().getId());         
            qry.executeUpdate();
        }
    }

    @Override
    public void postUpdateBusinessLogic(EntityManager em, T o) throws Exception {
        super.postUpdateBusinessLogic(em, o); //To change body of generated methods, choose Tools | Templates.
        /*
        Adicionar dois atributos “quantidadePessoas” e “quantidadeEmpresas” na classe Cidade. 
        Ao incluir, alterar ou remover pessoas físicas e empresas, esses atributos devem ser atualizados automaticamente com a quantidade de objetos associados em cada cidade respectivamente.  
        */
        PessoaFisica oNew = (PessoaFisica) o;
        if (oNew.getCidade() != null){
            Long qtd = (Long) em.createQuery(
                    "select count(o) from PessoaFisica o "+
                    " where o.cidade.id = "+oNew.getCidade().getId()).getSingleResult();
            Query qry = em.createNativeQuery("update Cidade set quantidadePessoas = :qtd where id = :id");
            qry.setParameter("qtd", qtd);
            qry.setParameter("id", oNew.getCidade().getId());         
            qry.executeUpdate();
        }
    }

    @Override
    public void postPersistBusinessLogic(EntityManager em, T o) throws Exception {
        super.postPersistBusinessLogic(em, o); //To change body of generated methods, choose Tools | Templates.
        /*
        Adicionar dois atributos “quantidadePessoas” e “quantidadeEmpresas” na classe Cidade. 
        Ao incluir, alterar ou remover pessoas físicas e empresas, esses atributos devem ser atualizados automaticamente com a quantidade de objetos associados em cada cidade respectivamente.  
        */
        PessoaFisica oNew = (PessoaFisica) o;
        if (oNew.getCidade() != null){
            Long qtd = (Long) em.createQuery(
                    "select count(o) from PessoaFisica o "+
                    " where o.cidade.id = "+oNew.getCidade().getId()).getSingleResult();
            Query qry = em.createNativeQuery("update Cidade set quantidadePessoas = :qtd where id = :id");
            qry.setParameter("qtd", qtd);
            qry.setParameter("id", oNew.getCidade().getId());         
            qry.executeUpdate();
        }
    }    
    
    
}
