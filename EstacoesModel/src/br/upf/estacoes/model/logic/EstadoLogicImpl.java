/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.logic;

import br.upf.estacoes.model.beans.Estado;
import br.upf.estacoes.model.enums.SiglaEstadoEnum;
import javax.persistence.EntityManager;
import javax.persistence.PrePersist;

/**
 *
 * @author jaqson
 */
public class EstadoLogicImpl<T> extends GenericLogicImpl<T>{

    @Override
    @PrePersist
    public void prePersistObjectLogic(T o) throws Exception {
        super.prePersistObjectLogic(o); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Passou pelo logic object...");  
        Estado oNew = (Estado) o;
        if ((oNew.getNome().equals("Rio Grande do Sul")) && 
                (oNew.getSigla() != SiglaEstadoEnum.RS))        
            throw new Exception("A sigla para Rio Grande do Sul deve ser RS");
        if (oNew.getNome().equals("Santa Catarina"))
            oNew.setSigla(SiglaEstadoEnum.SC); 
    }

    
    
    
    @Override
    public void prePersistBusinessLogic(EntityManager em, T o) throws Exception{
        super.prePersistBusinessLogic(em, o); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Passou pelo business...");  
    }
    
    
    
}
