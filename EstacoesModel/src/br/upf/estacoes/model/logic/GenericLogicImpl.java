/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.logic;

import javax.persistence.EntityManager;

/**
 *
 * @author jaqson
 */
public class GenericLogicImpl<T> implements GenericLogic<T>{

    @Override
    public void prePersistObjectLogic(T o) throws Exception {
    }

    @Override
    public void preUpdateObjectLogic(T o) throws Exception {
    }

    @Override
    public void preRemoveObjectLogic(T o) throws Exception {
    }

    @Override
    public void postLoadObjectLogic(T o) throws Exception {
    }

    @Override
    public void prePersistBusinessLogic(EntityManager em, T o) throws Exception{
    }

    @Override
    public void postPersistBusinessLogic(EntityManager em, T o) throws Exception{
    }

    @Override
    public void preUpdateBusinessLogic(EntityManager em, T o) throws Exception{
    }

    @Override
    public void postUpdateBusinessLogic(EntityManager em, T o) throws Exception{
    }

    @Override
    public void preRemoveBusinessLogic(EntityManager em, T o) throws Exception{
    }

    @Override
    public void postRemoveBusinessLogic(EntityManager em, T o) throws Exception{
    }
    
}
