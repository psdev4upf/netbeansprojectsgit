/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.validacoes;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author jaqson
 */
public class SexoValidoImpl implements ConstraintValidator<SexoValido, String>{

    @Override
    public void initialize(SexoValido a) {
    }

    @Override
    public boolean isValid(String t, ConstraintValidatorContext cvc) {
        if (t == null)
            return true;
        return ( (t.equals("Masculino")) || (t.equals("Feminino")) );
    }
    
}
