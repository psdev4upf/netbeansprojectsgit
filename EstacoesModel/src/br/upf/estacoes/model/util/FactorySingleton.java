/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jaqson
 */
public class FactorySingleton {
    
    private EntityManagerFactory factory = null;
    
    private FactorySingleton() {
        try {
            factory = Persistence.createEntityManagerFactory("EstacoesModelPU");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static FactorySingleton getInstance() {
        return FactoryEmSilgletonHolder.INSTANCE;
    }

    public EntityManagerFactory getFactory() {
        return factory;
    }
    
    public EntityManager getEntityManager() {
        return factory.createEntityManager();
    }    

    public void setFactory(EntityManagerFactory factory) {
        this.factory = factory;
    }
    
    private static class FactoryEmSilgletonHolder {

        private static final FactorySingleton INSTANCE = new FactorySingleton();
    }
}
