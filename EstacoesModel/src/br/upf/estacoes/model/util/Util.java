/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.util;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author jaqson
 */
public class Util {
   public static Integer calcularIdade(Date dataNascimento){    
      Calendar hoje = Calendar.getInstance();    
      int diaNoAnoHoje = hoje.get(Calendar.DAY_OF_YEAR);    
      int anoHoje = hoje.get(Calendar.YEAR);    
      Calendar nascimento = Calendar.getInstance();        
      nascimento.setTime(dataNascimento);    
      int diaNoAnoNasc = nascimento.get(Calendar.DAY_OF_YEAR);    
      int anoNasc = nascimento.get(Calendar.YEAR);    
      int idade = anoHoje - anoNasc;    
      if(diaNoAnoHoje < diaNoAnoNasc)    
         idade--; //Ainda não fez aniversário esse ano.    
      return idade;    
   }       
}
