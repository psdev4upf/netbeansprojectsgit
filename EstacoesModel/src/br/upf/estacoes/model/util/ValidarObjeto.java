/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.util;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;

/**
 *
 * @author jaqson
 */
public class ValidarObjeto<T> {
    
    private static ValidatorFactory factory;

    public static String getMensagensErroException(ConstraintViolationException cve) {
        Set<ConstraintViolation<?>> violacoes = cve.getConstraintViolations();
        //String ret = "O objeto tem " + violacoes.size() + " erros";
        String ret = ResourceBundle.getBundle("JavaMessages").
                getString("quantidadeDeErrosDoObjeto");
        ret = MessageFormat.format(ret, violacoes.size());
        for (ConstraintViolation<?> erro : violacoes) {
            //System.out.println(erro.getMessage());
            ret += "\n" + erro.getMessage();
        }
        return ret;
    }    
    
    public List<String> validar(T objeto){
        List<String> list = new ArrayList<>();
        Set<ConstraintViolation<T>> violacoes = factory.getValidator().validate(objeto);
        for (ConstraintViolation<T> erro : violacoes) {
            list.add(erro.getMessage());
        }        
        return list;
    }
    
    public String getMensagensErroObjeto(T objeto){
        List<String> list = validar(objeto);
        String ret = ResourceBundle.getBundle("JavaMessages").
                getString("quantidadeDeErrosDoObjeto");
        ret = MessageFormat.format(ret, list.size());        
        //String ret = "O objeto tem "+list.size()+" erros";
        for(String s : list)
            ret += "\n"+s;
        return ret;
    }
    
    
    private ValidarObjeto() {
        factory = Validation.buildDefaultValidatorFactory();
    }
    
    public static ValidarObjeto getInstance() {
        return ValidarObjetoHolder.INSTANCE;
    }
    
    private static class ValidarObjetoHolder {

        private static final ValidarObjeto INSTANCE = new ValidarObjeto();
    }
}
