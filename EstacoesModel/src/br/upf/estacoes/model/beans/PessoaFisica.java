/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.beans;

import br.upf.estacoes.model.logic.PessoaFisicaLogicImpl;
import br.upf.estacoes.model.util.Util;
import br.upf.estacoes.model.validacoes.SexoValido;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 *
 * @author jaqson
 */
@Entity
@EntityListeners(PessoaFisicaLogicImpl.class)
@DiscriminatorValue("F")
public class PessoaFisica extends Pessoa implements Serializable {
   private String cpf;
   private String rg;
   @Temporal(javax.persistence.TemporalType.DATE)
   private Date dataNascimento;
   @Transient
   private Short idade;
   @SexoValido(message = "O sexo deve ser Masculino ou Feminino")
   private String sexo;
   
    public PessoaFisica() {
    }
   
    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Short getIdade() {
        idade = Util.calcularIdade(dataNascimento).
                shortValue();
        return idade;
    }

    public void setIdade(Short idade) {
        this.idade = idade;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
}
