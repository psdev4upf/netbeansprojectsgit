/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Transient;

/**
 *
 * @author jaqson
 */
@Entity
public class VariavelDaEstacao implements Serializable {
    private static long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_VariavelDaEstacao")
    @SequenceGenerator(name = "seq_VariavelDaEstacao", sequenceName = "VariavelDaEstacao_Id", allocationSize = 1) 
    private Long id;
    private Integer intervaloLeitura;
    private Integer intervaloEnvio;
    @ManyToOne(optional = false)
    private TipoDeVariavel tipoDeVariavel;
    @ManyToOne(optional = false)
    private Estacao estacao;

    @ManyToMany
    private List<Compartilhamento> compartilhamentos;
    
    public VariavelDaEstacao() {
    }
  
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VariavelDaEstacao)) {
            return false;
        }
        VariavelDaEstacao other = (VariavelDaEstacao) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return id + " | " + tipoDeVariavel + " | " + estacao;
    }    
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIntervaloLeitura() {
        return intervaloLeitura;
    }

    public void setIntervaloLeitura(Integer intervaloLeitura) {
        this.intervaloLeitura = intervaloLeitura;
    }

    public Integer getIntervaloEnvio() {
        return intervaloEnvio;
    }

    public void setIntervaloEnvio(Integer intervaloEnvio) {
        this.intervaloEnvio = intervaloEnvio;
    }

    public TipoDeVariavel getTipoDeVariavel() {
        return tipoDeVariavel;
    }

    public void setTipoDeVariavel(TipoDeVariavel tipoDeVariavel) {
        this.tipoDeVariavel = tipoDeVariavel;
    }

    public Estacao getEstacao() {
        return estacao;
    }

    public void setEstacao(Estacao estacao) {
        this.estacao = estacao;
    }
    
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }      

    public List<Compartilhamento> getCompartilhamentos() {
        return compartilhamentos;
    }

    public void setCompartilhamentos(List<Compartilhamento> compartilhamentos) {
        this.compartilhamentos = compartilhamentos;
    }
    
}
