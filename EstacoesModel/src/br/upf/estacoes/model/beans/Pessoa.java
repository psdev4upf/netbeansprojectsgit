/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.beans;

import br.upf.estacoes.model.enums.TipoTelefoneEnum;
import br.upf.estacoes.model.logic.EstadoLogicImpl;
import br.upf.estacoes.model.logic.PessoaLogicImpl;
import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author jaqson
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@EntityListeners(PessoaLogicImpl.class)
@XmlRootElement
@DiscriminatorColumn(
    name="dtype",
    discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue(value="P")
public abstract class Pessoa implements Serializable {
    private static long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_Pessoa")
    @SequenceGenerator(name = "seq_Pessoa", sequenceName = "Pessoa_Id", allocationSize = 1)        
    private Long id;
    @Column(length = 60)
    @NotEmpty(message = "{Pessoa.nome.NotEmpty}")
    private String nome;
    @Column(length = 20)
    @Length(min = 6, message = "{Pessoa.login.Length}")
    private String login;
    @Length(min = 6, message = "{Pessoa.senha.Length}")
    private String senha;
    @Column(length = 100)
    @Email(message = "{Pessoa.email.Email}")
    private String email;
    private String endereco;
    private String bairro;
    private String cep;
    private String telefone;
    @Enumerated(EnumType.ORDINAL)
    private TipoTelefoneEnum tipoTelefone;
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Cidade cidade;
    @Valid
    @Size(min = 1, message = "{Pessoa.telefones.Size}")
    @OneToMany(mappedBy = "pessoa", cascade = CascadeType.ALL, 
               fetch = FetchType.LAZY, orphanRemoval = true)
    private List<TelefoneSMS> telefones;
    @ManyToOne(optional = false)
    private Perfil perfil;
    @ManyToMany
    private List<Compartilhamento> compartilhamentos;
    
    private Integer quantidadeDeTelefonesSMS;
    
    public Pessoa() {
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pessoa)) {
            return false;
        }
        Pessoa other = (Pessoa) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.upf.estacoes.model.beans.Pessoa[ id=" + getId() + " ]";
    } 
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    @XmlTransient
    public List<TelefoneSMS> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<TelefoneSMS> telefones) {
        this.telefones = telefones;
    }
    
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }    
    
    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }    

    public TipoTelefoneEnum getTipoTelefone() {
        return tipoTelefone;
    }

    public void setTipoTelefone(TipoTelefoneEnum tipoTelefone) {
        this.tipoTelefone = tipoTelefone;
    }

    @XmlTransient
    public List<Compartilhamento> getCompartilhamentos() {
        return compartilhamentos;
    }

    public void setCompartilhamentos(List<Compartilhamento> compartilhamentos) {
        this.compartilhamentos = compartilhamentos;
    }

    public Integer getQuantidadeDeTelefonesSMS() {
        return quantidadeDeTelefonesSMS;
    }

    public void setQuantidadeDeTelefonesSMS(Integer quantidadeDeTelefonesSMS) {
        this.quantidadeDeTelefonesSMS = quantidadeDeTelefonesSMS;
    }
}
