/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.beans;

import br.upf.estacoes.model.enums.SiglaEstadoEnum;
import br.upf.estacoes.model.logic.EstadoLogicImpl;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author jaqson
 */
@Entity
@EntityListeners(EstadoLogicImpl.class)
@XmlRootElement
public class Estado implements Serializable{
    @Version
    private Long versao;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, 
                    generator = "Estado_Seq")
    @SequenceGenerator(name = "Estado_Seq", sequenceName = "Estado_id",
                    allocationSize = 1)
    private Long id;
    @NotEmpty(message = "{Estado.nome.NotEmpty}")
    @Length(min = 3, max = 60, message = "{Estado.nome.Length}")
    private String nome;
    @NotNull(message = "{Estado.sigla.NotNull}")
    @Enumerated(EnumType.STRING)
    @Column(length = 2)
    private SiglaEstadoEnum sigla;

    public Estado() {
    }

    public Estado(Long id, String nome, SiglaEstadoEnum sigla) {
        this.id = id;
        this.nome = nome;
        this.sigla = sigla;
    }


    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estado other = (Estado) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return nome + " | " + sigla;
    }

    public Long getVersao() {
        return versao;
    }

    public void setVersao(Long versao) {
        this.versao = versao;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public SiglaEstadoEnum getSigla() {
        return sigla;
    }

    public void setSigla(SiglaEstadoEnum sigla) {
        this.sigla = sigla;
    }



}
