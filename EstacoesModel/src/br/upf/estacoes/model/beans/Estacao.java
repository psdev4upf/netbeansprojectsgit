/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.beans;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author jaqson
 */
@Entity
@XmlRootElement
public class Estacao implements Serializable {
    private static long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_Estacao")
    @SequenceGenerator(name = "seq_Estacao", sequenceName = "Estacao_Id", allocationSize = 1)    
    private Long id;
    private String codigoIdentificacao;
    private String chaveSeguranca;
    private String descricao;
    private String tipoEstacao;
    private Double latitude;
    private Double longitude;
    @ManyToOne(optional = false)
    private Pessoa proprietario;
    @OneToMany(mappedBy = "estacao", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL, orphanRemoval = true)
    private List<VariavelDaEstacao> variaveis;
    @ManyToOne(optional = false)
    private Cidade cidade;

    public Estacao() {
    }

    public Estacao(Long id, String codigoIdentificacao, String chaveSeguranca, String descricao, String tipoEstacao, Double latitude, Double longitude, Pessoa proprietario, List<VariavelDaEstacao> variaveis, Cidade cidade) {
        this.id = id;
        this.codigoIdentificacao = codigoIdentificacao;
        this.chaveSeguranca = chaveSeguranca;
        this.descricao = descricao;
        this.tipoEstacao = tipoEstacao;
        this.latitude = latitude;
        this.longitude = longitude;
        this.proprietario = proprietario;
        this.variaveis = variaveis;
        this.cidade = cidade;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estacao)) {
            return false;
        }
        Estacao other = (Estacao) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return id + " | " + codigoIdentificacao + " | " + descricao + " | " + tipoEstacao;
    }
    
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getCodigoIdentificacao() {
        return codigoIdentificacao;
    }

    public void setCodigoIdentificacao(String codigoIdentificacao) {
        this.codigoIdentificacao = codigoIdentificacao;
    }

    public String getChaveSeguranca() {
        return chaveSeguranca;
    }

    public void setChaveSeguranca(String chaveSeguranca) {
        this.chaveSeguranca = chaveSeguranca;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getTipoEstacao() {
        return tipoEstacao;
    }

    public void setTipoEstacao(String tipoEstacao) {
        this.tipoEstacao = tipoEstacao;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Pessoa getProprietario() {
        return proprietario;
    }

    public void setProprietario(Pessoa proprietario) {
        this.proprietario = proprietario;
    }

    @XmlTransient
    public List<VariavelDaEstacao> getVariaveis() {
        return variaveis;
    }

    public void setVariaveis(List<VariavelDaEstacao> variaveis) {
        this.variaveis = variaveis;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }
    
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static void setSerialVersionUID(long serialVersionUID) {
        Estacao.serialVersionUID = serialVersionUID;
    }    
    
}
