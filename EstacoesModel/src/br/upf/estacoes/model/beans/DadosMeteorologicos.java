/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.beans;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;

/**
 *
 * @author jaqson
 */
@Entity
public class DadosMeteorologicos implements Serializable {
    private static long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_DadosMeteorologicos")
    @SequenceGenerator(name = "seq_DadosMeteorologicos", sequenceName = "DadosMeteorologicos_Id", allocationSize = 1)    
    private Long id;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataHora;
    private Double valor;
    @ManyToOne(optional = false)
    private VariavelDaEstacao variavelDaEstacao;    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DadosMeteorologicos)) {
            return false;
        }
        DadosMeteorologicos other = (DadosMeteorologicos) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "br.upf.estacoes.model.beans.DadosMeteorologicos[ id=" + getId() + " ]";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }    
    
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public static void setSerialVersionUID(long aSerialVersionUID) {
        serialVersionUID = aSerialVersionUID;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public VariavelDaEstacao getVariavelDaEstacao() {
        return variavelDaEstacao;
    }

    public void setVariavelDaEstacao(VariavelDaEstacao variavelDaEstacao) {
        this.variavelDaEstacao = variavelDaEstacao;
    }
    
}
