/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.beans;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author jaqson
 */
@Entity
@Table(schema = "permissoes")
public class Programa implements Serializable{
    @Id
    private String id;
    private String descricao;
    @OneToMany(mappedBy = "programa", cascade = CascadeType.ALL,
             orphanRemoval = true)
    private List<ProgramaOperacao> operacoes;

    public Programa() {
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Programa other = (Programa) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Programa{" + "id=" + id + ", descricao=" + descricao + '}';
    }
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public List<ProgramaOperacao> getOperacoes() {
        return operacoes;
    }

    public void setOperacoes(List<ProgramaOperacao> operacoes) {
        this.operacoes = operacoes;
    }
    
    
}
