/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.estacoes.model.beans;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author jaqson
 */
@Entity
@Table(schema = "permissoes")
public class Permissao implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_Permissao")
    @SequenceGenerator(name = "seq_Permissao", sequenceName = "permissoes.Permissao_Id", allocationSize = 1)        
    private Long id;
    @ManyToOne(optional = false)
    private Perfil perfil;
    @ManyToOne(optional = false)
    private Programa programa;
    @ManyToOne(optional = false)
    private ProgramaOperacao programaOperacao;

    public Permissao() {
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 13 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Permissao other = (Permissao) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Permissao{" + "id=" + id + ", perfil=" + perfil + ", programa=" + programa + ", programaOperacao=" + programaOperacao + '}';
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Programa getPrograma() {
        return programa;
    }

    public void setPrograma(Programa programa) {
        this.programa = programa;
    }

    public ProgramaOperacao getProgramaOperacao() {
        return programaOperacao;
    }

    public void setProgramaOperacao(ProgramaOperacao programaOperacao) {
        this.programaOperacao = programaOperacao;
    }
    
    
    
}
