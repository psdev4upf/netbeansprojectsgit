/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testes.validation;

import br.upf.estacoes.model.beans.Estado;
import br.upf.estacoes.model.enums.SiglaEstadoEnum;
import br.upf.estacoes.model.util.FactorySingleton;
import br.upf.estacoes.model.util.ValidarObjeto;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.validation.ConstraintViolationException;
import org.junit.Test;

/**
 *
 * @author jaqson
 */
public class EstadoTestes {
    
    @Test
    public void inserir(){
        try {
            Estado e = new Estado(null, null, null);
            EntityManager em = FactorySingleton.getInstance().getEntityManager();
            em.getTransaction().begin();
            em.persist(e);
            em.getTransaction().commit();
        } catch (RollbackException e) {
            String s = ValidarObjeto.getInstance().getMensagensErroException(
                    (ConstraintViolationException) e.getCause());
            System.out.println(s);
        }
        
        
    }
    
    @Test
    public void validacaoManual(){
        Estado e = new Estado(null, null, null);
        String ret = ValidarObjeto.getInstance().getMensagensErroObjeto(e);
        System.out.println(ret);
    }
    @Test
    public void validacaoManualNome(){
        Estado e = new Estado(null, "X", null);
        String ret = ValidarObjeto.getInstance().getMensagensErroObjeto(e);
        System.out.println(ret);
    }    
    
    @Test
    public void validarEmIngles(){
        Locale.setDefault(new Locale("en", "US"));
        Estado e = new Estado(null, "X", null);
        String ret = ValidarObjeto.getInstance().getMensagensErroObjeto(e);
        System.out.println(ret);        
    }
    
    @Test
    public void validarRioGrandeDoSulRS(){
        try {
            EntityManager em = FactorySingleton.getInstance().getEntityManager();
            em.getTransaction().begin();
            Estado e = new Estado(null, "Rio Grande do Sul", SiglaEstadoEnum.PR);
            em.persist(e);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
    
    
    
}
