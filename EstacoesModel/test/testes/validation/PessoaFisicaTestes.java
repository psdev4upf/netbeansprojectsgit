/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testes.validation;

import br.upf.estacoes.model.beans.PessoaFisica;
import br.upf.estacoes.model.util.ValidarObjeto;
import org.junit.Test;

/**
 *
 * @author jaqson
 */
public class PessoaFisicaTestes {
    
    @Test
    public void testeSexo(){
        PessoaFisica pf = new PessoaFisica();
        pf.setSexo("Indeciso");
        System.out.println(ValidarObjeto.getInstance().getMensagensErroObjeto(pf));
    }
    
}
