package testes.services;

import br.upf.estacoes.model.beans.Cidade;
import br.upf.estacoes.model.beans.Empresa;
import br.upf.estacoes.model.beans.Perfil;
import br.upf.estacoes.model.beans.Ramo;
import br.upf.estacoes.model.beans.TelefoneSMS;
import br.upf.estacoes.model.logic.EmpresaLogicImpl;
import br.upf.estacoes.model.services.GenericJpaServiceImpl;
import br.upf.estacoes.model.util.ValidarObjeto;
import java.util.ArrayList;
import javax.persistence.RollbackException;
import javax.validation.ConstraintViolationException;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jaqson
 */
public class T006_Empresa {
    GenericJpaServiceImpl<Empresa> dao = new GenericJpaServiceImpl(
                         new EmpresaLogicImpl<Empresa>(), Empresa.class);     
    
    @Test
    public void insercaoCorreta() {
        Empresa p = new Empresa();
        p.setNome("Empresa XX");
        p.setLogin("paulologin");
        p.setSenha("senhapaulo");
        p.setPerfil(dao.getEm().find(Perfil.class, 1L));
        p.setCnpj("123123123");
        p.setCidade(dao.getEm().find(Cidade.class, 1L));
        p.setTelefones(new ArrayList<TelefoneSMS>());
        TelefoneSMS t = new TelefoneSMS();
        t.setNumero("123123");
        t.setOperadora("Oi");
        t.setPessoa(p);
        p.getTelefones().add(t);
        p.setRamo(dao.getEm().find(Ramo.class, 1L));
        
        try {
            dao.persist(p);
        } catch (RollbackException e) {
            String s = ValidarObjeto.getInstance().getMensagensErroException(
                    (ConstraintViolationException) e.getCause());
            System.out.println(s);
        } catch (Exception ex) {
            System.out.println("Erros:");
            System.out.println( ex.getMessage());
        }
    }      
    
    
}
