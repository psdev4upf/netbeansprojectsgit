/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testes.services;

import br.upf.estacoes.model.beans.Estado;
import br.upf.estacoes.model.enums.SiglaEstadoEnum;
import br.upf.estacoes.model.logic.EstadoLogicImpl;
import br.upf.estacoes.model.services.GenericJpaServiceImpl;
import org.junit.Test;

/**
 *
 * @author jaqson
 */
public class EstadoServices { 
    
    GenericJpaServiceImpl<Estado> dao =
            new GenericJpaServiceImpl(new EstadoLogicImpl<Estado>(),Estado.class);
    @Test
    public void inserir(){
        Estado e = new Estado(null, "Rio Grande do Sul", SiglaEstadoEnum.RS);
        try {
            dao.persist(e);
        } catch (Exception exception) {
            exception.printStackTrace();
            System.out.println(exception);
        }
    }
    
}
