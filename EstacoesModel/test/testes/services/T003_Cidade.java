package testes.services;

import br.upf.estacoes.model.beans.Cidade;
import br.upf.estacoes.model.beans.Estado;
import br.upf.estacoes.model.logic.GenericLogicImpl;
import br.upf.estacoes.model.services.GenericJpaServiceImpl;
import br.upf.estacoes.model.util.FactorySingleton;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jaqson
 */
public class T003_Cidade {
    GenericJpaServiceImpl<Cidade> dao = new GenericJpaServiceImpl(
                         new GenericLogicImpl<Cidade>(), Cidade.class);     
    @Test
    public void inserir() throws Exception{
        Cidade c = new Cidade(null, "Passo Fundo", dao.getEm().find(Estado.class, 1L));
        dao.persist(c);
        c = new Cidade(null, "Soledade", dao.getEm().find(Estado.class, 1L));
        dao.persist(c);
        c = new Cidade(null, "Chapecó", dao.getEm().find(Estado.class, 2L));
        dao.persist(c);
    }
    
    @Test
    public void listar() throws Exception{
        for(Cidade o : dao.findObjetos(Boolean.TRUE))
            System.out.println(o);
    }    
}
