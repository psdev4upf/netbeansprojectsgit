package testes.services;

import br.upf.estacoes.model.beans.Cidade;
import br.upf.estacoes.model.beans.Estado;
import br.upf.estacoes.model.beans.Perfil;
import br.upf.estacoes.model.beans.Ramo;
import br.upf.estacoes.model.logic.GenericLogicImpl;
import br.upf.estacoes.model.services.GenericJpaServiceImpl;
import br.upf.estacoes.model.util.FactorySingleton;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jaqson
 */
public class T004_Outros {
    @Test
    public void inserirPerfil() throws Exception{
       GenericJpaServiceImpl<Perfil> dao = new GenericJpaServiceImpl(
                         new GenericLogicImpl<Perfil>(), Perfil.class);
        Perfil p = new Perfil();
        p.setAdministrador(Boolean.TRUE);
        p.setDescricao("Admin");
        dao.persist(p);
        
        p = new Perfil();
        p.setAdministrador(Boolean.FALSE);
        p.setDescricao("Outro Perfil");
        dao.persist(p);
    }
    
    @Test
    public void inserirRamo() throws Exception{
       GenericJpaServiceImpl<Ramo> dao = new GenericJpaServiceImpl(
                         new GenericLogicImpl<Ramo>(), Ramo.class);
        Ramo p = new Ramo();
        p.setDescricao("Indústria");
        dao.persist(p);
        
        p = new Ramo();
        p.setDescricao("Comércio");
        dao.persist(p);
    }    
 
}
