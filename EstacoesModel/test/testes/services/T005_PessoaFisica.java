package testes.services;

import br.upf.estacoes.model.beans.Cidade;
import br.upf.estacoes.model.beans.Perfil;
import br.upf.estacoes.model.beans.PessoaFisica;
import br.upf.estacoes.model.beans.TelefoneSMS;
import br.upf.estacoes.model.logic.PessoaFisicaLogicImpl;
import br.upf.estacoes.model.services.GenericJpaServiceImpl;
import br.upf.estacoes.model.util.ValidarObjeto;
import java.util.ArrayList;
import javax.persistence.RollbackException;
import javax.validation.ConstraintViolationException;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jaqson
 */
public class T005_PessoaFisica {
    GenericJpaServiceImpl<PessoaFisica> dao = new GenericJpaServiceImpl(
                         new PessoaFisicaLogicImpl<PessoaFisica>(), PessoaFisica.class);     
    @Test
    public void erroPerfil() {
        PessoaFisica p = new PessoaFisica();
        p.setNome("Paulo");
        try {
            dao.persist(p);
        } catch (RollbackException e) {
            String s = ValidarObjeto.getInstance().getMensagensErroException(
                    (ConstraintViolationException) e.getCause());
            System.out.println(s);
        } catch (Exception ex) {
            System.out.println("Erros:");
            System.out.println( ex.getMessage());
        }
    }
 
    @Test
    public void erroPerfil_LoginSenha() {
        PessoaFisica p = new PessoaFisica();
        p.setNome("Paulo");
        p.setPerfil(dao.getEm().find(Perfil.class, 1));
        try {
            dao.persist(p);
        } catch (RollbackException e) {
            String s = ValidarObjeto.getInstance().getMensagensErroException(
                    (ConstraintViolationException) e.getCause());
            System.out.println(s);
        } catch (Exception ex) {
            System.out.println("Erros:");
            System.out.println( ex.getMessage());
        }
    }

    @Test
    public void erroPerfil_LoginSenha2() {
        PessoaFisica p = new PessoaFisica();
        p.setNome("Paulo");
        p.setLogin("paulo");
        p.setPerfil(dao.getEm().find(Perfil.class, 1));
        try {
            dao.persist(p);
        } catch (RollbackException e) {
            String s = ValidarObjeto.getInstance().getMensagensErroException(
                    (ConstraintViolationException) e.getCause());
            System.out.println(s);
        } catch (Exception ex) {
            System.out.println("Erros:");
            System.out.println( ex.getMessage());
        }
    }
    
    @Test
    public void erroRG_ou_CPF() {
        PessoaFisica p = new PessoaFisica();
        p.setNome("Paulo");
        p.setLogin("paulo");
        p.setSenha("senha");
        p.setPerfil(dao.getEm().find(Perfil.class, 1));
        try {
            dao.persist(p);
        } catch (RollbackException e) {
            String s = ValidarObjeto.getInstance().getMensagensErroException(
                    (ConstraintViolationException) e.getCause());
            System.out.println(s);
        } catch (Exception ex) {
            System.out.println("Erros:");
            System.out.println( ex.getMessage());
        }
    }    
    
    @Test
    public void erroCaracteresLogin() {
        PessoaFisica p = new PessoaFisica();
        p.setNome("Paulo");
        p.setLogin("paulo");
        p.setSenha("senha");
        p.setPerfil(dao.getEm().find(Perfil.class, 1));
        p.setRg("123123123");
        try {
            dao.persist(p);
        } catch (RollbackException e) {
            String s = ValidarObjeto.getInstance().getMensagensErroException(
                    (ConstraintViolationException) e.getCause());
            System.out.println(s);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Erros:");
            System.out.println( ex.getMessage());
        }
    }      
    
    @Test
    public void erroUmTelefoneSMS() {
        PessoaFisica p = new PessoaFisica();
        p.setNome("Paulo");
        p.setLogin("paulologin");
        p.setSenha("senhapaulo");
        p.setPerfil(dao.getEm().find(Perfil.class, 1L));
        p.setRg("123123123");
        p.setCidade(dao.getEm().find(Cidade.class, 1L));
        p.setTelefones(new ArrayList<TelefoneSMS>());
        
        try {
            dao.persist(p);
        } catch (RollbackException e) {
            String s = ValidarObjeto.getInstance().getMensagensErroException(
                    (ConstraintViolationException) e.getCause());
            System.out.println(s);
        } catch (Exception ex) {
            System.out.println("Erros:");
            System.out.println( ex.getMessage());
        }
    }    
    
    @Test
    public void insercaoCorreta() {
        PessoaFisica p = new PessoaFisica();
        p.setNome("Paulo");
        p.setLogin("paulologin");
        p.setSenha("senhapaulo");
        p.setPerfil(dao.getEm().find(Perfil.class, 1L));
        p.setRg("123123123");
        p.setCidade(dao.getEm().find(Cidade.class, 1L));
        p.setTelefones(new ArrayList<TelefoneSMS>());
        TelefoneSMS t = new TelefoneSMS();
        t.setNumero("123123");
        t.setOperadora("Oi");
        t.setPessoa(p);
        p.getTelefones().add(t);
        
        try {
            dao.persist(p);
        } catch (RollbackException e) {
            String s = ValidarObjeto.getInstance().getMensagensErroException(
                    (ConstraintViolationException) e.getCause());
            System.out.println(s);
        } catch (Exception ex) {
            System.out.println("Erros:");
            System.out.println( ex.getMessage());
        }
    }      
    
    
    @Test
    public void excluirPessoa() {
        try {
            dao.remove(21L);
        } catch (RollbackException e) {
            String s = ValidarObjeto.getInstance().getMensagensErroException(
                    (ConstraintViolationException) e.getCause());
            System.out.println(s);
        } catch (Exception ex) {
            System.out.println("Erros:");
            System.out.println( ex.getMessage());
        }
    }     
    
}
