/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package testes.services;

import br.upf.estacoes.model.beans.Estado;
import br.upf.estacoes.model.enums.SiglaEstadoEnum;
import br.upf.estacoes.model.logic.EstadoLogicImpl;
import br.upf.estacoes.model.services.GenericJpaServiceImpl;
import org.junit.Test;

/**
 *
 * @author jaqson
 */
public class T002_Estado {
    GenericJpaServiceImpl<Estado> dao = new GenericJpaServiceImpl(
                         new EstadoLogicImpl<Estado>(), Estado.class);    
    @Test
    public void inserir() throws Exception{
        Estado e = new Estado(null, "Rio Grande do Sul", SiglaEstadoEnum.RS);
        dao.persist(e);
        e = new Estado(null, "Santa Catarina", SiglaEstadoEnum.SC);
        dao.persist(e);
        e = new Estado(null, "Paraná", SiglaEstadoEnum.PR);
        dao.persist(e);
    }
    
    @Test
    public void listar() throws Exception{
        for(Estado o : dao.findObjetos(Boolean.TRUE))
            System.out.println(o);
    }
    
}
