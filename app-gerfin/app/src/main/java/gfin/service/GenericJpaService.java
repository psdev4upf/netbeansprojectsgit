/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gfin.service;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager; 

/**
 *
 * @author jaqson
 * @author filipe.benevenuti
 * @param <T>
 */
public interface GenericJpaService<T> extends Serializable {

    /**
     * Retorna a instância da EntityManager definida como atribudo para o objeto
     * do serviço. A EntityManager é criada e fechada em cada método, com
     * exceção daqueles que retornam instâncias de objetos, em que deve-se
     * permitir informar se a EntityManager será fechada automaticamente pelo
     * método ou se o usuário fará o fechamento manual após uso de objetos em
     * proxy.
     *
     * @return Instância da EntityManager usada pelo serviço.
     */
    EntityManager getEm();

    /**
     * Persiste um objeto no banco de dados.
     *
     * @param objeto Instância do objeto a ser persistido.
     * @throws Exception Exceção gerada, se for o caso.
     */
    void persist(T objeto) throws Exception;

    /**
     * Sincroniza/atualiza um objeto persistente no banco de dados.
     *
     * @param objeto Instância do objeto a ser sincronizado/atualizado.
     * @return Instância atualizada do objeto após sincronizado com o banco de
     * dados.
     * @throws Exception Exceção gerada, se for o caso.
     */
    T merge(T objeto) throws Exception;

    /**
     * Remove um objeto do banco de dados.
     *
     * @param id Código identificador do objeto a ser removido.
     * @throws Exception Exceção gerada, se for o caso.
     */
    void remove(Long id) throws Exception;
       
    /**
     * Remove um objeto do banco de dados.
     *
     * @param objeto referência do objeto a ser removido.
     * @throws Exception Exceção gerada, se for o caso.
     */
    void remove(T objeto) throws Exception;    

    /**
     * Recupera a instância de um objeto do banco de dados.
     *
     * @param id Código do objeto a ser recuperado.
     * @param closeEm Se a EntityManager deve ser fechada automaticamente pelo
     * método.
     * @return Instância do objeto respectivo ao id informado.
     * @throws Exception Exceção gerada, se for o caso.
     */
    T findObjeto(Long id, Boolean closeEm) throws Exception;
    

    /**
     * Recupera a instância de um objeto do banco de dados.
     *
     * @param pkey Código composto do objeto a ser recuperado.
     * @param closeEm Se a EntityManager deve ser fechada automaticamente pelo
     * método.
     * @return Instância do objeto respectivo ao id informado.
     * @throws Exception Exceção gerada, se for o caso.
     */
    T findObjeto(T pkey, Boolean closeEm) throws Exception;    

    /**
     *
     * @param closeEm Se a EntityManager deve ser fechada automaticamente pelo
     * método.
     * @return Coleção com todas as instâncias encontradas.
     * @throws Exception Exceção gerada, se for o caso.
     */
    List<T> findObjetos(Boolean closeEm) throws Exception;

    /**
     * Obter uma coleção com os objetos persistentes obtidos do banco de dados,
     * dentro de um intervalo de paginação informado.
     *
     * @param maxResults Quantidade máxima de objetos retornados.
     * @param firstResult Primeiro objeto a ser retornado. Inicia em zero.
     * @param closeEm Se a EntityManager deve ser fechada automaticamente pelo
     * método.
     * @return Coleção com os objetos encontrados.
     * @throws Exception Exceção gerada, se for o caso.
     */
    List<T> findObjetos(int maxResults, int firstResult, Boolean closeEm) throws Exception;

    /**
     * Contar a quantidade de objetos existentes.
     *
     * @return Long com a quantidade de objetos.
     * @throws Exception Exceção gerada, se for o caso.
     */
    Long getObjetosCount() throws Exception;
}
