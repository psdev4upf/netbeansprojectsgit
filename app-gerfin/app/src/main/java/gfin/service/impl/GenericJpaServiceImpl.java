package gfin.service.impl;

import gfin.utils.ConnFactorySingleton;
import gfin.logic.GenericLogic;
import gfin.service.GenericJpaService;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author jaqson
 * @author filipe.benevenuti 
 * @param <T> 
 */
public class GenericJpaServiceImpl<T> implements GenericJpaService<T>{

    private EntityManager em;
    private final GenericLogic logic;
    private final Class classeDoObjeto;

    /**
     *
     * @param logic
     * @param classeDoObjeto
     */
    public GenericJpaServiceImpl(GenericLogic logic, Class classeDoObjeto) {
        this.logic = logic;
        this.classeDoObjeto = classeDoObjeto;
    }
    
    @Override
    public EntityManager getEm() {
        return this.em; 
    }

    @Override
    public void persist(T objeto) throws Exception {
        try {
            em = ConnFactorySingleton.getInstance().getEntityManager();
            em.getTransaction().begin();
            logic.prePersistBusinessLogic(em, objeto);
            em.persist(objeto);
            logic.postPersistBusinessLogic(em, objeto);
            em.getTransaction().commit();
        } catch (Exception exception) {
            throw exception;
        }finally{
            if (em != null)
                em.close();
        }
    }

    @Override
    public T merge(T objeto) throws Exception {
        try {
            em = ConnFactorySingleton.getInstance().getEntityManager();
            em.getTransaction().begin();
            logic.preUpdateBusinessLogic(em, objeto);
            objeto = em.merge(objeto);
            logic.postUpdateBusinessLogic(em, objeto);
            em.getTransaction().commit();
            return objeto;
        } catch (Exception exception) {
            throw exception;
        }finally{
            if (em != null)
                em.close();
        }
    }

    @Override
    public void remove(Long id) throws Exception {
        try {
            em = ConnFactorySingleton.getInstance().getEntityManager();
            em.getTransaction().begin();
            T objeto = (T) em.find(classeDoObjeto, id);
            logic.preRemoveBusinessLogic(em, objeto);
            em.remove(objeto);
            logic.postRemoveBusinessLogic(em, objeto);
            em.getTransaction().commit();
        } catch (Exception exception) {
            throw exception;
        }finally{
            if (em != null)
                em.close();
        }
    }

    @Override
    public void remove(T objeto) throws Exception {
        try {
            em = ConnFactorySingleton.getInstance().getEntityManager();
            em.getTransaction().begin();
            logic.preRemoveBusinessLogic(em, objeto);
            objeto = em.merge(objeto);
            em.remove(objeto);
            logic.postRemoveBusinessLogic(em, objeto);
            em.getTransaction().commit();
        } catch (Exception exception) {
            throw exception;
        }finally{
            if (em != null)
                em.close();
        }
    }

    @Override
    public T findObjeto(Long id, Boolean closeEm) throws Exception {
        try {
            em = ConnFactorySingleton.getInstance().getEntityManager();
            T objeto = (T) em.find(classeDoObjeto, id);
            return objeto;
        } catch (Exception exception) {
            throw exception;
        }finally{
            if ((closeEm)&&(em != null))
                em.close();
        }
    }    

    @Override
    public T findObjeto(T pkey, Boolean closeEm) throws Exception {
        try {
            em = ConnFactorySingleton.getInstance().getEntityManager();
            T objeto = (T) em.find(classeDoObjeto, pkey);
            return objeto;
        } catch (Exception exception) {
            throw exception;
        }finally{
            if ((closeEm)&&(em != null))
                em.close();
        }
    }        

    @Override
    public List<T> findObjetos(Boolean closeEm) throws Exception {
        try {
            em = ConnFactorySingleton.getInstance().getEntityManager();
            Query q = em.createQuery("select o from "+
                    classeDoObjeto.getSimpleName()+" o");
            List<T> list = q.getResultList();
            return list;
        } catch (Exception exception) {
            throw exception;
        }finally{
            if ((closeEm)&&(em != null))
                em.close();
        }
    }

    @Override
    public List<T> findObjetos(int maxResults, int firstResult, Boolean closeEm) throws Exception {
        try {
            em = ConnFactorySingleton.getInstance().getEntityManager();
            Query q = em.createQuery("select o from "+
                    classeDoObjeto.getSimpleName()+" o");
            q.setMaxResults(maxResults);
            q.setFirstResult(firstResult);
            List<T> list = q.getResultList();
            return list;
        } catch (Exception exception) {
            throw exception;
        }finally{
            if ((closeEm)&&(em != null))
                em.close();
        }
    }

    @Override
    public Long getObjetosCount() throws Exception {
        try {
            em = ConnFactorySingleton.getInstance().getEntityManager();
            Query q = em.createQuery("select count(o) from "+
                    classeDoObjeto.getSimpleName()+" o");
            return (Long) q.getSingleResult();
        } catch (Exception exception) {
            throw exception;
        }finally{
            if (em != null)
                em.close();
        }
    }
}
