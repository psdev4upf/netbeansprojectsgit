/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gfin.logic;

import javax.persistence.EntityManager;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

/**
 *
 * @author jaqson
 * @author filipe.benevenuti
 * @param <T>
 */
public interface GenericLogic<T> {
    /**
     * Lógica de negócio a ser executada sobre o objeto que está sendo persistido.
     * Pode acessar ou modificar os dados do objeto.
     * @param o Objeto que está sendo persistido.
     * @throws Exception Exceção gerada, se for o caso.
     */
    @PrePersist
    public void prePersistObjectLogic(T o) throws Exception;
    /**
     * Lógica de negócio a ser executada sobre o objeto que está sendo atualizado.
     * Pode acessar ou modificar os dados do objeto.
     * @param o Objeto que está sendo atualizado.
     * @throws Exception Exceção gerada, se for o caso.
     */
    @PreUpdate
    public void preUpdateObjectLogic(T o) throws Exception; 
    /**
     * Lógica de negócio a ser executada sobre o objeto que está sendo removido.
     * Pode acessar ou modificar os dados do objeto.
     * @param o Objeto que está sendo removido.
     * @throws Exception Exceção gerada, se for o caso.
     */
    @PreRemove
    public void preRemoveObjectLogic(T o) throws Exception; 
    /**
     * Lógica de negócio a ser executada sobre o objeto que está sendo carregado.
     * Pode acessar ou modificar os dados do objeto.
     * @param o Objeto que está sendo carregado.
     * @throws Exception Exceção gerada, se for o caso.
     */
    @PostLoad
    public void postLoadObjectLogic(T o) throws Exception; 
    
    /**
     * Lógica de negócio a ser executada antes do objeto ser persistido.
     * Não é anotado como um callback method. pode utilizar a EntityManager e modificar 
     * outros objetos.
     * Pode ser utilizado pela camada de serviço, que deve passar a instância
     * atual da EntityManager e o objeto que está sendo persistido.
     * @param em Instância da EntityManager corrente no serviço.
     * @param o Objeto que está sendo persistido.
     * @throws java.lang.Exception
     */
    public void prePersistBusinessLogic(EntityManager em, T o) throws Exception;
    /**
     * Lógica de negócio a ser executada após o objeto ser persistido.
     * Não é anotado como um callback method. pode utilizar a EntityManager e modificar 
     * outros objetos.
     * Pode ser utilizado pela camada de serviço, que deve passar a instância
     * atual da EntityManager e o objeto que está sendo persistido.
     * @param em Instância da EntityManager corrente no serviço.
     * @param o Objeto que está sendo persistido.
     * @throws java.lang.Exception
     */    
    public void postPersistBusinessLogic(EntityManager em, T o) throws Exception;
    /**
     * Lógica de negócio a ser executada antes do objeto ser atualizado.
     * Não é anotado como um callback method. pode utilizar a EntityManager e modificar 
     * outros objetos.
     * Pode ser utilizado pela camada de serviço, que deve passar a instância
     * atual da EntityManager e o objeto que está sendo atualizado.
     * @param em Instância da EntityManager corrente no serviço.
     * @param o Objeto que está sendo atualizado.
     * @throws java.lang.Exception
     */       
    public void preUpdateBusinessLogic(EntityManager em, T o) throws Exception;
    /**
     * Lógica de negócio a ser executada após o objeto ser atualizado.
     * Não é anotado como um callback method. pode utilizar a EntityManager e modificar 
     * outros objetos.
     * Pode ser utilizado pela camada de serviço, que deve passar a instância
     * atual da EntityManager e o objeto que está sendo atualizado.
     * @param em Instância da EntityManager corrente no serviço.
     * @param o Objeto que está sendo atualizado.
     * @throws java.lang.Exception
     */       
    public void postUpdateBusinessLogic(EntityManager em, T o) throws Exception;    
    /**
     * Lógica de negócio a ser executada antes do objeto ser removido.
     * Não é anotado como um callback method. pode utilizar a EntityManager e modificar 
     * outros objetos.
     * Pode ser utilizado pela camada de serviço, que deve passar a instância
     * atual da EntityManager e o objeto que está sendo removido.
     * @param em Instância da EntityManager corrente no serviço.
     * @param o Objeto que está sendo removido.
     * @throws java.lang.Exception
     */    
    public void preRemoveBusinessLogic(EntityManager em, T o) throws Exception;
    /**
     * Lógica de negócio a ser executada após o objeto ser removido.
     * Não é anotado como um callback method. pode utilizar a EntityManager e modificar 
     * outros objetos.
     * Pode ser utilizado pela camada de serviço, que deve passar a instância
     * atual da EntityManager e o objeto que está sendo removido.
     * @param em Instância da EntityManager corrente no serviço.
     * @param o Objeto que está sendo removido.
     * @throws java.lang.Exception
     */      
    public void postRemoveBusinessLogic(EntityManager em, T o) throws Exception;      
        
}
