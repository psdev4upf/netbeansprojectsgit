package gfin.logic.impl;

import gfin.model.Cotacao;
import gfin.model.Moeda;
import javax.persistence.EntityManager;

/**
 *
 * @author Filipe
 * @param <T>
 */
public class CotacaoLogicImpl<T> extends GenericLogicImpl<T> {
    
    @Override
    public void postPersistBusinessLogic(EntityManager em, T o) throws Exception{
        super.postPersistBusinessLogic(em, o);
        postPersistOrUpdateBusinessLogic(em, o);
    }    

    @Override
    public void postUpdateBusinessLogic(EntityManager em, T o) throws Exception{
        super.postUpdateBusinessLogic(em, o);
        postPersistOrUpdateBusinessLogic(em, o);        
    }
    
    public void postPersistOrUpdateBusinessLogic(EntityManager em, T o) throws Exception{
        Cotacao oNew = (Cotacao)o;        
        
        /**
         * Ao inserir uma cotação, grava na moeda qual é última cotação.         * 
         */
        if ( oNew.getDia().after(oNew.getMoeda().getUltimaCotacao()) ) {
            oNew.getMoeda().setUltimaCotacao(oNew.getDia());
            Moeda m = em.merge(oNew.getMoeda());
            oNew.setMoeda(m);            
        } 
        
    }
    
}
