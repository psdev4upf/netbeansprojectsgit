/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gfin.logic.impl;

import gfin.model.Moeda;
import gfin.utils.GenericUse;
import java.util.Currency;
import java.util.List;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

/**
 *
 * @author Filipe
 */
public class MoedaLogicImpl<T> extends GenericLogicImpl<T> {
    
    @Override
    @PrePersist
    public void prePersistObjectLogic(T o) throws Exception {
        super.prePersistObjectLogic(o);        
        prePersistOrUpdateObjectLogic(o);
    }

    @Override
    @PreUpdate
    public void preUpdateObjectLogic(T o) throws Exception {
        super.preUpdateObjectLogic(o);        
        prePersistOrUpdateObjectLogic(o);        
    }    
    
    public void prePersistOrUpdateObjectLogic(T o) throws Exception{
        Moeda oNew = (Moeda)o;
        
        /**
         * Ao inserir ou alterar uma moeda, verifica se a sigla está 
         * de acordo com padrão ISO 4217
         * 
         * Caso esteja, também deve utilizar o mesmo símbolo previsto
         * na ISO.
         */
        
        List<Currency> currencys  = GenericUse.getAllCurrencies();
        boolean found = false;
        for (Currency currency : currencys) {
            if (currency.getCurrencyCode().equals(oNew.getSigla())) {
                if (!currency.getSymbol().equals(oNew.getSimbolo())) {
                    //TODO Internacionalizar 
                    throw new Exception("Esta moeda está com o símbolo " + oNew.getSimbolo() + ". No entanto o símbolo deve ser " + currency.getSymbol() + " (ISO 4217).");
                } else {
                    found = true;
                    break;
                }
            } 
        }
        
        if (!found) {
            //TODO Internacionalizar
            throw new Exception("A sigla " + oNew.getSigla() + " não atende ao padrão ISO 4217.");            
        }
        
    }
    
}
