package gfin.utils;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;

/**
 *
 * @author jaqson
 * @author filipe.benevenuti
 * @param <T>
 */
public class ObjectValidation<T> {
    
    private static ValidatorFactory factory;

    /**
     *
     * @param cve
     * @return
     */
    public static String getMensagensErroException(ConstraintViolationException cve) {
        Set<ConstraintViolation<?>> violacoes = cve.getConstraintViolations();
        //String ret = "O objeto tem " + violacoes.size() + " erros";
        String ret = ResourceBundle.getBundle("JavaMessages").
                getString("quantidadeDeErrosDoObjeto");
        ret = MessageFormat.format(ret, violacoes.size());
        for (ConstraintViolation<?> erro : violacoes) {
            //System.out.println(erro.getMessage());
            ret += "\n" + erro.getMessage();
        }
        return ret;
    }

    /**
     *
     * @param objeto
     * @return
     */
    public List<String> validar(T objeto){
        List<String> list = new ArrayList<>();
        Set<ConstraintViolation<T>> violacoes = factory.getValidator().validate(objeto);
        for (ConstraintViolation<T> erro : violacoes) {
            list.add(erro.getMessage());
        }        
        return list;
    }
    
    /**
     *
     * @param objeto
     * @return
     */
    public String getMensagensErroObjeto(T objeto){
        List<String> list = validar(objeto);
        String ret = ResourceBundle.getBundle("JavaMessages").
                getString("quantidadeDeErrosDoObjeto");
        ret = MessageFormat.format(ret, list.size());        
        //String ret = "O objeto tem "+list.size()+" erros";
        for(String s : list)
            ret += "\n"+s;
        return ret;
    }
    
    
    private ObjectValidation() {
        factory = Validation.buildDefaultValidatorFactory();
    }
    
    /**
     *
     * @return
     */
    public static ObjectValidation getInstance() {
        return ValidarObjetoHolder.INSTANCE;
    }
    
    private static class ValidarObjetoHolder {

        private static final ObjectValidation INSTANCE = new ObjectValidation();
    }
}
