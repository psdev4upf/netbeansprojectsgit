/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gfin.utils;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

/**
 *
 * @author Filipe
 */
public class GenericUse {
    
    public static List<Currency> getAllCurrencies()
    {
        List<Currency> lCurr = new ArrayList<>();
        Locale[] locs = Locale.getAvailableLocales();

        for(Locale loc : locs) {
            try {
                lCurr.add( Currency.getInstance( loc ) );
            } catch(Exception exc)
            {
                // Locale not found
            }
        }

        return lCurr;
    }    
    
}
