package gfin.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Algoritmo de criptografia baseado no exemplo de Mustafa Cantürk, do site <a
 * href="http://mustafacanturk.com/sha512-hashing-on-java/">mustafacanturk.com</a>.
 *
 * @author Filipe Benevenuti
 * @author Felipe Furst
 * @author Mustafa Cantürk
 */
public final class PasswordService {

    private static PasswordService instance;

    private PasswordService() {
    }

    public synchronized String encrypt(String plaintext) throws NoSuchAlgorithmException {
        MessageDigest md = null;
       
            md = MessageDigest.getInstance("SHA-512");

            md.update(plaintext.getBytes());
            byte[] mb = md.digest();
            String out = "";
            for (int i = 0; i < mb.length; i++) {
                byte temp = mb[i];
                String s = Integer.toHexString(new Byte(temp));
                while (s.length() < 2) {
                    s = "0" + s;
                }
                s = s.substring(s.length() - 2);
                out += s;
            }          
            
            //System.out.println(out.length());
            //System.out.println("CRYPTO: " + out);
            return out;       
    }

    public static synchronized PasswordService getInstance() {//step 1
        if (instance == null) {
            instance = new PasswordService();
        }
        return instance;
    }
}
