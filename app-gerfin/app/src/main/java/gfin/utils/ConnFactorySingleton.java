/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gfin.utils;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author jaqson
 * @author filipe.benevenuti
 */
public class ConnFactorySingleton {
    
    private EntityManagerFactory factory = null;
    
    private ConnFactorySingleton() {
        try {
            factory = Persistence.createEntityManagerFactory("GerFinPU");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     *
     * @return
     */
    public static ConnFactorySingleton getInstance() {
        return FactoryEmSilgletonHolder.INSTANCE;
    }

    /**
     *
     * @return
     */
    public EntityManagerFactory getFactory() {
        return factory;
    }
    
    /**
     *
     * @return
     */
    public EntityManager getEntityManager() {
        return factory.createEntityManager();
    }    

    /**
     *
     * @param factory
     */
    public void setFactory(EntityManagerFactory factory) {
        this.factory = factory;
    }
    
    private static class FactoryEmSilgletonHolder {

        private static final ConnFactorySingleton INSTANCE = new ConnFactorySingleton();
    }
}
