package gfin.enums;

/**
 *
 * @author Filipe Benevenuti
 */
public enum PeriodicidadeDiasEnum {

    /**
     *
     */
    SEMANAL(7),

    /**
     *
     */
    QUINZENAL(15),

    /**
     *
     */
    MENSAL(30);

    private Integer dias;

    private PeriodicidadeDiasEnum(Integer dias) {
        this.dias = dias;
    }

    /**
     *
     * @return
     */
    public Integer getDias() {
        return dias;
    }

    /**
     *
     * @param dias
     */
    public void setDias(Integer dias) {
        this.dias = dias;
    }
}
