package gfin.enums;

/**
 *
 * @author Filipe Benevenuti
 */
public enum TipoQuitacaoEnum {

    /**
     *
     */
    PARCIAL("P"),

    /**
     *
     */
    TOTAL("T");

    private String sigla;

    private TipoQuitacaoEnum(String sigla) {
        this.sigla = sigla;
    }

    /**
     *
     * @return
     */
    public String getSigla() {
        return sigla;
    }

    /**
     *
     * @param sigla
     */
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
}
