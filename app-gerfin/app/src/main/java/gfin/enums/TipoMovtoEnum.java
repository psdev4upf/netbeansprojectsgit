package gfin.enums;

/**
 *
 * @author Filipe Benevenuti
 */
public enum TipoMovtoEnum {

    /**
     *
     */
    DEBITO("D"),

    /**
     *
     */
    CREDITO("C");

    private String sigla;

    private TipoMovtoEnum(String sigla) {
        this.sigla = sigla;
    }

    /**
     *
     * @return
     */
    public String getSigla() {
        return sigla;
    }

    /**
     *
     * @param sigla
     */
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

}
