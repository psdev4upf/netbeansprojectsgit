/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gfin.model;

import gfin.enums.TipoMovtoEnum;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa um grupo de {@link Categoria}s
 * <p>
 * Se trata de uma categorização em função do tipo da despesa e/ou do crédito.
 * <p>
 * Ligado à {@link ContaPagar}, {@link ContaReceber} e {@link Movimentacao}.
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Categoria
 * @see ContaPagar
 * @see ContaReceber
 * @see Movimentacao
 * @see Registro
 *
 */
@Entity
//@Table(indexes = {
//    @Index(name = "fk_grupo_registro", columnList = "registro_id")})
public class Grupo implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_grupo")
    @SequenceGenerator(name = "gen_grupo", sequenceName = "seq_grupo", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.grupo.id.notnull}")
    private Long id;

    @Column(nullable = false, length = 100)
    @NotNull(message = "{message.grupo.descricao.notnull}")
    @Size(max = 100, message = "{message.grupo.descricao.max}")
    private String descricao;

    @Column(nullable = false)
    @NotNull(message = "{message.grupo.tipomovto.notnull}")
    private TipoMovtoEnum tipoMovto;
    
    @ManyToOne
    @ForeignKey(name = "fk_grupo_registro")
    private Registro registro;    

    @Version
    private Long version;

    public Grupo() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public TipoMovtoEnum getTipoMovto() {
        return tipoMovto;
    }

    public void setTipoMovto(TipoMovtoEnum tipoMovto) {
        this.tipoMovto = tipoMovto;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the registro
     */
    public Registro getRegistro() {
        return registro;
    }

    /**
     * @param registro the registro to set
     */
    public void setRegistro(Registro registro) {
        this.registro = registro;
    }

    @Override
    public String toString() {
        return "Grupo{" + "id=" + id + ", descricao=" + descricao + ", tipoMovto=" + tipoMovto + ", registro=" + registro + ", version=" + version + '}';
    }
    
    

}
