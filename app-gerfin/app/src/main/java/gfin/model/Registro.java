package gfin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Representa um "bloco" de informações, pertencentes a um grupo de
 * {@link Pessoa}s e/ou {@link Usuario}s.
 * <p>
 * Se trata da informação que separa os dados de um usuário de outro. Sendo
 * assim, é a solução para que um usuário não seja capaz de alterar ou mesmo
 * visualizar os dados de outro.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Pessoa
 * @see Usuario
 *
 */
@Entity
public class Registro implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_registro")
    @SequenceGenerator(name = "gen_registro", sequenceName = "seq_registro", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.registro.id.notnull}")
    private Long id;

    @Column(nullable = false, length = 50)
    @NotNull(message = "{message.registro.nome.notnull}")
    @Size(max = 50, message = "{message.registro.nome.max}")
    private String nome;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    @NotNull(message = "{message.registro.datacriacao.notnull}")
    private Date dataCriacao;

    @Version
    private Long version;

    /**
     *
     */
    public Registro() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return this.nome;
    }

    /**
     *
     * @return
     */
    public Date getDataCriacao() {
        return this.dataCriacao;
    }

    /**
     *
     * @return
     */
    public Long getVersion() {
        return this.version;
    }

    /**
     *
     * @param myId
     */
    public void setId(Long myId) {
        this.id = myId;
    }

    /**
     *
     * @param myNome
     */
    public void setNome(String myNome) {
        this.nome = myNome;
    }

    /**
     *
     * @param myDataCriacao
     */
    public void setDataCriacao(Date myDataCriacao) {
        this.dataCriacao = myDataCriacao;
    }

    /**
     *
     * @param myVersion
     */
    public void setVersion(Long myVersion) {
        this.version = myVersion;
    }

    @Override
    public String toString() {
        return "Registro{" + "id=" + id + ", nome=" + nome + ", dataCriacao=" + dataCriacao + ", version=" + version + '}';
    }
    
    

}
