package gfin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma pessoa jurídica, detalhe de uma {@link Pessoa}.
 * <p>
 * Se trata da referência principal dos "donos" dos lançamentos de contas e
 * outros registros.
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Pessoa
 * @see Registro
 * @see Usuario
 *
 */
@Entity
@ForeignKey(name = "fk_pessoajur_pessoa")
@PrimaryKeyJoinColumn(name = "ID")
public class PessoaJuridica extends Pessoa {

    @Column(nullable = false, length = 14)
    @NotNull(message = "{message.pessoajuridica.cnpj.notnull}")
    @Size(max = 14, message = "{message.pessoajuridica.cnpj.max}")
    private String cnpj;

    @Column(nullable = false, length = 100)
    @NotNull(message = "{message.pessoajuridica.nomefantasia.notnull}")
    @Size(max = 100, message = "{message.pessoajuridica.nomefantasia.max}")
    private String nomeFantasia;

    /**
     *
     */
    public PessoaJuridica() {
        super();
    }

    /**
     *
     * @return
     */
    public String getCnpj() {
        return this.cnpj;
    }

    /**
     *
     * @return
     */
    public String getNomeFantasia() {
        return this.nomeFantasia;
    }

    /**
     *
     * @param myCnpj
     */
    public void setCnpj(String myCnpj) {
        this.cnpj = myCnpj;
    }

    /**
     *
     * @param myNomeFantasia
     */
    public void setNomeFantasia(String myNomeFantasia) {
        this.nomeFantasia = myNomeFantasia;
    }

}
