package gfin.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma pessoa física, detalhe de uma {@link Pessoa}.
 * <p>
 * Se trata da referência principal dos "donos" dos lançamentos de contas e
 * outros registros.
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Pessoa
 * @see Registro
 * @see Usuario
 *
 */
@Entity
@ForeignKey(name = "fk_pessoafis_pessoa")
@PrimaryKeyJoinColumn(name = "ID")
public class PessoaFisica extends Pessoa {

    @Column(nullable = false, length = 11)
    @NotNull(message = "{message.pessoafisica.cpf.notnull}")
    @Size(max = 11, message = "{message.pessoafisica.cpf.max}")
    private String cpf;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    @NotNull(message = "{message.pessoafisica.datanascimento.notnull}")
    private Date dataNascimento;

    /**
     *
     */
    public PessoaFisica() {
        super();
    }

    /**
     *
     * @return
     */
    public String getCpf() {
        return this.cpf;
    }

    /**
     *
     * @return
     */
    public Date getDataNascimento() {
        return this.dataNascimento;
    }

    /**
     *
     * @param myCpf
     */
    public void setCpf(String myCpf) {
        this.cpf = myCpf;
    }

    /**
     *
     * @param myDataNascimento
     */
    public void setDataNascimento(Date myDataNascimento) {
        this.dataNascimento = myDataNascimento;
    }

}
