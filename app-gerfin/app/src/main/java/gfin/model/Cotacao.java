package gfin.model;

import gfin.logic.impl.CotacaoLogicImpl;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma cotação de uma {@link Moeda} em uma determinada data.
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Moeda
 * @see Registro
 * @see Usuario
 *
 */

@Entity
@EntityListeners(CotacaoLogicImpl.class)
//@Table(indexes = {
//    @Index(name = "fk_cotacao_moeda", columnList = "moeda_id"),
//    @Index(name = "fk_cotacao_registro", columnList = "registro_id")})
public class Cotacao implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_cotacao")
    @SequenceGenerator(name = "gen_cotacao", sequenceName = "seq_cotacao", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.cotacao.id.notnull}")
    private Long id;

    @ManyToOne
    @JoinColumn(nullable = false)
    @ForeignKey(name = "fk_cotacao_moeda")
    @NotNull(message = "{message.cotacao.moeda.notnull}")
    private Moeda moeda;

    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    @NotNull(message = "{message.cotacao.dia.notnull}")
    private Date dia;

    @Column(nullable = false)
    @NotNull(message = "{message.cotacao.valor.notnull}")
    private Double valor;

    @ManyToOne(fetch = FetchType.EAGER)
    @ForeignKey(name = "fk_cotacao_registro")
    private Registro registro;

    @Version
    private Long version;

    /**
     *
     */
    public Cotacao() {
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Moeda getMoeda() {
        return moeda;
    }

    /**
     *
     * @param moeda
     */
    public void setMoeda(Moeda moeda) {
        this.moeda = moeda;
    }

    /**
     *
     * @return
     */
    public Date getDia() {
        return dia;
    }

    /**
     *
     * @param dia
     */
    public void setDia(Date dia) {
        this.dia = dia;
    }

    /**
     *
     * @return
     */
    public Double getValor() {
        return valor;
    }

    /**
     *
     * @param valor
     */
    public void setValor(Double valor) {
        this.valor = valor;
    }

    /**
     *
     * @return
     */
    public Long getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Cotacao{" + "id=" + id + ", moeda=" + moeda + ", dia=" + dia + ", valor=" + valor + '}';
    }

    /**
     * @return the registro
     */
    public Registro getRegistro() {
        return registro;
    }

    /**
     * @param registro the registro to set
     */
    public void setRegistro(Registro registro) {
        this.registro = registro;
    }

}
