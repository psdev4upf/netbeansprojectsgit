package gfin.model;

import gfin.enums.PeriodicidadeDiasEnum;
import gfin.enums.TipoMovtoEnum;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma repetição de contas a pagar ({@link ContaPagar}) ou a receber
 * ({@link ContaReceber}).
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see ContaPagar
 * @see ContaReceber
 * @see Registro
 *
 */
@Entity
//@Table(indexes = {
//    @Index(name = "fk_periodico_registro", columnList = "registro_id")})
public class Periodico implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_periodico")
    @SequenceGenerator(name = "gen_periodico", sequenceName = "seq_periodico", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.periodico.id.notnull}")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @NotNull(message = "{message.periodico.tipomovto.notnull}")
    private TipoMovtoEnum tipoMovto;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    @NotNull(message = "{message.periodico.datainicio.notnull}")
    private Date dataInicio;

    @Column(nullable = false)
    @NotNull(message = "{message.periodico.numrepeticoes.notnull}")
    private Long numRepeticoes;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @NotNull(message = "{message.periodico.periodicidadedias.notnull}")
    private PeriodicidadeDiasEnum periodicidadeDias;

    @Column(nullable = false)
    @NotNull(message = "{message.periodico.valor.notnull}")
    private double valor;

    @Version
    private Long version;

    @ManyToOne
    @ForeignKey(name = "fk_periodico_registro")
    private Registro registro;

    /**
     *
     */
    public Periodico() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     *
     * @return
     */
    public TipoMovtoEnum getTipoMovto() {
        return this.tipoMovto;
    }

    /**
     *
     * @return
     */
    public Date getDataInicio() {
        return this.dataInicio;
    }

    /**
     *
     * @return
     */
    public Long getNumRepeticoes() {
        return this.numRepeticoes;
    }

    /**
     *
     * @return
     */
    public PeriodicidadeDiasEnum getPeriodicidadeDias() {
        return this.periodicidadeDias;
    }

    /**
     *
     * @return
     */
    public double getValor() {
        return this.valor;
    }

    /**
     *
     * @return
     */
    public Long getVersion() {
        return this.version;
    }

    /**
     *
     * @return
     */
    public Registro getRegistro() {
        return this.registro;
    }

    /**
     *
     * @param myId
     */
    public void setId(Long myId) {
        this.id = myId;
    }

    /**
     *
     * @param myTipoMovto
     */
    public void setTipoMovto(TipoMovtoEnum myTipoMovto) {
        this.tipoMovto = myTipoMovto;
    }

    /**
     *
     * @param myDataInicio
     */
    public void setDataInicio(Date myDataInicio) {
        this.dataInicio = myDataInicio;
    }

    /**
     *
     * @param myNumRepeticoes
     */
    public void setNumRepeticoes(Long myNumRepeticoes) {
        this.numRepeticoes = myNumRepeticoes;
    }

    /**
     *
     * @param myPeriodicidadeDias
     */
    public void setPeriodicidadeDias(PeriodicidadeDiasEnum myPeriodicidadeDias) {
        this.periodicidadeDias = myPeriodicidadeDias;
    }

    /**
     *
     * @param myValor
     */
    public void setValor(double myValor) {
        this.valor = myValor;
    }

    /**
     *
     * @param myVersion
     */
    public void setVersion(Long myVersion) {
        this.version = myVersion;
    }

    /**
     *
     * @param myRegistro
     */
    public void setRegistro(Registro myRegistro) {
        this.registro = myRegistro;
    }

}
