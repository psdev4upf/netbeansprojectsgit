package gfin.model;

import gfin.enums.TipoMovtoEnum;
import java.io.Serializable;
import java.util.List;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma movimentação de uma {@link Conta}.
 * <p>
 * Pode estar vinculada a uma conta a pagar ({@link ContaPagar}) ou a receber
 * ({@link ContaReceber}) de forma múltipla.
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Conta
 * @see ContaPagar
 * @see ContaReceber
 * @see Registro
 * @see Usuario
 *
 */
@Entity
//@Table(indexes = {
//    @Index(name = "fk_mvntacao_conta", columnList = "conta_id"),
//    @Index(name = "fk_mvntacao_registro", columnList = "registro_id")
//})
public class Movimentacao implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_movimentacao")
    @SequenceGenerator(name = "gen_movimentacao", sequenceName = "seq_movimentacao", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.movimentacao.id.notnull}")
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    @NotNull(message = "{message.movimentacao.datamovto.notnull}")
    private Date dataMovto;

    @Column(nullable = false)
    @NotNull(message = "{message.movimentacao.valor.notnull}")
    private Double valor;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @NotNull(message = "{message.movimentacao.tipomovto.notnull}")
    private TipoMovtoEnum tipoMovto;

    @Column(nullable = false, length = 200)
    @NotNull(message = "{message.movimentacao.histmovto.notnull}")
    @Size(max = 200, message = "{message.movimentacao.histmovto.max}")
    private String histMovto;

    @Version
    private Long version;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    @ForeignKey(name = "fk_mvntacao_conta")
    private Conta conta;

    @ManyToMany(fetch = FetchType.LAZY)
    @ForeignKey(name = "fk_mvntacao_ctapagar", inverseName = "fk_ctapagar_mvntacao")
    private List<ContaPagar> contaPagar;

    @ManyToMany(fetch = FetchType.LAZY)
    @ForeignKey(name = "fk_mvntacao_ctarec", inverseName = "fk_ctarec_mvntacao")
    private List<ContaReceber> contaReceber;

    @ManyToOne(fetch = FetchType.EAGER)
    @ForeignKey(name = "fk_mvntacao_registro")
    private Registro registro;

    /**
     *
     */
    public Movimentacao() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Date getDataMovto() {
        return dataMovto;
    }

    /**
     *
     * @param dataMovto
     */
    public void setDataMovto(Date dataMovto) {
        this.dataMovto = dataMovto;
    }

    /**
     *
     * @return
     */
    public Double getValor() {
        return valor;
    }

    /**
     *
     * @param valor
     */
    public void setValor(Double valor) {
        this.valor = valor;
    }

    /**
     *
     * @return
     */
    public TipoMovtoEnum getTipoMovto() {
        return tipoMovto;
    }

    /**
     *
     * @param tipoMovto
     */
    public void setTipoMovto(TipoMovtoEnum tipoMovto) {
        this.tipoMovto = tipoMovto;
    }

    /**
     *
     * @return
     */
    public String getHistMovto() {
        return histMovto;
    }

    /**
     *
     * @param histMovto
     */
    public void setHistMovto(String histMovto) {
        this.histMovto = histMovto;
    }

    /**
     *
     * @return
     */
    public Long getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    public Conta getConta() {
        return conta;
    }

    /**
     *
     * @param conta
     */
    public void setConta(Conta conta) {
        this.conta = conta;
    }

    /**
     *
     * @return
     */
    public List<ContaPagar> getContaPagar() {
        return contaPagar;
    }

    /**
     *
     * @param contaPagar
     */
    public void setContaPagar(List<ContaPagar> contaPagar) {
        this.contaPagar = contaPagar;
    }

    /**
     *
     * @return
     */
    public List<ContaReceber> getContaReceber() {
        return contaReceber;
    }

    /**
     *
     * @param contaReceber
     */
    public void setContaReceber(List<ContaReceber> contaReceber) {
        this.contaReceber = contaReceber;
    }

    /**
     *
     * @return
     */
    public Registro getRegistro() {
        return registro;
    }

    /**
     *
     * @param registro
     */
    public void setRegistro(Registro registro) {
        this.registro = registro;
    }

    @Override
    public String toString() {
        return "Movimentacao{" + "id=" + id + ", dataMovto=" + dataMovto + ", valor=" + valor + ", tipoMovto=" + tipoMovto + ", histMovto=" + histMovto + ", version=" + version + ", conta=" + conta + ", contaPagar=" + contaPagar + ", contaReceber=" + contaReceber + ", registro=" + registro + '}';
    }

}
