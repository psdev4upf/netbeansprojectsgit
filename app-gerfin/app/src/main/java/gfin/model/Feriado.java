package gfin.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma feriado. Utilizado para o cálculo de dia de vencimentos.
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Registro
 * @see Usuario
 *
 */
@Entity
//@Table(indexes = {
//    @Index(name = "fk_feriado_registro", columnList = "registro_id")})
public class Feriado implements Serializable {

    @Id
    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_feriado")
    @SequenceGenerator(name = "gen_feriado", sequenceName = "seq_feriado", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.feriado.dia.notnull}")
    private Date dia;

    @Column(nullable = false, length = 100)
    @NotNull(message = "{message.feriado.descricao.notnull}")
    @Size(max = 100, message = "{message.feriado.descricao.max}")
    private String descricao;

    @Column(nullable = false)
    @NotNull(message = "{message.feriado.fixo.notnull}")
    private Boolean fixo;

    @ManyToOne
    @ForeignKey(name = "fk_feriado_registro")
    private Registro registro;

    @Version
    private Long version;

    /**
     *
     */
    public Feriado() {
    }

    /**
     *
     * @return
     */
    public Date getDia() {
        return dia;
    }

    /**
     *
     * @param dia
     */
    public void setDia(Date dia) {
        this.dia = dia;
    }

    /**
     *
     * @return
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     *
     * @param descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     *
     * @return
     */
    public Boolean isFixo() {
        return fixo;
    }

    /**
     *
     * @param fixo
     */
    public void setFixo(Boolean fixo) {
        this.fixo = fixo;
    }

    @Override
    public String toString() {
        return "Feriado{" + "dia=" + dia + ", descricao=" + descricao + '}';
    }

    /**
     *
     * @return
     */
    public Long getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     * @return the registro
     */
    public Registro getRegistro() {
        return registro;
    }

    /**
     * @param registro the registro to set
     */
    public void setRegistro(Registro registro) {
        this.registro = registro;
    }

}
