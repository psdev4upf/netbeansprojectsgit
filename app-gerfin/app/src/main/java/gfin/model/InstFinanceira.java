package gfin.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma instituição financeira (bancos, financeiras, etc).
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see InstFinanceira
 * @see Registro
 *
 */

@Entity
@ForeignKey(name = "fk_instfin_pessjur")  
@PrimaryKeyJoinColumn(name = "ID")
public class InstFinanceira extends PessoaJuridica {

    @Column(nullable = false)
    @NotNull(message = "{message.instfinanceira.idfebraban.notnull}")
    private Long idFebraban;
    
    /**
     *
     */
    public InstFinanceira() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getIdFebraban() {
        return this.idFebraban;
    }

    /**
     *
     * @param myIdFebraban
     */
    public void setIdFebraban(Long myIdFebraban) {
        this.idFebraban = myIdFebraban;
    }

}
