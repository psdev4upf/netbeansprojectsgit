package gfin.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma agência de uma instituição financeira
 * ({@link InstFinanceira}).
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see InstFinanceira
 * @see Registro
 *
 */
@Entity
//@Table(indexes = {
//    @Index(name = "fk_agencia_instfin", columnList = "instfinanceira_id")})
public class Agencia implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_agencia")
    @SequenceGenerator(name = "gen_agencia", sequenceName = "seq_agencia", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.agencia.id.notnull}")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @ForeignKey(name = "fk_agencia_instfin")
    @NotNull(message = "{message.agencia.instfinanceira.notnull}")
    private InstFinanceira instFinanceira;

    @Column(nullable = false, length = 10)
    @NotNull(message = "{message.agencia.numero.notnull}")
    @Size(min = 4, max = 10, message = "{message.agencia.numero.size}")
    private String numero;

    @Column(nullable = false, length = 1)
    @NotNull(message = "{message.agencia.numerodv.notnull}")
    private String numeroDV;

    @Column(nullable = false, length = 50)
    @NotNull(message = "{message.agencia.nome.notnull}")
    private String nome;

    @ManyToOne(fetch = FetchType.EAGER)
    @ForeignKey(name = "fk_agencia_registro")
    private Registro registro;

    @Version
    private Long version;

    /**
     * Constrói uma agência nula.
     */
    public Agencia() {
    }

    /**
     *
     * @return Retorna o identificador da agência.
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id Define o identificador da agência.
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return Retorna o número da agência sem o dígito verificador.
     */
    public String getNumero() {
        return numero;
    }

    /**
     *
     * @param numero Define o número da agência sem o dígito verificador.
     */
    public void setNumero(String numero) {
        this.numero = numero;
    }

    /**
     *
     * @return Retorna o dígito verificador da agência.
     */
    public String getNumeroDV() {
        return numeroDV;
    }

    /**
     *
     * @param numeroDV Define o dígito verificador da agência.
     */
    public void setNumeroDV(String numeroDV) {
        this.numeroDV = numeroDV;
    }

    /**
     *
     * @return Retorna o nome da agência.
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * @param nome Define o nome da agência.
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @return Retorna a versão do objeto.
     */
    public Long getVersion() {
        return version;
    }

    /**
     *
     * @param version Define a versão do objeto.
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     *
     * @return Retorna a instituição financeira a qual a agência pertence.
     */
    public InstFinanceira getInstFinanceira() {
        return instFinanceira;
    }

    /**
     *
     * @param instFinanceira Define a instituição financeira a qual a agência
     * pertence.
     */
    public void setInstFinanceira(InstFinanceira instFinanceira) {
        this.instFinanceira = instFinanceira;
    }

    @Override
    public String toString() {
        return "Agencia{" + "id=" + id + ", numero=" + numero + ", numeroDV=" + numeroDV + ", nome=" + nome + '}';
    }

    /**
     *
     * @return Retorna o registro proprietário do cadastro.
     */
    public Registro getRegistro() {
        return registro;
    }

    /**
     *
     * @param registro Define o registro proprietário do cadastro.
     */
    public void setRegistro(Registro registro) {
        this.registro = registro;
    }

}
