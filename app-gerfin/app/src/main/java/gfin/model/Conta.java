package gfin.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma conta em uma {@link Agencia} de uma instituição financeira
 * ({@link InstFinanceira}).
 * <p>
 * Está vinculada a uma {@link Moeda} e pertence a uma {@link Pessoa}.
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Agencia
 * @see InstFinanceira
 * @see Moeda
 * @see Pessoa
 * @see Registro
 *
 */
@Entity
//@Table(indexes = {
//    @Index(name = "fk_conta_agencia", columnList = "agencia_id"),
//    @Index(name = "fk_conta_moeda", columnList = "moeda_id"),
//    @Index(name = "fk_conta_tipoconta", columnList = "tipoconta_id"),
//    @Index(name = "fk_conta_registro", columnList = "registro_id"),
//    @Index(name = "fk_conta_pessoa", columnList = "pessoa_id")
//})
public class Conta implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_conta")
    @SequenceGenerator(name = "gen_conta", sequenceName = "seq_conta", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.conta.id.notnull}")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    @ForeignKey(name = "fk_conta_agencia")
    @NotNull(message = "{message.conta.agencia.notnull}")
    private Agencia agencia;

    @Column(nullable = false)
    @NotNull(message = "{message.conta.numero.notnull}")
    private Integer numero;

    @Column(nullable = false)
    @NotNull(message = "{message.conta.numerodv.notnull}")
    private Integer numeroDv;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    @ForeignKey(name = "fk_conta_moeda")
    @NotNull(message = "{message.conta.moeda.notnull}")
    private Moeda moeda;

    @Column(nullable = false)
    @NotNull(message = "{message.conta.limite.notnull}")
    private Double limite;

    @Column(nullable = false)
    @NotNull(message = "{message.conta.saldoinicial.notnull}")
    private Double saldoInicial;

    @Column(nullable = false, length = 100)
    @NotNull(message = "{message.conta.nome.notnull}")
    @Size(max = 100, message = "{message.conta.nome.max}")
    private String nome;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    @ForeignKey(name = "fk_conta_tipoconta")
    private TipoConta tipoConta;

    @ManyToOne(fetch = FetchType.EAGER)
    @ForeignKey(name = "fk_conta_registro")
    private Registro registro;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    @ForeignKey(name = "fk_conta_pessoa")
    private Pessoa pessoa;

    @Version
    private Long version;

    /**
     *
     */
    public Conta() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Agencia getAgencia() {
        return agencia;
    }

    /**
     *
     * @param agencia
     */
    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    /**
     *
     * @return
     */
    public Integer getNumero() {
        return numero;
    }

    /**
     *
     * @param numero
     */
    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    /**
     *
     * @return
     */
    public Integer getNumeroDv() {
        return numeroDv;
    }

    /**
     *
     * @param numeroDv
     */
    public void setNumeroDv(Integer numeroDv) {
        this.numeroDv = numeroDv;
    }

    /**
     *
     * @return
     */
    public Moeda getMoeda() {
        return moeda;
    }

    /**
     *
     * @param moeda
     */
    public void setMoeda(Moeda moeda) {
        this.moeda = moeda;
    }

    /**
     *
     * @return
     */
    public Double getLimite() {
        return limite;
    }

    /**
     *
     * @param limite
     */
    public void setLimite(Double limite) {
        this.limite = limite;
    }

    /**
     *
     * @return
     */
    public Double getSaldoInicial() {
        return saldoInicial;
    }

    /**
     *
     * @param saldoInicial
     */
    public void setSaldoInicial(Double saldoInicial) {
        this.saldoInicial = saldoInicial;
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @return
     */
    public Long getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    public TipoConta getTipoConta() {
        return tipoConta;
    }

    /**
     *
     * @param tipoConta
     */
    public void setTipoConta(TipoConta tipoConta) {
        this.tipoConta = tipoConta;
    }

    /**
     *
     * @return
     */
    public Registro getRegistro() {
        return registro;
    }

    /**
     *
     * @param registro
     */
    public void setRegistro(Registro registro) {
        this.registro = registro;
    }

    /**
     *
     * @return
     */
    public Pessoa getPessoa() {
        return pessoa;
    }

    /**
     *
     * @param pessoa
     */
    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    @Override
    public String toString() {
        return "Conta{" + "agencia=" + agencia + ", numero=" + numero + ", numeroDv=" + numeroDv + ", nome=" + nome + ", tipoConta=" + tipoConta + '}';
    }

}
