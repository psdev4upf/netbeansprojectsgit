package gfin.model;

import gfin.enums.TipoMovtoEnum;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma categoria de um {@link Grupo}.
 * <p>
 * Se trata de uma categorização em função do tipo da despesa e/ou do crédito.
 * <p>
 * Ligado à {@link ContaPagar}, {@link ContaReceber} e {@link Movimentacao}.
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Grupo
 * @see ContaPagar
 * @see ContaReceber
 * @see Movimentacao
 * @see Registro
 *
 */
@Entity
//@Table(indexes = {
//    @Index(name = "fk_categoria_grupo", columnList = "grupo_id")})
public class Categoria implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_categoria")
    @SequenceGenerator(name = "gen_categoria", sequenceName = "seq_categoria", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.categoria.id.notnull}")
    private Long Id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(nullable = false)
    @ForeignKey(name = "fk_categoria_grupo")
    @NotNull(message = "{message.categoria.grupo.notnull}")
    private Grupo grupo;

    @Column(nullable = false, length = 300)
    @NotNull(message = "{message.categoria.descricao.notnull}")
    @Size(max = 300, message = "{message.categoria.descricao.max}")
    private String descricao;

    @Column(nullable = false)
    @NotNull(message = "{message.categoria.tipomovto.notnull}")
    private TipoMovtoEnum tipoMovto;

    @ManyToOne(fetch = FetchType.EAGER)
    @ForeignKey(name = "fk_categoria_registro")
    private Registro registro;

    @Version
    private Long version;

    /**
     * Constrói uma categoria nula;
     */
    public Categoria() {

    }

    /**
     *
     * @return
     */
    public Long getId() {
        return Id;
    }

    /**
     *
     * @param Id
     */
    public void setId(Long Id) {
        this.Id = Id;
    }

    /**
     *
     * @return
     */
    public Grupo getGrupo() {
        return grupo;
    }

    /**
     *
     * @param grupo
     */
    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    /**
     *
     * @return
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     *
     * @param descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     *
     * @return
     */
    public TipoMovtoEnum getTipoMovto() {
        return tipoMovto;
    }

    /**
     *
     * @param tipoMovto
     */
    public void setTipoMovto(TipoMovtoEnum tipoMovto) {
        this.tipoMovto = tipoMovto;
    }

    /**
     *
     * @return
     */
    public Long getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Categoria{" + "Id=" + Id + ", grupo=" + grupo + ", descricao=" + descricao + ", tipoMovto=" + tipoMovto + ", version=" + version + '}';
    }

    /**
     *
     * @return
     */
    public Registro getRegistro() {
        return registro;
    }

    /**
     *
     * @param registro
     */
    public void setRegistro(Registro registro) {
        this.registro = registro;
    }
    
    

}
