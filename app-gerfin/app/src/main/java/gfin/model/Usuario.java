package gfin.model;

import gfin.validators.iface.Email;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa o usuário de uma {@link Pessoa}.
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Pessoa
 * @see Usuario
 *
 */
@Entity
//@Table(indexes = {
//    @Index(name = "fk_usuario_pessoa", columnList = "pessoa_id")})
public class Usuario implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_usuario")
    @SequenceGenerator(name = "gen_usuario", sequenceName = "seq_usuario", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.usuario.id.notnull}")
    private Long id;

    @Column(nullable = false, length = 100)
    @NotNull(message = "{message.usuario.nome.notnull}")
    @Size(max = 100, message = "{message.usuario.nome.max}")
    private String nome;

    //sha-512 = 128 chars hex
    @Column(nullable = false, length = 128)
    @NotNull(message = "{message.usuario.senha.notnull}")
    @Size(max = 128, message = "{message.usuario.senha.max}")
    private String senha;

    @Column(nullable = false, length = 255)
    @NotNull(message = "{message.usuario.email.notnull}")
    @Size(max = 255, message = "{message.usuario.email.max}")
    @Email
    private String email;

    @OneToOne
    @ForeignKey(name = "fk_usuario_pessoa")
    //@NotNull(message = "{message.usuario.pessoa.notnull}")
    private Pessoa pessoa;

    @Version
    private Long version;

    /**
     *
     */
    public Usuario() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return this.nome;
    }

    /**
     *
     * @return
     */
    public String getSenha() {
        return this.senha;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return this.email;
    }

    /**
     *
     * @return
     */
    public Long getVersion() {
        return this.version;
    }

    /**
     *
     * @return
     */
    public Pessoa getPessoa() {
        return this.pessoa;
    }

    /**
     *
     * @param myId
     */
    public void setId(Long myId) {
        this.id = myId;
    }

    /**
     *
     * @param myNome
     */
    public void setNome(String myNome) {
        this.nome = myNome;
    }

    /**
     *
     * @param mySenha
     */
    public void setSenha(String mySenha) {
        this.senha = mySenha;
    }

    /**
     *
     * @param myEmail
     */
    public void setEmail(String myEmail) {
        this.email = myEmail;
    }

    /**
     *
     * @param myVersion
     */
    public void setVersion(Long myVersion) {
        this.version = myVersion;
    }

    /**
     *
     * @param myPessoa
     */
    public void setPessoa(Pessoa myPessoa) {
        this.pessoa = myPessoa;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", nome=" + nome + ", senha=" + senha + ", email=" + email + ", version=" + version + ", pessoa=" + pessoa + '}';
    }

}
