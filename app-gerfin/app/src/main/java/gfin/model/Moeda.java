package gfin.model;

import gfin.logic.impl.MoedaLogicImpl;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma moeda.
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Registro
 * @see Usuario
 *
 */

@Entity
@EntityListeners(MoedaLogicImpl.class)
public class Moeda implements Serializable {
    
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_moeda")
    @SequenceGenerator(name = "gen_moeda", sequenceName = "seq_moeda", allocationSize = 1, initialValue = 1)        
    @NotNull(message = "{message.moeda.id.notnull}")
    private Long id;
    
    @Column(nullable = false, length = 50)
    @NotNull(message = "{message.moeda.nome.notnull}")
    @Size(max = 50, message = "{message.moeda.nome.max}")
    private String nome;
    
    @Column(nullable = false, length = 4)
    @NotNull(message = "{message.moeda.simbolo.notnull}")
    @Size(max = 4, message = "{message.moeda.simbolo.max}")
    private String simbolo;
    
    @Column(nullable = false, length = 4)
    @NotNull(message = "{message.moeda.sigla.notnull}")
    @Size(max = 4, message = "{message.moeda.sigla.max}")
    private String sigla;
    
    @Column(nullable = false)
    @NotNull(message = "{message.moeda.moedapadrao.notnull}")
    private Boolean moedaPadrao;
    
    @Column(nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    //@NotNull(message = "{message.moeda.ultimacotacao.notnull}")
    private Date ultimaCotacao;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @ForeignKey(name = "fk_moeda_registro")
    private Registro registro;    
    
    @Version
    private Long version;

    /**
     *
     */
    public Moeda() {
        
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return nome;
    }

    /**
     *
     * @param nome
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     *
     * @return
     */
    public String getSimbolo() {
        return simbolo;
    }

    /**
     *
     * @param simbolo
     */
    public void setSimbolo(String simbolo) {
        this.simbolo = simbolo;
    }

    /**
     *
     * @return
     */
    public String getSigla() {
        return sigla;
    }

    /**
     *
     * @param sigla
     */
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }      

    /**
     *
     * @return
     */
    public Boolean isMoedaPadrao() {
        return moedaPadrao;
    }

    /**
     *
     * @param moedaPadrao
     */
    public void setMoedaPadrao(Boolean moedaPadrao) {
        this.moedaPadrao = moedaPadrao;
    }    

    /**
     *
     * @return
     */
    public Long getVersion () {
        return version;
    }

    /**
     *
     * @return
     */
    public Date getUltimaCotacao() {
        return ultimaCotacao;
    }

    /**
     *
     * @param ultimaCotacao
     */
    public void setUltimaCotacao(Date ultimaCotacao) {
        this.ultimaCotacao = ultimaCotacao;
    }      

    /**
     *
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }       
    
    @Override
    public String toString() {
        return "Moeda{" + "id=" + id + ", nome=" + nome + '}';
    }         

    /**
     * @return the registro
     */
    public Registro getRegistro() {
        return registro;
    }

    /**
     * @param registro the registro to set
     */
    public void setRegistro(Registro registro) {
        this.registro = registro;
    }    
}
