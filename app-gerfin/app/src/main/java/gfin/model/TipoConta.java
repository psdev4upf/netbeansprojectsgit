package gfin.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Representa o tipo de uma {@link Conta} (poupança, corrente, cartão de
 * crédito, etc).
 * <p>
 * Pertence a todos os {@link Usuario}s. A manutenção destes dados é da área
 * administrativa do software.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Conta
 * @see Usuario
 *
 */
@Entity
public class TipoConta implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_tipoconta")
    @SequenceGenerator(name = "gen_tipoconta", sequenceName = "seq_tipoconta", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.tipoconta.id.notnull}")
    private Long id;

    @Column(nullable = false, length = 50)
    @NotNull(message = "{message.tipoconta.descricao.notnull}")
    @Size(max = 50, message = "{message.tipoconta.descricao.max}")
    private String descricao;

    @Version
    private Long version;

    /**
     *
     */
    public TipoConta() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     *
     * @return
     */
    public String getDescricao() {
        return this.descricao;
    }

    /**
     *
     * @return
     */
    public Long getVersion() {
        return this.version;
    }

    /**
     *
     * @param myId
     */
    public void setId(Long myId) {
        this.id = myId;
    }

    /**
     *
     * @param myDescricao
     */
    public void setDescricao(String myDescricao) {
        this.descricao = myDescricao;
    }

    /**
     *
     * @param myVersion
     */
    public void setVersion(Long myVersion) {
        this.version = myVersion;
    }

}
