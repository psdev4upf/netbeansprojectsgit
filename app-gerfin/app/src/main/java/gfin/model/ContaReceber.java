package gfin.model;

import gfin.enums.TipoQuitacaoEnum;
import java.io.Serializable;
import java.util.List;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma conta a receber, um crédito a ser recebido.
 * <p>
 * Está vinculada a uma {@link Moeda} e vincula uma dívida de terceiros com o
 * usuário. Quem deve é o terceiro (cedente), quem receberá é o usuário
 * (favorecido), mas ambos são do tipo {@link Pessoa}.
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see Moeda
 * @see Pessoa
 * @see Registro
 * @see Usuario
 *
 */
@Entity
//@Table(indexes = {
//    @Index(name = "fk_ctareceber_categoria", columnList = "categoria_id"),
//    @Index(name = "fk_ctareceber_pes_fav", columnList = "favorecido_id"),
//    @Index(name = "fk_ctareceber_registro", columnList = "registro_id"),
//    @Index(name = "fk_ctareceber_pes_ced", columnList = "cedente_id"),
//    @Index(name = "fk_ctareceber_conta", columnList = "contadestino_id")
//})
public class ContaReceber implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_contasreceber")
    @SequenceGenerator(name = "gen_contasreceber", sequenceName = "seq_contasreceber", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.contareceber.id.notnull}")
    private Long id;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    @NotNull(message = "{message.contareceber.datarec.notnull}")
    private Date dataRec;

    @Column(nullable = false, length = 300)
    @NotNull(message = "{message.contareceber.descricao.notnull}")
    @Size(max = 300, message = "{message.contareceber.descricao.max}")
    private String descricao;

    @Column(nullable = false)
    @NotNull(message = "{message.contareceber.valor.notnull}")
    private Double valor;

    @Column(nullable = true)
    @Temporal(TemporalType.DATE)
    private Date dataQuitacao;

    @Column(nullable = true)
    private double valorQuitacao;

    @Column(nullable = true)
    @Enumerated(EnumType.STRING)
    private TipoQuitacaoEnum tipoQuitacao;

    @ManyToOne(fetch = FetchType.EAGER)
    @ForeignKey(name = "fk_ctareceber_categoria")
    private Categoria categoria;

    @ManyToMany(mappedBy = "contaReceber", fetch = FetchType.LAZY)
    private List<Movimentacao> movimentacao;

    @ManyToOne(fetch = FetchType.EAGER)
    @ForeignKey(name = "fk_ctareceber_pes_fav")
    private Pessoa favorecido;

    @ManyToOne(fetch = FetchType.EAGER)
    @ForeignKey(name = "fk_ctareceber_registro")
    private Registro registro;

    @ManyToOne(fetch = FetchType.EAGER)
    @ForeignKey(name = "fk_ctareceber_pes_ced")
    private Pessoa cedente;

    @ManyToOne(fetch = FetchType.EAGER)
    @ForeignKey(name = "fk_ctareceber_conta")
    private Conta contaDestino;

    @Version
    private Long version;

    /**
     *
     */
    public ContaReceber() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public Date getDataRec() {
        return dataRec;
    }

    /**
     *
     * @param dataRec
     */
    public void setDataRec(Date dataRec) {
        this.dataRec = dataRec;
    }

    /**
     *
     * @return
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     *
     * @param descricao
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     *
     * @return
     */
    public double getValor() {
        return valor;
    }

    /**
     *
     * @param valor
     */
    public void setValor(double valor) {
        this.valor = valor;
    }

    /**
     *
     * @return
     */
    public Date getDataQuitacao() {
        return dataQuitacao;
    }

    /**
     *
     * @param dataQuitacao
     */
    public void setDataQuitacao(Date dataQuitacao) {
        this.dataQuitacao = dataQuitacao;
    }

    /**
     *
     * @return
     */
    public double getValorQuitacao() {
        return valorQuitacao;
    }

    /**
     *
     * @param valorQuitacao
     */
    public void setValorQuitacao(double valorQuitacao) {
        this.valorQuitacao = valorQuitacao;
    }

    /**
     *
     * @return
     */
    public TipoQuitacaoEnum getTipoQuitacao() {
        return tipoQuitacao;
    }

    /**
     *
     * @param tipoQuitacao
     */
    public void setTipoQuitacao(TipoQuitacaoEnum tipoQuitacao) {
        this.tipoQuitacao = tipoQuitacao;
    }

    /**
     *
     * @return
     */
    public Long getVersion() {
        return version;
    }

    /**
     *
     * @param version
     */
    public void setVersion(Long version) {
        this.version = version;
    }

    /**
     *
     * @return
     */
    public Categoria getCategoria() {
        return categoria;
    }

    /**
     *
     * @param categoria
     */
    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    /**
     *
     * @return
     */
    public List<Movimentacao> getMovimentacao() {
        return movimentacao;
    }

    /**
     *
     * @param movimentacao
     */
    public void setMovimentacao(List<Movimentacao> movimentacao) {
        this.movimentacao = movimentacao;
    }

    /**
     *
     * @return
     */
    public Pessoa getFavorecido() {
        return favorecido;
    }

    /**
     *
     * @param favorecido
     */
    public void setFavorecido(Pessoa favorecido) {
        this.favorecido = favorecido;
    }

    /**
     *
     * @return
     */
    public Registro getRegistro() {
        return registro;
    }

    /**
     *
     * @param registro
     */
    public void setRegistro(Registro registro) {
        this.registro = registro;
    }

    /**
     *
     * @return
     */
    public Pessoa getCedente() {
        return cedente;
    }

    /**
     *
     * @param cedente
     */
    public void setCedente(Pessoa cedente) {
        this.cedente = cedente;
    }

    /**
     *
     * @return
     */
    public Conta getContaDestino() {
        return contaDestino;
    }

    /**
     *
     * @param contaDestino
     */
    public void setContaDestino(Conta contaDestino) {
        this.contaDestino = contaDestino;
    }

    @Override
    public String toString() {
        return "ContaReceber{" + "id=" + id + ", dataRec=" + dataRec + ", descricao=" + descricao + ", valor=" + valor + ", dataQuitacao=" + dataQuitacao + ", valorQuitacao=" + valorQuitacao + ", tipoQuitacao=" + tipoQuitacao + ", version=" + version + ", categoria=" + categoria + ", movimentacao=" + movimentacao + ", favorecido=" + favorecido + ", registro=" + registro + ", cedente=" + cedente + ", contaDestino=" + contaDestino + '}';
    }

}
