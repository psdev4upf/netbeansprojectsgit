package gfin.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.annotations.ForeignKey;

/**
 * Representa uma pessoa, origem de {@link PessoaFisica} e de
 * {@link PessoaJuridica}.
 * <p>
 * Se trata da referência principal dos "donos" dos lançamentos de contas e
 * outros registros.
 * <p>
 * Pertence a um {@link Registro} e, portanto, é individual, não pertencendo a
 * todos os utilizadores (não confundir com {@link Usuario}) do aplicativo.
 *
 * @author Filipe Benevenuti
 * <a href="mailto:benevenuti@gmail.com">&lt;benevenuti@gmail.com&gt;</a>
 * @author Felipe Furst
 * <a href="mailto:fefurst@gmail.com">&lt;fefurst@gmail.com&gt;</a>
 *
 * @see PessoaFisica
 * @see PessoaJuridica
 * @see Registro
 * @see Usuario
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
//@Table(indexes = {
//    @Index(name = "fk_pessoa_registro", columnList = "registro_id")})
public class Pessoa implements Serializable {

    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen_pessoa")
    @SequenceGenerator(name = "gen_pessoa", sequenceName = "seq_pessoa", allocationSize = 1, initialValue = 1)
    @NotNull(message = "{message.pessoa.id.notnull}")
    private Long id;

    @Column(nullable = false, length = 100)
    @NotNull(message = "{message.pessoa.nome.notnull}")
    @Size(max = 100, message = "{message.pessoa.nome.max}")
    private String nome;

    @ManyToOne
    @ForeignKey(name = "fk_pessoa_registro")
    @NotNull(message = "{message.pessoa.registro.notnull}")
    private Registro registro;

    @Version
    private Long version;

    /**
     *
     */
    public Pessoa() {
        super();
    }

    /**
     *
     * @return
     */
    public Long getId() {
        return this.id;
    }

    /**
     *
     * @return
     */
    public String getNome() {
        return this.nome;
    }

    /**
     *
     * @return
     */
    public Long getVersion() {
        return this.version;
    }

    /**
     *
     * @return
     */
    public Registro getRegistro() {
        return this.registro;
    }

    /**
     *
     * @param myId
     */
    public void setId(Long myId) {
        this.id = myId;
    }

    /**
     *
     * @param myNome
     */
    public void setNome(String myNome) {
        this.nome = myNome;
    }

    /**
     *
     * @param myVersion
     */
    public void setVersion(Long myVersion) {
        this.version = myVersion;
    }

    /**
     *
     * @param myRegistro
     */
    public void setRegistro(Registro myRegistro) {
        this.registro = myRegistro;
    }

    @Override
    public String toString() {
        return "Pessoa{" + "id=" + id + ", nome=" + nome + ", registro=" + registro + ", version=" + version + '}';
    } 

}
