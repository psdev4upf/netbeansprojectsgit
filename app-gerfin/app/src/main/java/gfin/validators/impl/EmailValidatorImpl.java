/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gfin.validators.impl;

import gfin.validators.iface.Email;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author Felipe
 */
public class EmailValidatorImpl implements ConstraintValidator<Email, String>{

    /**
     *
     * @param a
     */
    @Override
    public void initialize(Email a) {
    }

    /**
     *
     * @param t
     * @param cvc
     * @return
     */
    @Override
    public boolean isValid(String t, ConstraintValidatorContext cvc) {        
        if (t == null)
            return true;
        
        String emailreg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";        
        return t.matches(emailreg);
    }    
}
