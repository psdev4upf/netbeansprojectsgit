package gfin.testing;

import gfin.enums.TipoMovtoEnum;
import gfin.logic.impl.GenericLogicImpl;
import gfin.logic.impl.RegistroLogicImpl;
import gfin.model.Grupo;
import gfin.model.Registro;
import gfin.service.impl.GenericJpaServiceImpl;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Filipe Benevenuti
 *
 */
public class T06_CRUD_Grupo {

    GenericJpaServiceImpl<Registro> daoRegistro = new GenericJpaServiceImpl(new RegistroLogicImpl<Registro>(), Registro.class);
    GenericJpaServiceImpl<Grupo> daoGrupo = new GenericJpaServiceImpl(new GenericLogicImpl<Grupo>(), Grupo.class);

    @Test
    public void m01_crud_grupo_create() {
        try {
            // busca o registro 1
            Registro r;

            r = daoRegistro.findObjeto(1L, Boolean.FALSE);

            // cria o grupo 1
            Grupo g = new Grupo();
            g.setRegistro(r);
            g.setDescricao("Alimentação");
            g.setTipoMovto(TipoMovtoEnum.DEBITO);
            g.setRegistro(null);            
            daoGrupo.persist(g);
            System.out.println(g);

        } catch (Exception ex) {
            Logger.getLogger(T06_CRUD_Grupo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void m02_crud_grupo_read() {
        try {
            // busca
            Grupo g = daoGrupo.findObjeto(1L, Boolean.TRUE);
            System.out.println(g);
        } catch (Exception ex) {
            Logger.getLogger(T06_CRUD_Grupo.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    @Test
    public void m03_crud_grupo_update() {
        try {
            // busca
            Grupo g = daoGrupo.findObjeto(1L, Boolean.TRUE);
            System.out.println(g);
            
            // update
            g.setDescricao(g.getDescricao() + " - Atualizado");
            g = daoGrupo.merge(g);
            System.out.println(g);
            
        } catch (Exception ex) {
            Logger.getLogger(T06_CRUD_Grupo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void m04_crud_grupo_delete() {
        try {
            // busca
            Grupo g = daoGrupo.findObjeto(1L, Boolean.TRUE);
            System.out.println(g);
            
            // delete
            daoGrupo.remove(g);           
            
        } catch (Exception ex) {
            Logger.getLogger(T06_CRUD_Grupo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
