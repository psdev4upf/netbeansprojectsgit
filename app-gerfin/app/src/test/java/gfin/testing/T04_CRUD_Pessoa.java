package gfin.testing;

import gfin.logic.impl.PessoaLogicImpl;
import gfin.logic.impl.RegistroLogicImpl;
import gfin.model.Pessoa;
import gfin.model.Registro;
import gfin.service.impl.GenericJpaServiceImpl;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Filipe Benevenuti
 *
 */
public class T04_CRUD_Pessoa {

    GenericJpaServiceImpl<Registro> daoRegistro = new GenericJpaServiceImpl(new RegistroLogicImpl<Registro>(), Registro.class);
    GenericJpaServiceImpl<Pessoa> daoPessoa = new GenericJpaServiceImpl(new PessoaLogicImpl<Pessoa>(), Pessoa.class);

    @Test
    public void m01_crud_pessoa_create() {

        try {
            // 1 - busca o registro 1
            Registro r = daoRegistro.findObjeto(1L, Boolean.TRUE);

            // 2 - cria a pessoa 1
            Pessoa p = new Pessoa();
            p.setNome("Super Administrador GerFin");
            p.setRegistro(r);
            daoPessoa.persist(p);

        } catch (Exception ex) {
            Logger.getLogger(C01_Init.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void m02_crud_pessoa_read() {
        try {
            // busca
            Pessoa p = daoPessoa.findObjeto(1L, Boolean.TRUE);
            System.out.println(p);

        } catch (Exception ex) {
            Logger.getLogger(T04_CRUD_Pessoa.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void m03_crud_pessoa_update() {
        try {
            // busca
            Pessoa p = daoPessoa.findObjeto(1L, Boolean.TRUE);
            System.out.println(p);

            // update
            p.setNome(p.getNome() + " - Atualizada");
            p = daoPessoa.merge(p);
            System.out.println(p);

        } catch (Exception ex) {
            Logger.getLogger(T04_CRUD_Pessoa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void m04_crud_pessoa_delete() {
        try {
            // busca
            Pessoa p = daoPessoa.findObjeto(1L, Boolean.TRUE);
            System.out.println(p);

            // deleta
            daoPessoa.remove(p);
            System.out.println(p);

        } catch (Exception ex) {
            Logger.getLogger(T04_CRUD_Pessoa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
