package gfin.testing;

import gfin.logic.impl.RegistroLogicImpl;
import gfin.model.Registro;
import gfin.service.impl.GenericJpaServiceImpl;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Filipe Benevenuti
 *
 */
public class T03_CRUD_Registro {

    static GenericJpaServiceImpl<Registro> daoRegistro = new GenericJpaServiceImpl(new RegistroLogicImpl<Registro>(), Registro.class);

    @Test
    public void m01_crud_registro_create() {

        // cria
        Registro r = new Registro();
        r.setNome("Registro do superadmin");
        r.setDataCriacao(Calendar.getInstance().getTime());

        try {
            daoRegistro.persist(r);
            System.out.println(r);
        } catch (Exception ex) {
            Logger.getLogger(C01_Init.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void m02_crud_registro_read() {

        Registro r;
        try {
            // busca
            r = daoRegistro.findObjeto(1L, Boolean.TRUE);
            System.out.println(r);
        } catch (Exception ex) {
            Logger.getLogger(T03_CRUD_Registro.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void m03_crud_registro_update() {
        Registro r;

        try {
            // busca
            r = daoRegistro.findObjeto(1L, Boolean.FALSE);
            System.out.println(r);

            // atualiza
            r.setNome(r.getNome() + " - Atualizado");
            r = daoRegistro.merge(r);
            System.out.println(r);

        } catch (Exception ex) {
            Logger.getLogger(T03_CRUD_Registro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void m04_crud_registro_delete() {
        Registro r;
        try {
            // busca
            r = daoRegistro.findObjeto(1L, Boolean.FALSE);
            System.out.println(r);

            // deleta
            daoRegistro.remove(r); // deve dar erro, se tiver qualquer registro filho
        } catch (Exception ex) {
            Logger.getLogger(T03_CRUD_Registro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
