/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gfin.testing;

import gfin.logic.impl.CotacaoLogicImpl;
import gfin.logic.impl.MoedaLogicImpl;
import gfin.model.Cotacao;
import gfin.model.Moeda;
import gfin.model.Pessoa;
import gfin.model.Usuario;
import gfin.service.impl.GenericJpaServiceImpl;
import gfin.utils.ConnFactorySingleton;
import gfin.utils.ObjectValidation;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.validation.ConstraintViolationException;
import org.junit.Test;

/**
 *
 * @author Filipe
 */
public class C02_LogicTmp {

    GenericJpaServiceImpl<Cotacao> daoCotacao = new GenericJpaServiceImpl(new CotacaoLogicImpl<Cotacao>(), Cotacao.class);
    

    @Test
    public void insereReal() {
        GenericJpaServiceImpl<Moeda> daoMoeda = new GenericJpaServiceImpl(new MoedaLogicImpl<Cotacao>(), Moeda.class);
        Moeda m = new Moeda();
        m.setMoedaPadrao(Boolean.TRUE);
        m.setNome("Real");
        m.setSimbolo("R$"); //R$
        m.setSigla("BRL"); // BRL
        Calendar c = Calendar.getInstance();
        c.set(2000, 1, 1);
        m.setUltimaCotacao(c.getTime());

        //Locale.setDefault(new Locale("de", "DE"));
        //Locale.setDefault(new Locale("es", "ES"));
        //Locale.setDefault(new Locale("fr", "FR"));
        //Locale.setDefault(new Locale("en", "US"));
        try {
            daoMoeda.persist(m);
        } catch (Exception ex) {
            Logger.getLogger(C02_LogicTmp.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void validacaoManual() {
        Usuario usuario = new Usuario();
        usuario.setId(1L);
        usuario.setNome("teste");
        usuario.setSenha("sdfsdfsdfsdfsdfsf23r2r23");
        usuario.setPessoa(new Pessoa());
        usuario.setEmail("testes.teste.com");

        String retVal = ObjectValidation.getInstance().getMensagensErroObjeto(usuario);
        System.out.println(retVal);
    }

    @Test
    public void validaInserir() {
        try {
            Usuario usuario = new Usuario();
            usuario.setId(1L);
            usuario.setNome("teste");
            usuario.setSenha("sdfsdfsdfsdfsdfsf23r2r23");
            usuario.setPessoa(null);
            usuario.setEmail("testes.teste.com");
            EntityManager em = ConnFactorySingleton.getInstance().getEntityManager();
            em.getTransaction().begin();
            em.persist(usuario);
            em.getTransaction().commit();
        } catch (RollbackException err) {
            String s = ObjectValidation.getInstance().getMensagensErroException((ConstraintViolationException) err.getCause());
            System.out.println(s);

        }
    }

}
