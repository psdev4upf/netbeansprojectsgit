package gfin.testing;

import gfin.utils.ConnFactorySingleton;
import org.junit.Test;


/**
 *
 * @author Filipe Benevenuti
 */
public class C01_Init {

    @Test
    public void m00_criarDDLBanco() {
        ConnFactorySingleton.getInstance();
    }

    @Test
    public void m01_criaRegistroSuperAdmin() {
        T03_CRUD_Registro T03 = new T03_CRUD_Registro();
        T03.m01_crud_registro_create();
    }

    @Test
    public void m02_criaPessoaSuperAdmin() {
        T04_CRUD_Pessoa T04 = new T04_CRUD_Pessoa();
        T04.m01_crud_pessoa_create();
    }

    @Test
    public void m03_criaUsuarioSuperAdmin() {
        T05_CRUD_Usuario T05 = new T05_CRUD_Usuario();
        T05.m01_crud_usuario_create();
    }

}
