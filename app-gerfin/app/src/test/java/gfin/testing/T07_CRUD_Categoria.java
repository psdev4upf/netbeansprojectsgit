package gfin.testing;

import gfin.enums.TipoMovtoEnum;
import gfin.logic.impl.GenericLogicImpl;
import gfin.logic.impl.RegistroLogicImpl;
import gfin.model.Categoria;
import gfin.model.Grupo;
import gfin.model.Registro;
import gfin.service.impl.GenericJpaServiceImpl;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Filipe Benevenuti
 *
 */
public class T07_CRUD_Categoria {

    GenericJpaServiceImpl<Registro> daoRegistro = new GenericJpaServiceImpl(new RegistroLogicImpl<Registro>(), Registro.class);
    GenericJpaServiceImpl<Grupo> daoGrupo = new GenericJpaServiceImpl(new GenericLogicImpl<Grupo>(), Grupo.class);
    GenericJpaServiceImpl<Categoria> daoCategoria = new GenericJpaServiceImpl(new GenericLogicImpl<Categoria>(), Categoria.class);

    @Test
    public void m01_crud_categoria_create() {
        try {
            // busca o grupo 1 - Alimentação
            Grupo g = daoGrupo.findObjeto(1L, Boolean.TRUE);
            
            // busca o registro 1
            Registro r = daoRegistro.findObjeto(1L, Boolean.TRUE);

            Categoria c = new Categoria();
            c.setGrupo(g);
            c.setDescricao("Supermercado");
            c.setRegistro(r);
            c.setTipoMovto(TipoMovtoEnum.DEBITO);
            daoCategoria.persist(c);
            System.out.println(c);
            
        } catch (Exception ex) {
            Logger.getLogger(T07_CRUD_Categoria.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void m02_crud_categoria_read() {
        try {
            // busca            
            Categoria c = daoCategoria.findObjeto(1L, Boolean.FALSE);
            System.out.println(c);
        } catch (Exception ex) {
            Logger.getLogger(T07_CRUD_Categoria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void m03_crud_categoria_update() {
        try {
            // busca            
            Categoria c = daoCategoria.findObjeto(1L, Boolean.FALSE);
            System.out.println(c);
            
            // update
            c.setDescricao(c.getDescricao() + " - Atualizada");
            c = daoCategoria.merge(c);
            System.out.println(c);
            
        } catch (Exception ex) {
            Logger.getLogger(T07_CRUD_Categoria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void m04_crud_categoria_delete() {
        try {
            // busca            
            Categoria c = daoCategoria.findObjeto(1L, Boolean.FALSE);
            System.out.println(c);
            
            // delete
            daoCategoria.remove(c);
        } catch (Exception ex) {
            Logger.getLogger(T07_CRUD_Categoria.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
