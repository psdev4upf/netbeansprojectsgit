package gfin.testing;

import gfin.logic.impl.PessoaLogicImpl;
import gfin.logic.impl.UsuarioLogicImpl;
import gfin.model.Pessoa;
import gfin.model.Usuario;
import gfin.service.impl.GenericJpaServiceImpl;
import gfin.utils.PasswordService;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;

/**
 *
 * @author Filipe Benevenuti
 *
 */
public class T05_CRUD_Usuario {

    GenericJpaServiceImpl<Pessoa> daoPessoa = new GenericJpaServiceImpl(new PessoaLogicImpl<Pessoa>(), Pessoa.class);
    GenericJpaServiceImpl<Usuario> daoUsuario = new GenericJpaServiceImpl(new UsuarioLogicImpl<Usuario>(), Usuario.class);

    @Test
    public void m01_crud_usuario_create() {

        try {
            // 1 - busca a pessoa 1
            Pessoa p = daoPessoa.findObjeto(1L, Boolean.TRUE);

            // 2 - cria o usuario 1
            Usuario u = new Usuario();
            u.setNome("superadmin");
            u.setEmail("benevenuti@gmail.com");
            u.setPessoa(p);
            u.setSenha(PasswordService.getInstance().encrypt("superadmin")); // sha-512            
            daoUsuario.persist(u);

        } catch (Exception ex) {
            Logger.getLogger(T05_CRUD_Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void m02_crud_usuario_read() {
        try {
            // busca
            Usuario u = daoUsuario.findObjeto(1L, Boolean.TRUE);
            System.out.println(u);
        } catch (Exception ex) {
            Logger.getLogger(T05_CRUD_Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void m03_crud_usuario_update() {
        try {
            // busca
            Usuario u = daoUsuario.findObjeto(1L, Boolean.TRUE);
            System.out.println(u);

            // update
            u.setNome(u.getNome() + " - Alterado");
            u = daoUsuario.merge(u);
            System.out.println(u);

        } catch (Exception ex) {
            Logger.getLogger(T05_CRUD_Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Test
    public void m04_crud_usuario_delete() {
        try {
            // busca
            Usuario u = daoUsuario.findObjeto(1L, Boolean.TRUE);
            System.out.println(u);

            // delete            
            daoUsuario.remove(u);

        } catch (Exception ex) {
            Logger.getLogger(T05_CRUD_Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
