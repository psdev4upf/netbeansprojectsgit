/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gfin.web.sb;

import gfin.model.Cotacao;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author benevenuti
 */
@Stateless
public class CotacaoFacade extends AbstractFacade<Cotacao> {
    @PersistenceContext(unitName = "GerFinWebPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CotacaoFacade() {
        super(Cotacao.class);
    }
    
}
