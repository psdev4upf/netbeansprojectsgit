/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gfin.web.mb;

/**
 *
 * @author Felipe
 */
import java.io.Serializable;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import org.primefaces.model.chart.BarChartSeries;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.PieChartModel;


 
@Named("graficoController")
@RequestScoped
public class GraficoController implements Serializable {
     
     private PieChartModel pieModel1;
    private CartesianChartModel barModel;
 
    public GraficoController() {
        createPieModels();
    }
 
    public PieChartModel getPieModel1() {
        return pieModel1;
    }
     
    public CartesianChartModel getBarModel() {
        return barModel;
    }
     
    private void createPieModels() {
        createPieModel1();
        createMarModel();
    }
 
    private void createPieModel1() {
        pieModel1 = new PieChartModel();
         
        pieModel1.set("Alimentação", 540.10);
        pieModel1.set("Lazer", 325.00);
        pieModel1.set("Contas", 702.34);
        pieModel1.set("Serviços", 421.12);
         
        //pieModel1.setTitle("Simple Pie");
        //pieModel1.setLegendPosition("w");
    }
     
    private void createMarModel() {
        barModel = new CartesianChartModel();
         
        
        BarChartSeries bcs = new BarChartSeries();
        bcs.set("Cartão de Crédito", 540);
        bcs.set("Aluguel", 325);
        bcs.set("Condomínio", 702);
        bcs.set("Luz", 421);
        bcs.set("IPVA", 956);
        
        bcs.setLabel("Gastos gerais");
        
        barModel.addSeries(bcs);
         
        
    }
    
}