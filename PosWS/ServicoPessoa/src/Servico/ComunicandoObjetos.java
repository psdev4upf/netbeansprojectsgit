/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servico;

import Dados.Pessoa;
import java.rmi.RemoteException;
import org.apache.axis2.AxisFault;
import stubcalculadora.CalculadoraStub;

/**
 *
 * @author rebonatto
 */
public class ComunicandoObjetos {
   private CalculadoraStub servico;
   
   public double addAltura(Pessoa p, double altura) throws AxisFault{
       AxisFault f;
       double ret;
       
       try {
           servico = new CalculadoraStub();
       } catch (AxisFault ex) {
          f = new AxisFault(ex.getMessage());
          throw f; 
       }
       if (p.getCodigo() == 0){
           f = new AxisFault("Objeto pessoa com codigo 0!");
           throw f;           
       }
      
       if (altura == 0){
           f = new AxisFault("Altura 0!");
           throw f;           
       }
       
       CalculadoraStub.Soma vai = new CalculadoraStub.Soma();
       vai.setX(p.getAltura());
       vai.setY(altura);
       try {
           ret = servico.soma(vai).get_return();
       } catch (AxisFault ex){
           f = new AxisFault(ex.getMessage());
           throw f;           
       } catch (RemoteException ex) {
           f = new AxisFault("Falha ao invocar servico");
           throw f;           
       }
       return ret;
   }
   
   public double divAltura(Pessoa p, double altura) throws AxisFault{
       AxisFault f;
       double ret;
       
       try {
           servico = new CalculadoraStub();
       } catch (AxisFault ex) {
          f = new AxisFault(ex.getMessage());
          throw f; 
       }
       if (p.getCodigo() == 0){
           f = new AxisFault("Objeto pessoa com codigo 0!");
           throw f;           
       }
            
       CalculadoraStub.Divide vai = new CalculadoraStub.Divide();
       vai.setX(p.getAltura());
       vai.setY(altura);
       try {
           ret = servico.divide(vai).get_return();
       } catch (AxisFault ex){
           f = new AxisFault(ex.getMessage());
           throw f;           
       } catch (RemoteException ex) {
           f = new AxisFault("Falha ao invocar servico");
           throw f;           
       }
       return ret;
   }
   
   public Pessoa pegaPessoa(){
           Pessoa tmp = new Pessoa();
       	      
           tmp.setCodigo(5);
           tmp.setNome("Web Service");
           tmp.setAltura(3.14);
              
           return tmp;
    }    
   
   public Pessoa modificaPessoa(Pessoa teste){
       Pessoa obj = new Pessoa();

       obj.setCodigo(teste.getCodigo()+1);
       obj.setNome("Nome " + teste.getNome());             
       obj.setAltura(teste.getAltura() < 1.5 ? 1.5 : teste.getAltura());
       
       return (obj);
   }              
}
