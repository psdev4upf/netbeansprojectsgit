/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Calcula;

import org.apache.axis2.AxisFault;

/**
 *
 * @author rebonatto
 */
public class Calculadora {
    
    public double soma(double x, double y){
        return x+y;
    }
    
    public double diminui(double x, double y){
        return x-y;
    }

    public double multiplica(double x, double y){
        return x*y;
    }
    
    public double divide(double x, double y) throws AxisFault{
        if (y == 0){
            AxisFault f = new AxisFault("Divisao por zero");
            throw f ;
        }
        return x/y;
    }
    
    
}
