
import Calcula.Calculadora;
import org.apache.axis2.AxisFault;
import org.junit.Test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rebonatto
 */
public class TestarCalculadora {           
    @Test
    public void TesteCalculadora(){
        Calculadora obj = new Calculadora();
        System.out.println("5 + 4 " + obj.soma(5, 4)); 
        System.out.println("5 - 4 " + obj.diminui(5, 4)); 
        System.out.println("5 * 4 " + obj.multiplica(5, 4)); 
        try { 
            System.out.println("5 / 4 " + obj.divide(5, 4));
            System.out.println("5 / 0 " + obj.divide(5, 0)); 
        } catch (AxisFault ex) {
            System.out.println("Falha AxisFault: " + ex.getMessage());
        }
        
    }
}
