
/**
 * ComunicandoObjetosCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package stubObjetos;

    /**
     *  ComunicandoObjetosCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ComunicandoObjetosCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ComunicandoObjetosCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ComunicandoObjetosCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for modificaPessoa method
            * override this method for handling normal response from modificaPessoa operation
            */
           public void receiveResultmodificaPessoa(
                    stubObjetos.ComunicandoObjetosStub.ModificaPessoaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from modificaPessoa operation
           */
            public void receiveErrormodificaPessoa(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for pegaPessoa method
            * override this method for handling normal response from pegaPessoa operation
            */
           public void receiveResultpegaPessoa(
                    stubObjetos.ComunicandoObjetosStub.PegaPessoaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from pegaPessoa operation
           */
            public void receiveErrorpegaPessoa(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addAltura method
            * override this method for handling normal response from addAltura operation
            */
           public void receiveResultaddAltura(
                    stubObjetos.ComunicandoObjetosStub.AddAlturaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addAltura operation
           */
            public void receiveErroraddAltura(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for divAltura method
            * override this method for handling normal response from divAltura operation
            */
           public void receiveResultdivAltura(
                    stubObjetos.ComunicandoObjetosStub.DivAlturaResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from divAltura operation
           */
            public void receiveErrordivAltura(java.lang.Exception e) {
            }
                


    }
    