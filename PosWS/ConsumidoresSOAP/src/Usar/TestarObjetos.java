/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Usar;

import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.axis2.AxisFault;
import stubObjetos.ComunicandoObjetosStub;
import stubObjetos.ComunicandoObjetosStub.DivAltura;

/**
 *
 * @author rebonatto
 */
public class TestarObjetos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)  {
        // TODO code application logic here
        ComunicandoObjetosStub servico = null;
        try {
            servico = new ComunicandoObjetosStub();
        } catch (AxisFault ex) {
            System.out.println("Erro ao contactar serviço");
        }
        ComunicandoObjetosStub.Pessoa p;
        try {
            p = servico.pegaPessoa(new ComunicandoObjetosStub.PegaPessoa()).get_return();
            DivAltura vai = new DivAltura();                    
            vai.setP(p);
            vai.setAltura(1);            
            System.out.println(servico.divAltura(vai).get_return());
        } catch (AxisFault af){
            System.out.println("Axis Fault " + af.getMessage());
        } catch (RemoteException ex) {
            Logger.getLogger(TestarObjetos.class.getName()).log(Level.SEVERE, null, ex);
        }
            
    }
    
}
