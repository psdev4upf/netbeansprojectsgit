/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Usar;

import java.rmi.RemoteException;
import org.apache.axis2.AxisFault;
import stubCalculadora.CalculadoraStub;

/**
 *
 * @author rebonatto
 */
public class TestarCalculadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CalculadoraStub service = null;
        try {  service = new CalculadoraStub();  } 
        catch (AxisFault ex) {  System.out.println("Erro ao contatar serico"); }         
        try {
            CalculadoraStub.Divide obj = new CalculadoraStub.Divide();
            obj.setX(4);             obj.setY(0);
            System.out.println(service.divide(obj).get_return());
        } catch (AxisFault ex){
            System.out.println("Falha Axis " + ex.getMessage());  
        } catch (RemoteException ex) {
            System.out.println("Falha " + ex.getMessage());
        }
    }   
    
    
}
