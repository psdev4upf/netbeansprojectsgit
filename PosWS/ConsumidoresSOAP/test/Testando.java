
import java.rmi.RemoteException;
import org.apache.axis2.AxisFault;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import stubObjetos.ComunicandoObjetosStub;
import stubObjetos.ComunicandoObjetosStub.DivAltura;
import stubObjetos.ComunicandoObjetosStub.PegaPessoa;
import stubObjetos.ComunicandoObjetosStub.Pessoa;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rebonatto
 */
public class Testando {
    ComunicandoObjetosStub service ;
    
    @Test
    public void RetornaPessoa() throws RemoteException {
        ComunicandoObjetosStub.PegaPessoaResponse p = new ComunicandoObjetosStub.PegaPessoaResponse();
        p = service.pegaPessoa(new ComunicandoObjetosStub.PegaPessoa());
        Pessoa pes = p.get_return();
        System.out.println("Codigo: " + pes.getCodigo() + " Nome: " + pes.getNome() + " altura: " + pes.getAltura()); 
    }
          
    @Test
    public void testeServicos()  {
        try {
            Pessoa p = service.pegaPessoa(new PegaPessoa()).get_return();
            DivAltura vai = new DivAltura();                    
            vai.setP(p);
            vai.setAltura(0);
            //System.out.println(p.getAltura());
            System.out.println(service.divAltura(vai).get_return());
        } catch (AxisFault f){
            System.out.println("Falha no swervio " + f.getMessage());
        } catch (RemoteException ex) {
            System.out.println("Erro!");
        }        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws AxisFault {
         service = new ComunicandoObjetosStub();
    }
    
    @After
    public void tearDown() {
    }
}
