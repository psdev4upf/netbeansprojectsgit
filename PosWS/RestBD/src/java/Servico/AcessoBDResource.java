/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servico;

import br.upf.estacoes.model.beans.Estado;
import br.upf.estacoes.model.logic.EstadoLogicImpl;
import br.upf.estacoes.model.services.GenericJpaServiceImpl;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * REST Web Service
 *
 * @author rebonatto
 */
@Path("AcessoBD")
public class AcessoBDResource {
    GenericJpaServiceImpl<Estado> dao = new GenericJpaServiceImpl(
                         new EstadoLogicImpl<Estado>(), Estado.class); 
       
    @GET
    @Path("Estados")
    @Produces(MediaType.TEXT_PLAIN)
    public String listarSiglaEstados() throws Exception{        
        String ret = new String();
        for(Estado o : dao.findObjetos(Boolean.TRUE))
            ret += o.getSigla() + " | ";
        
        return ret; //"Yes";
    }
       
   
    @Context
    private UriInfo context;

    /**
     * Creates a new instance of AcessoBDResource
     */
    public AcessoBDResource() {
    }

    /**
     * Retrieves representation of an instance of Servico.AcessoBDResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/xml")
    public String getXml() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    /**
     * PUT method for updating or creating an instance of AcessoBDResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/xml")
    public void putXml(String content) {
    }
}
