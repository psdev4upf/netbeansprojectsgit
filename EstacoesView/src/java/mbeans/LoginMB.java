/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mbeans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Filipe
 */
@ManagedBean
@SessionScoped
public class LoginMB {
    private String usr;
    private String pwd;

    public LoginMB() {
    }
    
    public String valida(){
        if (usr.equals("pos") && (pwd.equals("pos"))){
            return "bemvindo?faces-redirect=true";
        } else {
            return "errologin?faces-redirect=true";
        }
    }

    public String getUsr() {
        return usr;
    }

    public void setUsr(String usr) {
        this.usr = usr;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }
    
    
    
}
