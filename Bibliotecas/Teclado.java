/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Teclado {

    public static String lerString(String msg) {
        System.out.print(msg);
        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        try {
            return br.readLine();
        } catch (IOException ex) {
            System.out.println("String inválida!");
            return lerString(msg);
        }
    }

    public static int lerInt(String msg) {
        String lido = lerString(msg);
        try {
            return Integer.parseInt(lido);
        } catch (Exception ex) {
            System.out.println("Valor inteiro inválido!");
            return lerInt(msg);
        }
    }

    public static double lerDouble(String msg) {
        String lido = lerString(msg);
        try {
            return Double.parseDouble(lido);
        } catch (Exception ex) {
            System.out.println("Valor double inválido!");
            return lerInt(msg);
        }
    }

    public static float lerFloat(String msg) {
        String lido = lerString(msg);
        try {
            return Float.parseFloat(lido + "F");
        } catch (Exception ex) {
            System.out.println("Valor float inválido!");
            return lerInt(msg);
        }
    }
}
