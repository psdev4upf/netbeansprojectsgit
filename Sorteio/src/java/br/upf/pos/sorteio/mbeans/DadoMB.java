/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.sorteio.mbeans;

import br.upf.pos.sorteio.sbeans.LancadorDeDadoBean;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Felipe
 */
@ManagedBean
@RequestScoped
public class DadoMB {

    @EJB
    private LancadorDeDadoBean lancadorDeDadoBean;
    
    private int resultado;
    /**
     * Creates a new instance of DadoMB
     */
    public DadoMB() {
    }

    public void lancaDado() {
        resultado = lancadorDeDadoBean.lanca();
    }
    
    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }
}
