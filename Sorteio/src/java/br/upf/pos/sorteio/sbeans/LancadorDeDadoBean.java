/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.sorteio.sbeans;

import javax.ejb.Stateless;

/**
 *
 * @author Felipe
 */
@Stateless
public class LancadorDeDadoBean {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    public int lanca() {
        return (int)Math.floor(Math.random()*6)+1;
    }
}
