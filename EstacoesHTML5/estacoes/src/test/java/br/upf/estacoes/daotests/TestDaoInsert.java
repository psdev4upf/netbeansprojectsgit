package br.upf.estacoes.daotests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.net.UnknownHostException;

import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import br.upf.estacoes.persistence.implementation.DaoMongoDB;

public class TestDaoInsert {
/*
    private static DB db = null;

    @BeforeClass
    public static void setup() {

        try {

            db = TestDaoInsert.getDb();

            DBCollection estadoColl = db.getCollection("estados");
            DBObject estadoIndex = new BasicDBObject();
            estadoIndex.put("sigla", 1);
            estadoColl.ensureIndex(estadoIndex);
            BasicDBObject estado1 = new BasicDBObject();
            estado1.put("nome", "Rio Grande do Sul");
            estado1.put("sigla", "RS");
            estadoColl.insert(estado1);

            DBCollection cidadesColl = db.getCollection("cidades");
            DBObject cidadeIndex = new BasicDBObject();
            cidadeIndex.put("estado_id", 1);
            cidadeIndex.put("nome", -1);
            cidadesColl.ensureIndex(cidadeIndex);
            BasicDBObject cidade1 = new BasicDBObject();
            cidade1.put("estado_id", estado1.get("_id"));
            cidade1.put("nome", "Porto Alegre");
            cidadesColl.insert(cidade1);

        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    private static DB getDb() throws UnknownHostException {
        DB db = null;
        MongoClient mongoClient;
        String server = "localhost";
        int port = 27017;
        if (System.getProperty("dbServer") != null) {
            server = System.getProperty("dbServer");
        }
        if (System.getProperty("dbServerPort") != null) {
            port = Integer.parseInt(System.getProperty("dbServerPort"));
        }

        mongoClient = new MongoClient(server, port);
        db = mongoClient.getDB("estacoes");
        db.dropDatabase();

        db = null;
        mongoClient.close();
        mongoClient = new MongoClient(server, port);
        db = mongoClient.getDB("estacoes");
        return db;
    }

    @Test
    public void testInsereEstado() throws Exception {
        DaoMongoDB dao = new DaoMongoDB();
        JSONObject estadoJson = new JSONObject(
                "{"
                + "sigla : " + "SC" + ","
                + "nome : " + "Santa Catarina"
                + "}"
        );
        boolean resposta = dao.insereEstado(estadoJson);
        String sigla = estadoJson.get("sigla").toString();
        String nome = estadoJson.get("nome").toString();
        if (resposta) {
            estadoJson = null;
            estadoJson = dao.retornaEstado(sigla);
            assertTrue(estadoJson != null);
            assertTrue(estadoJson.get("sigla").equals(sigla)
                    && estadoJson.get("nome").equals(nome));
        }

        estadoJson = dao.retornaEstado("SC");
        assertTrue(estadoJson.get("nome").equals("Santa Catarina"));

        resposta = dao.insereEstado(estadoJson);
        assertFalse(resposta);
    }

    @Test
    public void testInsereCidade() throws Exception {
        DaoMongoDB dao = new DaoMongoDB();
        JSONObject cidadeJson = new JSONObject(
                "{"
                + "nome : " + "Florianópolis"
                + "}"
        );
        JSONObject estadoJson = new JSONObject(
                "{"
                + "sigla : " + "SC" + ","
                + "nome : " + "Santa Catarina"
                + "}"
        );
        boolean resposta = dao.insereCidade(cidadeJson, estadoJson);
        String nome = cidadeJson.get("nome").toString();
        if (resposta) {
            cidadeJson = null;
            cidadeJson = dao.retornaCidade(nome);
            assertTrue(cidadeJson != null);
            assertTrue(cidadeJson.get("nome").equals(nome));
        }

        cidadeJson = dao.retornaCidade("Florianópolis");
        assertTrue(cidadeJson.get("nome").equals("Florianópolis"));

        resposta = dao.insereCidade(cidadeJson, estadoJson);
        assertFalse(resposta);
    }
    
    */

}
