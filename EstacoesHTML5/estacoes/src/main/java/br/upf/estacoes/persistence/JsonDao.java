package br.upf.estacoes.persistence;

import java.util.Date;
import java.util.List;

import org.json.JSONObject;

public interface JsonDao {

    boolean insereCidade(JSONObject cidade, JSONObject estado) throws Exception;

    boolean insereEstado(JSONObject estado) throws Exception;

    boolean insereEstacao(JSONObject estacao, JSONObject cidade) throws Exception;

    boolean insereVariavel(JSONObject variavel, JSONObject estacao) throws Exception;
    
    public JSONObject editCidade(JSONObject cidade) throws Exception;

    public JSONObject editEstado(JSONObject estado) throws Exception;

    public JSONObject editEstacao(JSONObject estacao) throws Exception;

    public JSONObject editVariavel(JSONObject variavel) throws Exception;    

    public JSONObject retornaEstado(String id) throws Exception;

    public JSONObject retornaEstacao(String id) throws Exception;

    public JSONObject retornaCidade(String id) throws Exception;

    public JSONObject retornaVariavel(String id) throws Exception;

    public List<JSONObject> listCidades() throws Exception;

    public List<JSONObject> listEstados() throws Exception;

    public List<JSONObject> listEstacoes() throws Exception;

    public List<JSONObject> listVariaveis() throws Exception;

    public boolean removeCidade(JSONObject cidade, JSONObject estado) throws Exception;

    public boolean removeEstado(JSONObject estado) throws Exception;

    public boolean removeVariavel(JSONObject variavel, JSONObject estacao) throws Exception;

    public boolean removeEstacao(JSONObject estacao, JSONObject cidade) throws Exception;

    public JSONObject retornaLeituraVariavel(JSONObject cidade, JSONObject estacao, JSONObject variavel, Date dia) throws Exception;

    public boolean insereLeituraEstacao(JSONObject cidade, JSONObject estacao, JSONObject variavel, Date dia, Double valor) throws Exception;

}
