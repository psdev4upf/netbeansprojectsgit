package br.upf.estacoes.persistence.implementation;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;
import br.upf.estacoes.persistence.JsonDao;
import com.mongodb.MongoException.*;
import com.mongodb.WriteResult;

public class DaoMongoDB implements JsonDao {
    
    private DB db;
    private MongoClient mongoClient;
    
    private DBObject convert(JSONObject object) {
        Object obj = JSON.parse(object.toString());
        DBObject dbUser = (DBObject) obj;
        return dbUser;
    }
    
    private void connectToDatabase() {
        if (this.db == null) {
            try {
                String server = "localhost";
                int port = 27017;
                if (System.getProperty("dbServer") != null) {
                    server = System.getProperty("dbServer");
                }
                if (System.getProperty("dbServerPort") != null) {
                    port = Integer.parseInt(System.getProperty("dbServerPort"));
                }
                
                mongoClient = new MongoClient(server, port);
                this.db = mongoClient.getDB("estacoes");
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
            
        }
    }
    
    private void closeDb() {
        db = null;
        mongoClient.close();
    }
    
    public DaoMongoDB() {
        super();
    }
    
    @Override
    public boolean insereEstado(JSONObject estado) throws Exception {
        boolean returnCode = false;
        connectToDatabase();
        
        DBCollection collection = db.getCollection("estados");
        estado.put("id", getNextID(collection));
        DBObject dbEstado = this.convert(estado);
        try {
            //collection.ensureIndex(new BasicDBObject("sigla", 1), "siglaIdx", true);
            collection.insert(dbEstado);
            returnCode = true;
        } catch (DuplicateKey dupl) {
            returnCode = false;
            dupl.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            this.closeDb();
        }
        return returnCode;
    }
    
    @Override
    public boolean insereCidade(JSONObject cidade, JSONObject estado) throws Exception {
        boolean returnCode = false;
        connectToDatabase();
        
        DBObject dbEstado = this.selecionaEstado(estado.getString("id"));
        DBCollection collection = db.getCollection("cidades");
        cidade.put("id", getNextID(collection));
        DBObject dbCidade = this.convert(cidade);
        
        try {
            DBObject cidadeIndex = new BasicDBObject();
            cidadeIndex.put("estado_id", 1);
            cidadeIndex.put("id", -1);
            collection.ensureIndex(cidadeIndex);
            
            dbCidade.put("estado_id", dbEstado.get("id"));
            collection.insert(dbCidade);
            returnCode = true;
        } catch (DuplicateKey dupl) {
            returnCode = false;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            this.closeDb();
        }
        return returnCode;
    }
    
    @Override
    public boolean insereEstacao(JSONObject estacao, JSONObject cidade) throws Exception {
        boolean returnCode = false;
        connectToDatabase();
        
        DBObject dbCidade = this.convert(cidade);
        DBCollection collection = db.getCollection("estacoes");
        estacao.put("id", getNextID(collection));
        DBObject dbEstacao = this.convert(estacao);
        try {
            DBObject estacaoIndex = new BasicDBObject();
            estacaoIndex.put("cidade_id", 1);
            estacaoIndex.put("nome", -1);
            collection.ensureIndex(estacaoIndex);
            dbEstacao.put("cidade_id", dbCidade.get("_id"));
            collection.insert(dbEstacao);
            returnCode = true;
        } catch (DuplicateKey dupl) {
            returnCode = false;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            this.closeDb();
        }
        return returnCode;
    }
    
    @Override
    public boolean insereVariavel(JSONObject variavel, JSONObject estacao) throws Exception {
        boolean returnCode = false;
        connectToDatabase();
        
        DBObject dbEstacao = this.convert(estacao);
        DBCollection collection = db.getCollection("estacoes");
        variavel.put("id", getNextID(collection));
        DBObject dbVariavel = this.convert(variavel);
        try {
            DBObject variavelIndex = new BasicDBObject();
            variavelIndex.put("cidade_id", 1);
            variavelIndex.put("nome", -1);
            collection.ensureIndex(variavelIndex);
            dbVariavel.put("cidade_id", dbEstacao.get("_id"));
            collection.insert(dbVariavel);
            returnCode = true;
        } catch (DuplicateKey dupl) {
            returnCode = false;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            this.closeDb();
        }
        return returnCode;
    }
    
    @Override
    public JSONObject retornaLeituraVariavel(JSONObject cidade, JSONObject estacao, JSONObject variavel, Date dia) throws Exception {
        JSONObject user = null;
        try {
            DBObject dBuser = null;
            connectToDatabase();
            DBCollection collection = db.getCollection("leituras");
            BasicDBObject query = new BasicDBObject();
            query.put("cidade", cidade);
            query.put("estacao", estacao);
            query.put("variavel", variavel);
            query.put("dia", dia);
            DBCursor cursor = collection.find(query);
            if (cursor.hasNext()) {
                cursor.next();
                dBuser = cursor.curr();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            this.closeDb();
        }
        
        return user;
    }
    
    @Override
    public boolean insereLeituraEstacao(JSONObject cidade, JSONObject estacao, JSONObject variavel, Date dia, Double valor) throws Exception {
        boolean returnCode = false;
        connectToDatabase();
        DBObject dbCidade = this.convert(cidade);
        DBObject dbEstacao = this.convert(estacao);
        DBObject dbVariavel = this.convert(variavel);
        DBObject dbLeitura = new BasicDBObject();
        DBCollection collection = db.getCollection("leituras");
        try {
            DBObject leituraIndex = new BasicDBObject();
            leituraIndex.put("cidade_id", 1);
            leituraIndex.put("estacao_id", 1);
            leituraIndex.put("variavel_id", 1);
            leituraIndex.put("dia", -1);
            collection.ensureIndex(leituraIndex);
            dbLeitura.put("cidade_id", dbEstacao.get("_id"));
            dbLeitura.put("estacao_id", dbEstacao.get("_id"));
            dbLeitura.put("variavel_id", dbEstacao.get("_id"));
            dbLeitura.put("dia", dia);
            dbLeitura.put("valor", valor);
            collection.insert(dbLeitura);
            returnCode = true;
        } catch (DuplicateKey dupl) {
            returnCode = false;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            this.closeDb();
        }
        return returnCode;
    }
    
    @Override
    public JSONObject retornaEstado(String id) throws Exception {
        return getObj("id", id, "estados");
    }
    
    private DBObject selecionaEstado(String id) throws Exception {
        DBObject dbEstado = null;
        connectToDatabase();
        DBCollection collection = db.getCollection("estados");
        BasicDBObject query = new BasicDBObject("id", id);
        DBCursor cursor = collection.find(query);
        if (cursor.hasNext()) {
            cursor.next();
            dbEstado = cursor.curr();
            
        }
        return dbEstado;
    }
    
    @Override
    public JSONObject retornaCidade(String id) throws Exception {
        return getObj("id", id, "cidades");
    }
    
    @Override
    public boolean removeCidade(JSONObject cidade, JSONObject estado) throws Exception {
        boolean returnCode = false;
        connectToDatabase();
        
        DBObject dbEstado = this.selecionaEstado(estado.getString("sigla"));
        DBCollection collection = db.getCollection("cidades");
        
        try {
            BasicDBObject query = new BasicDBObject();
            query.put("estado_id", dbEstado.get("_id"));
            query.put("nome", cidade.get("nome"));
            collection.remove(query);
            returnCode = true;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            this.closeDb();
        }
        return returnCode;
    }
    
    @Override
    public boolean removeEstado(JSONObject estado) throws Exception {
        boolean returnCode = false;
        connectToDatabase();
        
        try {
            DBCollection collection = db.getCollection("estados");
            
            BasicDBObject query = new BasicDBObject();
            query.put("sigla", estado.getString("sigla"));
            collection.remove(query);
            
            returnCode = true;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            this.closeDb();
        }
        return returnCode;
    }
    
    @Override
    public JSONObject retornaEstacao(String id) throws Exception {
        return getObj("id", id, "estacoes");
    }
    
    @Override
    public boolean removeVariavel(JSONObject variavel, JSONObject estacao) throws Exception {
        boolean returnCode = false;
        connectToDatabase();
        
        DBObject dbEstacao = this.selecionaEstacao(estacao.getString("id"));
        DBCollection collection = db.getCollection("variaveis");
        
        try {
            BasicDBObject query = new BasicDBObject();
            query.put("estacao_id", dbEstacao.get("id"));
            query.put("id", variavel.get("id"));
            collection.remove(query);
            returnCode = true;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            this.closeDb();
        }
        return returnCode;
    }
    
    private DBObject selecionaEstacao(String id) throws Exception {
        DBObject dbEstacao = null;
        connectToDatabase();
        DBCollection collection = db.getCollection("estacoes");
        BasicDBObject query = new BasicDBObject("id", id);
        DBCursor cursor = collection.find(query);
        if (cursor.hasNext()) {
            cursor.next();
            dbEstacao = cursor.curr();
            
        }
        return dbEstacao;
    }
    
    @Override
    public boolean removeEstacao(JSONObject estacao, JSONObject cidade) throws Exception {
        boolean returnCode = false;
        connectToDatabase();
        
        DBObject dbCidade = this.selecionaCidade(cidade.getString("id"));
        DBCollection collection = db.getCollection("estacoes");
        
        try {
            BasicDBObject query = new BasicDBObject();
            query.put("cidade_id", dbCidade.get("_id"));
            query.put("id", estacao.get("id"));
            collection.remove(query);
            returnCode = true;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            this.closeDb();
        }
        return returnCode;
    }
    
    private DBObject selecionaCidade(String id) {
        DBObject dbCidade = null;
        connectToDatabase();
        DBCollection collection = db.getCollection("cidades");
        BasicDBObject query = new BasicDBObject("id", id);
        DBCursor cursor = collection.find(query);
        if (cursor.hasNext()) {
            cursor.next();
            dbCidade = cursor.curr();
            
        }
        return dbCidade;
    }
    
    @Override
    public List<JSONObject> listCidades() throws Exception {
        return listTable("cidade");
    }
    
    @Override
    public List<JSONObject> listEstados() throws Exception {
        return listTable("estados");
    }
    
    @Override
    public List<JSONObject> listEstacoes() throws Exception {
        return listTable("estacoes");
    }
    
    @Override
    public List<JSONObject> listVariaveis() throws Exception {
        return listTable("variaveis");
    }
    
    private List<JSONObject> listTable(String table) throws Exception {
        List<JSONObject> lst = new ArrayList<JSONObject>();
        try {
            connectToDatabase();
            DBCollection col = db.getCollection(table);
            DBCursor cur = col.find();
            
            while (cur.hasNext()) {
                cur.next();
                JSONObject obj = new JSONObject(cur.curr().toString());
                lst.add(obj);
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            this.closeDb();
        }
        
        return lst;
    }
    
    @Override
    public JSONObject retornaVariavel(String id) throws Exception {
        return getObj("id", id, "variavel");
    }
    
    private JSONObject getObj(String idName, String idValue, String tableName) throws Exception {
        JSONObject jObj = null;
        try {
            DBObject dbObj = null;
            connectToDatabase();
            DBCollection collection = db.getCollection(tableName);
            BasicDBObject query = new BasicDBObject(idName, idValue);
            DBCursor cursor = collection.find(query);
            if (cursor.hasNext()) {
                cursor.next();
                dbObj = cursor.curr();
                jObj = new JSONObject(dbObj.toString());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex);
        } finally {
            this.closeDb();
        }
        
        return jObj;
    }
    
    private String getNextID(DBCollection col) {
        String res;
        int c = 0;
        try {
            c = (int) col.getCount();            
        } catch (Exception e) {
            
        }
        c++;
        res = String.valueOf(c);
        return res;
    }
    
    @Override
    public JSONObject editEstado(JSONObject estado) throws Exception {
        return editObj(estado, "estados");
    }
    
    @Override
    public JSONObject editEstacao(JSONObject estacao) throws Exception {
        return editObj(estacao, "estacoes");
    }
    
    @Override
    public JSONObject editCidade(JSONObject cidade) throws Exception {
        return editObj(cidade, "cidades");
    }
    
    @Override
    public JSONObject editVariavel(JSONObject variavel) throws Exception {
        return editObj(variavel, "variaveis");
    }
    
    private JSONObject editObj(JSONObject obj, String tableName) throws Exception {
        JSONObject res = null;
        try {
            DBObject dbObj = convert(obj);
            connectToDatabase();
            
            DBCollection collection = db.getCollection(tableName);
            BasicDBObject query = new BasicDBObject("id", obj.getString("id"));
            WriteResult wr = collection.update(query, dbObj, false, false);
            res = obj;
        } catch (Exception ex) {
            ex.printStackTrace();
            res = null;
            throw new Exception(ex);
            
        } finally {
            this.closeDb();
            
        }
        return res;
    }
    
}
