package br.upf.estacoes.main;

import br.upf.estacoes.persistence.JsonDao;
import br.upf.estacoes.persistence.implementation.DaoMongoDB;
import com.google.gson.Gson;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;
import org.json.JSONObject;

/**
 * RESTful Web service que recebe parâmetros JSON e se comunica com o DAO
 * MongoDB. Roda com um container Jetty embutido.
 *
 * @author Cleuton Sampaio
 *
 */
@Path("/server")
public class Estacoes {

    private JsonDao dao = new DaoMongoDB();
    private static Server server;

    /**
     * Adiciona um novo estado.
     *
     * @param estado JSON String
     * @return JSONObject estado.
     */
    @POST
    @Path("addEstado")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addEstado(String estado) {
        String resposta = null;
        try {
            JSONObject jsonEstado = new JSONObject(estado);
            if (dao.insereEstado(jsonEstado)) {
                jsonEstado = dao.retornaEstado(jsonEstado.getString("id"));
                resposta = jsonEstado.toString();
            } else {
                resposta = this.formatError("Erro adicionando estado").toString();
            }
        } catch (Exception ex) {
            resposta = this.formatError("Exception ao inserir estado: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Adiciona uma nova estacao.
     *
     * @param estacao JSON String
     * @return JSONObject estacao.
     */
    @POST
    @Path("addEstacao")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addEstacao(String estacao) {
        String resposta = null;
        try {
            JSONObject jsonEstacao = new JSONObject(estacao);
            JSONObject jsonCidade = jsonEstacao.getJSONObject("cidade");
            if (dao.insereEstacao(jsonEstacao, jsonCidade)) {
                jsonEstacao = dao.retornaEstacao(jsonEstacao.getString("id"));
                resposta = jsonEstacao.toString();
            } else {
                resposta = this.formatError("Erro adicionando estacao").toString();
            }
        } catch (Exception ex) {
            resposta = this.formatError("Exception ao inserir estacao: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Adiciona uma nova cidade.
     *
     * @param cidade JSON String
     * @return JSONObject cidade.
     */
    @POST
    @Path("addCidade")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addCidade(String cidade) {
        String resposta = null;
        try {
            JSONObject jsonCidade = new JSONObject(cidade);
            JSONObject jsonEstado = jsonCidade.getJSONObject("estado");
            if (dao.insereCidade(jsonCidade, jsonEstado)) {
                jsonCidade = dao.retornaCidade(jsonCidade.getString("nome"));
                resposta = jsonCidade.toString();
            } else {
                resposta = this.formatError("Erro adicionando cidade").toString();
            }
        } catch (Exception ex) {
            resposta = this.formatError("Exception ao inserir cidade: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Adiciona uma nova variável da estação.
     *
     * @param variavel JSON String
     * @return JSONObject variavel.
     */
    @POST
    @Path("addVariavel")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String addVariavel(String variavel) {
        String resposta = null;
        try {
            JSONObject jsonVariavel = new JSONObject(variavel);
            JSONObject jsonEstacao = jsonVariavel.getJSONObject("estacao");
            if (dao.insereEstacao(jsonVariavel, jsonEstacao)) {
                jsonVariavel = dao.retornaEstacao(jsonVariavel.getString("id"));
                resposta = jsonVariavel.toString();
            } else {
                resposta = this.formatError("Erro adicionando variável").toString();
            }
        } catch (Exception ex) {
            resposta = this.formatError("Exception ao inserir variável: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Remove uma cidade.
     *
     * @param cidade JSON String
     * @return JSONObject cidade.
     */
    @POST
    @Path("removeCidade")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String removeCidade(String cidade) {
        String resposta = null;
        try {
            JSONObject jsonCidade = new JSONObject(cidade);
            JSONObject jsonEstado = jsonCidade.getJSONObject("estado");
            if (dao.removeCidade(jsonCidade, jsonEstado)) {
                resposta = "ok";
            } else {
                resposta = this.formatError("Erro removendo cidade").toString();
            }
        } catch (Exception ex) {
            resposta = this.formatError("Exception ao remover cidade: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Remove uma estação.
     *
     * @param estacao JSON String
     * @return JSONObject cidade.
     */
    @POST
    @Path("removeEstacao")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String removeEstacao(String estacao) {
        String resposta = null;
        try {
            JSONObject jsonEstacao = new JSONObject(estacao);
            JSONObject jsonCidade = jsonEstacao.getJSONObject("cidade");
            if (dao.removeEstacao(jsonEstacao, jsonCidade)) {
                resposta = "ok";
            } else {
                resposta = this.formatError("Erro removendo estação").toString();
            }
        } catch (Exception ex) {
            resposta = this.formatError("Exception ao remover estação: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Remove um estado.
     *
     * @param estado JSON String
     * @return JSONObject estado.
     */
    @POST
    @Path("removeEstado")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String removeEstado(String estado) {
        String resposta = null;
        try {
            JSONObject jsonEstado = new JSONObject(estado);
            if (dao.removeEstado(jsonEstado)) {
                resposta = "ok";
            } else {
                resposta = this.formatError("Erro removendo estado").toString();
            }
        } catch (Exception ex) {
            resposta = this.formatError("Exception ao remover estado: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Remove uma variável.
     *
     * @param variavel JSON String
     * @return JSONObject variavel.
     */
    @POST
    @Path("removeVariavel")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String removeVariavel(String variavel) {
        String resposta = null;
        try {
            JSONObject jsonVariavel = new JSONObject(variavel);
            JSONObject jsonEstacao = jsonVariavel.getJSONObject("estacao");
            if (dao.removeVariavel(jsonVariavel, jsonEstacao)) {
                resposta = "ok";
            } else {
                resposta = this.formatError("Erro removendo variável").toString();
            }
        } catch (Exception ex) {
            resposta = this.formatError("Exception ao remover variável: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Retorna a lista completa de cidades.
     *
     * @return resposta
     */
    @GET
    @Path("listCidade")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String listCidade() {
        String resposta = null;

        try {
            List<JSONObject> l = dao.listCidades();

            if (l != null) {
                Gson gson = new Gson();
                resposta = gson.toJson(l);
            }

        } catch (Exception ex) {
            resposta = this.formatError("Exception ao obter a lista de cidades: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Retorna a lista completa de estados.
     *
     * @return resposta
     */
    @GET
    @Path("listEstado")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String listEstado() {
        String resposta = null;

        try {
            List<JSONObject> l = dao.listEstados();

            if (l != null) {
                Gson gson = new Gson();
                resposta = gson.toJson(l);
            }

        } catch (Exception ex) {
            resposta = this.formatError("Exception ao obter a lista de estados: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Retorna a lista completa de estações.
     *
     * @return resposta
     */
    @GET
    @Path("listEstacao")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String listEstacao() {
        String resposta = null;

        try {
            List<JSONObject> l = dao.listEstacoes();

            if (l != null) {
                Gson gson = new Gson();
                resposta = gson.toJson(l);
            }

        } catch (Exception ex) {
            resposta = this.formatError("Exception ao obter a lista de estações: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Retorna a lista completa de variáveis.
     *
     * @return resposta
     */
    @GET
    @Path("listVariavel")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String listVariavel() {
        String resposta = null;

        try {
            List<JSONObject> l = dao.listVariaveis();

            if (l != null) {
                Gson gson = new Gson();
                resposta = gson.toJson(l);
            }

        } catch (Exception ex) {
            resposta = this.formatError("Exception ao obter a lista de variáveis: " + ex.toString()).toString();
        }
        return resposta;
    }

    private JSONObject formatError(String message) {
        JSONObject errorMsg = new JSONObject();
        errorMsg.put("status", "*** ERRO ***");
        errorMsg.put("mensagem", message);
        return errorMsg;
    }

    public static void main(String[] args) throws Exception {
        server = new Server(8080);
        WebAppContext context = new WebAppContext();
        context.setDescriptor("src/main/webapp/WEB-INF/web.xml");
        context.setResourceBase("src/main/webapp");
        context.setContextPath("/");
        context.setParentLoaderPriority(true);
        server.setHandler(context);
        server.start();
        server.join();

    }

    /**
     * Retorna uma cidade.
     *
     * @param id Id da cidade
     * @return resposta
     */
    @GET
    @Path("getCidade/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getCidade(@PathParam("id") String id) {
        String resposta = null;

        try {
            JSONObject o = dao.retornaCidade(id);

            if (o != null) {
                Gson gson = new Gson();
                resposta = gson.toJson(o);
            }

        } catch (Exception ex) {
            resposta = this.formatError("Exception ao obter a cidade: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Retorna um estado.
     *
     * @param id Id do estado
     * @return resposta
     */
    @GET
    @Path("getEstado/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getEstado(@PathParam("id") String id) {
        String resposta = null;

        try {
            JSONObject o = dao.retornaEstado(id);

            if (o != null) {
                Gson gson = new Gson();
                resposta = gson.toJson(o);
            }

        } catch (Exception ex) {
            resposta = this.formatError("Exception ao obter o estado: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Retorna uma estacao.
     *
     * @param id Id da estacao
     * @return resposta
     */
    @GET
    @Path("getEstacao/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getEstacao(@PathParam("id") String id) {
        String resposta = null;

        try {
            JSONObject o = dao.retornaEstacao(id);

            if (o != null) {
                Gson gson = new Gson();
                resposta = gson.toJson(o);
            }

        } catch (Exception ex) {
            resposta = this.formatError("Exception ao obter a estação: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Retorna uma variável.
     *
     * @param id Id da variável
     * @return resposta
     */
    @GET
    @Path("getVariavel")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String getVariavel(@PathParam("id") String id) {
        String resposta = null;

        try {
            JSONObject o = dao.retornaVariavel(id);

            if (o != null) {
                Gson gson = new Gson();
                resposta = gson.toJson(o);
            }

        } catch (Exception ex) {
            resposta = this.formatError("Exception ao obter a variável: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Edita uma cidade.
     *
     * @param cidade Objeto cidade em json
     * @return resposta
     */
    @POST
    @Path("editCidade")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String editCidade(String cidade) {
        String resposta = null;

        try {
            JSONObject jsonObj = new JSONObject(cidade);
            JSONObject o = dao.editCidade(jsonObj);

            if (o != null) {
                Gson gson = new Gson();
                resposta = gson.toJson(o);
            }

        } catch (Exception ex) {
            resposta = this.formatError("Exception ao editar a cidade: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Edita um estado.
     *
     * @param estado Objeto estado em json
     * @return resposta
     */
    @POST
    @Path("editEstado")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String editEstado(String estado) {
        String resposta = null;

        try {
            JSONObject jsonObj = new JSONObject(estado);
            JSONObject o = dao.editEstado(jsonObj);

            if (o != null) {
                Gson gson = new Gson();
                resposta = gson.toJson(o);
            }

        } catch (Exception ex) {
            resposta = this.formatError("Exception ao editar o estado: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Edita uma variável.
     *
     * @param variavel Objeto variavel em json
     * @return resposta
     */
    @POST
    @Path("editVariavel")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String editVariavel(String variavel) {
        String resposta = null;

        try {
            JSONObject jsonObj = new JSONObject(variavel);
            JSONObject o = dao.editVariavel(jsonObj);

            if (o != null) {
                Gson gson = new Gson();
                resposta = gson.toJson(o);
            }

        } catch (Exception ex) {
            resposta = this.formatError("Exception ao editar a variável: " + ex.toString()).toString();
        }
        return resposta;
    }

    /**
     * Edita uma estação.
     *
     * @param estacao Objeto estacao em json
     * @return resposta
     */
    @POST
    @Path("editEstacao")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String editEstacao(String estacao) {
        String resposta = null;

        try {
            JSONObject jsonObj = new JSONObject(estacao);
            JSONObject o = dao.editEstacao(jsonObj);

            if (o != null) {
                Gson gson = new Gson();
                resposta = gson.toJson(o);
            }

        } catch (Exception ex) {
            resposta = this.formatError("Exception ao editar a estação: " + ex.toString()).toString();
        }
        return resposta;
    }

}
