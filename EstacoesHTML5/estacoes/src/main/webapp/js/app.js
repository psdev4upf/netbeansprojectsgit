var wait;
wait = wait || (function () {
    var pleaseWaitDiv = $('<div class="modal hide" id="pleaseWaitDialog" role="dialog" data-backdrop="static" data-keyboard="false" style="z-index:1000px;"><div class="modal-dialog"><div class="modal-body" style="text-align:center;"><img src="img/loading.gif" width="100"/></div></div></div>');
    return {
        show: function() {
            pleaseWaitDiv.modal();
        },
        hide: function () {
            pleaseWaitDiv.modal('hide');
        }
    };
})();

function rxGet(url, data, success, always, type) {
    
    var options = {
        url: url,
        data: JSON.stringify(data),
        success: function(data, textStatus) {
            if(data) {
                if(data.status) {
                    if(data.status === "*** ERRO ***") {
                        alert(data.mensagem);
                        return;
                    }
                }
            }
            
            if(typeof success === "function") {
                success(data, textStatus);
            }
        },
        dataType: "json",
        contentType: 'application/json'
    };
    
    if(type !== null) {
        options.type = type;
    }
    
    if(typeof always !== "function") {
        always = function(data, textStatus) {};
    }
    
    $.ajax(options).fail(function(jqXHR, textStatus) {
        console.log("Erro!!!");
        console.dir(jqXHR);
        alert("Erro: "+textStatus);
    })
    .always(always);
    
}

function rxPost(url, data, success, always) {
    rxGet(url, data, success, always, "POST");
}


$(function(){
    
    //$("#pleaseWaitDialog").modal('show');
    
    //$("#pleaseWaitDialog").modal("hide");
    
    $( "#frmCad" ).on( "click", ".tabCrud", function(e) {
        e.preventDefault();
        $(this).tab('show');
        
        $(".crud").find("form").find("input,textarea,select").each(function(index, element){
            $(element).val("");
        });
        
    });  
    
    $('#frmCad').on('hidden.bs.modal', function (e) {
        $(this).removeData('bs.modal');
    });
    
    
    $("#frmCad").on( "click", ".btnEdit", function(e) {
        e.preventDefault();
        var l = Ladda.create(this);
        l.start();
        
        var entidade = $(this).parents(".crud").attr('id');
        var indiceId = $(this).parents("table").find("th").index($(this).parents("table").find("#id"));
        var parametro = {};
        parametro["id"] = $($(this).parents("tr").children()[indiceId]).html();

        var url = "get"+entidade.charAt(0).toUpperCase()+entidade.substr(1);        

        rxGet("/estacoes/server/"+url+"/"+parametro.id, null, function(data){
            $(".tabCrud a:last").tab('show');        

            $("#btnCrudSalvar").parents("form").find("input[id],textarea[id],select[id]").each(function(index, element){
                
                console.log($(element).attr("id"));
                console.dir(data);
                if(data) {
                    $(element).val(data.map[$(element).attr("id")]);
                }
            });
        }, function(){
            l.stop();
        });
        

        
        
        
        
        return false;
        
    });
    
    $("#frmCad").on( "click", ".btnRemove", function(e) {
        e.preventDefault();
        var l = Ladda.create(this);
        l.start();
        
        var entidade = $(this).parents(".crud").attr('id');

        var indiceId = $(this).parents("table").find("th").index($(this).parents("table").find("#id"));

        var parametro = {};
        parametro["id"] = $(this).parents("tr").children("td:nth-child("+indiceId+")").val();        

        var url = "remove"+entidade.charAt(0).toUpperCase()+entidade.substr(1);        

        rxPost("/estacoes/server/"+url, parametro, function(data){
            iniciaForm();
        }, function(){
            l.stop();
        });

        return false;
    });
    
    
    $("#frmCad").on( "click", "#btnCrudSalvar", function(e) {
        e.preventDefault();
        var l = Ladda.create(this);
        l.start();
        
        var entidade = $(this).parents(".crud").attr('id');
        
        var parametro = {};
        $(this).parent().find("input,textarea,select").each(function(index, element){
            if($(element).attr("id").indexOf("_") > -1) {
                var arr = $(element).attr("id").split("_");
                parametro[arr[0]] = {id:$(element).find("*:selected").val(), nome:$(element).find("*:selected").html()};
            }
            else {
                parametro[$(element).attr("id")] = $(element).val();
            }
            
        });
        
        //console.dir(parametro);
        if(parametro.id === "") {
            var url = "add"+entidade.charAt(0).toUpperCase()+entidade.substr(1);
        }
        else {
            var url = "edit"+entidade.charAt(0).toUpperCase()+entidade.substr(1);
        }
        
        rxPost("/estacoes/server/"+url, parametro, function(data){
            $(".tabCrud a:first").tab('show');
            iniciaForm();
        }, function(){
            l.stop();
        });        
        
        
        return false;        
    });
    
    $("#frmCad").on( "click", "#btnCrudClose", function(e) {
        $(this).parent().find("input,textarea,select").each(function(index, element){
            $(element).val("");
        });
        
        $(".tabCrud a:first").tab('show');
    });
    
});

function iniciaForm() {
    wait.show();
    var entidade = $(".crud").attr("id");
    
    var url = "list"+entidade.charAt(0).toUpperCase()+entidade.substr(1);        
    
    
    $(".crud").find("form").find("*[source]").each(function(index, element){
        var arr = $(element).attr("source").split(".");

        var url = "list"+arr[0].charAt(0).toUpperCase()+arr[0].substr(1);        

        rxGet("/estacoes/server/"+url, null, function(data){
            $.each(data, function(rxindex, rxelement){
                console.log('<option value="'+rxelement.map[arr[1]]+'">'+rxelement.map[arr[2]]+'</option>');
                $(element).append('<option value="'+rxelement.map[arr[1]]+'">'+rxelement.map[arr[2]]+'</option>');
            });
        });

    });
    
    rxGet("/estacoes/server/"+url, null, function(data){
        
        var table = $(".crud").find("table");
        
        if(table.find("tbody").length <= 0) {
            table.append("<tbody>");
        }
        
        table.find("tbody").empty();
        
        $.each(data, function(index, element) {
            var tr = $("<tr>");
            table.find("th").each(function(thindex, thelement){
                if($(thelement).hasClass("edit")) {
                    tr.append(
                        $("<td>").append(
                            //<button type="button" class="btn btn-primary btn-sm">Small button</button>
                            $("<button>").attr("type", "button").addClass("btn btn-primary btn-sm btnEdit ladda-button").append(
                                //<span class="ladda-label">
                                $("<span>").addClass("ladda-label").append(
                                    //<span class="glyphicon glyphicon-star"></span>
                                    $("<span>").addClass("glyphicon glyphicon-pencil")
                                )
                            ).attr("data-style","zoom-in")  
                        ).attr("width", "10")
                    );
                }
                else if($(thelement).hasClass("remove")) {
                    tr.append(
                        $("<td>").append(
                            $("<button>").attr("type", "button").addClass("btn btn-danger btn-sm btnRemove ladda-button").append(
                                $("<span>").addClass("ladda-label").append(
                                    //<span class="glyphicon glyphicon-star"></span>
                                    $("<span>").addClass("glyphicon glyphicon-remove")
                                )
                            ).attr("data-style","zoom-in")
                        ).attr("width", "10")
                    );
                }
                else {
                    console.log($(thelement).attr("id")+": "+element.map[$(thelement).attr("id")]);
                    tr.append(
                        $("<td>").append(function(){
                            if($(thelement).attr("source")){
                                return [$("<input>").attr("type").val(element.map[$(thelement).attr("id")]),
                                        $(".crud").find("form").find('option[value="'+element.map[$(thelement).attr("id")]+'"]').html() ];
                            }
                            else {
                                return element.map[$(thelement).attr("id")];
                            }
                        })
                    );
                }

            });
            table.find("tbody").append(tr);
        });
        
        wait.hide();
        
    });
    
    
        
    
    
}