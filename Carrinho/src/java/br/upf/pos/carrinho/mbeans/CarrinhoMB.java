/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.carrinho.mbeans;

import br.upf.pos.carrinho.sfbeans.CarrinhoBean;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Felipe
 */
@ManagedBean
@SessionScoped
public class CarrinhoMB {

    @EJB
    private CarrinhoBean carrinhoBean;
    private String produto;
    
    /**
     * Creates a new instance of CarrinhoMB
     */
    public CarrinhoMB() {
    }
    
    public List<String> getProdutos() {
        return new ArrayList<String>(carrinhoBean.getProdutos());
    }
    
    public void adiciona() {
        carrinhoBean.adiciona(produto);
    }
    
    public void remove(String produto) {
        carrinhoBean.remove(produto);
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }
}
