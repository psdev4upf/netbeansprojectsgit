/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.carrinho.sfbeans;

import java.util.HashSet;
import java.util.Set;
import javax.ejb.Stateful;

/**
 *
 * @author Felipe
 */
@Stateful
public class CarrinhoBean {
    private Set<String> produtos = new HashSet<String>();
    
    public void adiciona(String produto) {
        produtos.add(produto);
    }
    
    public void remove(String produto) {
        produtos.remove(produto);
    }
    
    public Set<String> getProdutos() {
        return produtos;
    }
}
