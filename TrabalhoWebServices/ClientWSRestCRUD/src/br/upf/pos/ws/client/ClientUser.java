/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upf.pos.ws.client;

import br.upf.estacoes.model.beans.Estado;
import br.upf.estacoes.model.enums.SiglaEstadoEnum;
import java.util.List;
import javax.ws.rs.client.Entity;

/**
 *
 * @author benevenuti
 */
public class ClientUser {

    // utilizei o pacote genson (https://genson.googlecode.com/files/genson-0.98.jar)
    // este cara consegue mapear os json automaticamente :D

    public Estado getEstado(Long idEstado) {
        Client c = new Client();
        Estado e = (Estado) c.readEstado(Estado.class, idEstado.toString());
        c.close();
        return e;
    }

    public List<Estado> getEstados() {
        Client c = new Client();
        List<Estado> l = (List<Estado>) c.readEstados(List.class);
        c.close();
        return l;
    }

    public Boolean altertEstado() {
        Client c = new Client();
        Estado e = new Estado(1L, "Rio Grande do Sul - ALT", SiglaEstadoEnum.RS);
        Entity<Estado> ee;
        ee = new Entity<Estado>(Estado.class, e);
        
        ee.
                
        Boolean res = c.updateEstado(, Boolean.class);
        c.close();
        return res;
    }
}
