/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upf.pos.ws.client;

import org.junit.Test;

/**
 *
 * @author benevenuti
 */
public class Teste001 {

    public Teste001() {
    }

    @Test
    public void T001() {
        ClientUser user = new ClientUser();
        System.out.println(user.getEstado(2L));
    }

    @Test
    public void T002() {
        ClientUser user = new ClientUser();
        System.out.println(user.getEstados());
    }

    @Test
    public void T003() {
        ClientUser user = new ClientUser();
        System.out.println(user.altertEstado());
    }
}
