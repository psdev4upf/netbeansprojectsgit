
import br.upf.pos.ws.client.ConsomeWSPessoa;
import org.junit.Test;

/**
 *
 * @author benevenuti
 */
public class TesteClientWSPessoa {

    @Test
    public void T00_Inclui() {
        ConsomeWSPessoa sPessoa = new ConsomeWSPessoa();
        sPessoa.CriaPessoa();
    }

    @Test
    public void T01_Altera() {
        ConsomeWSPessoa sPessoa = new ConsomeWSPessoa();
        sPessoa.AlteraPessoa();
    }

    @Test
    public void T02_Exclui() {
        ConsomeWSPessoa sPessoa = new ConsomeWSPessoa();
        sPessoa.ExcluiPessoa();
    }

    @Test
    public void T03_Lista() {
        ConsomeWSPessoa sPessoa = new ConsomeWSPessoa();
        sPessoa.ListaPessoa();
    }    
    
}
