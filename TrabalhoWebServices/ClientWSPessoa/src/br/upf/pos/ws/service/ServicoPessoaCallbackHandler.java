
/**
 * ServicoPessoaCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package br.upf.pos.ws.service;

    /**
     *  ServicoPessoaCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ServicoPessoaCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ServicoPessoaCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ServicoPessoaCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for listar method
            * override this method for handling normal response from listar operation
            */
           public void receiveResultlistar(
                    br.upf.pos.ws.service.ServicoPessoaStub.ListarResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from listar operation
           */
            public void receiveErrorlistar(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for excluir method
            * override this method for handling normal response from excluir operation
            */
           public void receiveResultexcluir(
                    br.upf.pos.ws.service.ServicoPessoaStub.ExcluirResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from excluir operation
           */
            public void receiveErrorexcluir(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for alterar method
            * override this method for handling normal response from alterar operation
            */
           public void receiveResultalterar(
                    br.upf.pos.ws.service.ServicoPessoaStub.AlterarResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from alterar operation
           */
            public void receiveErroralterar(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for contar method
            * override this method for handling normal response from contar operation
            */
           public void receiveResultcontar(
                    br.upf.pos.ws.service.ServicoPessoaStub.ContarResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from contar operation
           */
            public void receiveErrorcontar(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for consultar method
            * override this method for handling normal response from consultar operation
            */
           public void receiveResultconsultar(
                    br.upf.pos.ws.service.ServicoPessoaStub.ConsultarResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from consultar operation
           */
            public void receiveErrorconsultar(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for incluir method
            * override this method for handling normal response from incluir operation
            */
           public void receiveResultincluir(
                    br.upf.pos.ws.service.ServicoPessoaStub.IncluirResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from incluir operation
           */
            public void receiveErrorincluir(java.lang.Exception e) {
            }
                


    }
    