/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upf.pos.ws.client;

import br.upf.pos.ws.service.ServicoPessoaStub;
import br.upf.pos.ws.service.ServicoPessoaStub.Alterar;
import br.upf.pos.ws.service.ServicoPessoaStub.AlterarResponse;
import br.upf.pos.ws.service.ServicoPessoaStub.Excluir;
import br.upf.pos.ws.service.ServicoPessoaStub.ExcluirResponse;
import br.upf.pos.ws.service.ServicoPessoaStub.Incluir;
import br.upf.pos.ws.service.ServicoPessoaStub.IncluirResponse;
import br.upf.pos.ws.service.ServicoPessoaStub.Listar;
import br.upf.pos.ws.service.ServicoPessoaStub.ListarResponse;
import br.upf.pos.ws.service.ServicoPessoaStub.Pessoa;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.axis2.AxisFault;

/**
 *
 * @author benevenuti
 */
public class ConsomeWSPessoa {

    ServicoPessoaStub stub;

    public ConsomeWSPessoa() {
        try {
            this.stub = new ServicoPessoaStub();
        } catch (AxisFault ex) {
            Logger.getLogger(ConsomeWSPessoa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void CriaPessoa() {
        Incluir in = new Incluir();
        Pessoa p = new Pessoa();
        p.setNome("Fulano Client One");
        p.setCodigo(1);
        p.setAltura(1.91f);
        in.setP(p);
        try {
            IncluirResponse response = stub.incluir(in);

            if (response.is_returnSpecified()) {
                if (response.get_return()) {
                    System.out.println("Incluiu pessoa com sucesso");
                }
            }
        } catch (RemoteException ex) {
            Logger.getLogger(ConsomeWSPessoa.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void AlteraPessoa() {
        Alterar a = new Alterar();
        a.setCodigo(1);
        
        Pessoa p = new Pessoa();
        p.setNome("Fulano Client One Modified");
        p.setCodigo(1);
        p.setAltura(1.61f);

        a.setP(p);
        
        try {
            AlterarResponse response;
            response = stub.alterar(a);
            
            if (response.is_returnSpecified()) {
                if (response.get_return()) {
                    System.out.println("Pessoa alterada com sucesso");
                }
            }
        } catch (RemoteException ex) {
            Logger.getLogger(ConsomeWSPessoa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void ExcluiPessoa(){
        Excluir e = new Excluir();
        Pessoa p = new Pessoa();
        
        p.setNome("Fulano Client One");
        p.setCodigo(1);
        p.setAltura(1.91f);
        
        e.setP(p);  
        
        try {            
            
            ExcluirResponse response = stub.excluir(e);

            if (response.is_returnSpecified()) {
                if (response.get_return()) {
                    System.out.println("Excluiu pessoa com sucesso");
                }
            }
        } catch (RemoteException ex) {
            Logger.getLogger(ConsomeWSPessoa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void ListaPessoa(){
        Listar l = new Listar();
        
        try {
            ListarResponse response =  stub.listar(l);
            if (response.is_returnSpecified()){
                Pessoa[] ps = response.get_return();
                
                for (Pessoa pessoa : ps) {
                    System.out.println("\n Código: " + pessoa.getCodigo() + "\n Nome: " + pessoa.getNome() + "\n Altura: " + pessoa.getAltura());
                }
            }
        } catch (RemoteException ex) {
            Logger.getLogger(ConsomeWSPessoa.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
