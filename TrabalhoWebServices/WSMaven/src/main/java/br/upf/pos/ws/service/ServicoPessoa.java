/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upf.pos.ws.service;

import br.upf.pos.ws.beans.Pessoa;
import java.util.ArrayList;
import java.util.List;
import org.apache.axis2.AxisFault;
/*
 *
 * @author benevenuti
 */

public class ServicoPessoa {

    private List<Pessoa> listagem = new ArrayList<>();

    public ServicoPessoa() {
        
    }

    public Boolean incluir(Pessoa p) throws AxisFault {

        if (contar().equals(4)) {
            throw new AxisFault("Falha ao adicionar pessoa. A lista já tem 4 pessoas.");
        }

        Boolean contem;

        try {
            contem = (consultar(p.getCodigo()) != null);
        } catch (AxisFault f) {
            contem = Boolean.FALSE;
        }

        if (!contem) {
            listagem.add(p);
            return Boolean.TRUE;
        } else {
            throw new AxisFault("Falha ao adicionar pessoa. O código já existe.");
        }
    }

    public Boolean alterar(Integer codigo, Pessoa p) throws AxisFault {
        // ao invés de gets e sets resolvemos simplesmente adicionar a nova pessoa
        // e remover a anterior, deixando a nova no mesmo lugar

        Pessoa o = consultar(codigo);
        int idx = listagem.indexOf(o);
        listagem.add(idx, p);
        excluir(o);
        return Boolean.TRUE;
    }

    public Pessoa consultar(Integer codigo) throws AxisFault {
        for (Pessoa pessoa : listagem) {
            if (pessoa.getCodigo().equals(codigo)) {
                return pessoa;
            }
        }
        throw new AxisFault("O código informado não é de nenhuma pessoa.");
    }

    public Boolean excluir(Pessoa p) throws AxisFault {
        Pessoa o = consultar(p.getCodigo());

        if (o != null) {
            listagem.remove(o);
            return Boolean.TRUE;
        } else {
            return Boolean.FALSE;
        }

    }

    public List<Pessoa> listar() {
        return listagem;
    }

    public Integer contar() {
        return listagem.size();
    }

}
