package br.upf.pos.ws.service;

import br.upf.pos.ws.beans.Pessoa;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.axis2.AxisFault;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author benevenuti
 */
public class ServicoPessoaTest {

    // mantem o list vivo para os testes
    ServicoPessoa instance = new ServicoPessoa();

    public ServicoPessoaTest() {

    }

    @Test
    public void testIncluir() {
        System.out.println("incluir");
        Pessoa p = new Pessoa();
        p.setCodigo(1);
        p.setNome("Fulano");
        p.setAltura(1.70f);

        Boolean expResult = Boolean.TRUE;
        Boolean result = null;
        try {
            result = instance.incluir(p);
            assertEquals(expResult, result);
        } catch (AxisFault ex) {
            Logger.getLogger(ServicoPessoaTest.class.getName()).log(Level.SEVERE, null, ex);
            assertNull(result);
        }

    }

    @Test
    public void testIncluir2x() {
        testIncluir();
        testIncluir();
    }

    @Test
    public void testInclui4() {
        System.out.println("incluir");
        Pessoa p1 = new Pessoa();
        p1.setCodigo(1);
        p1.setNome("Fulano One");
        p1.setAltura(1.71f);

        Pessoa p2 = new Pessoa();
        p2.setCodigo(2);
        p2.setNome("Fulano Two");
        p2.setAltura(1.72f);

        Pessoa p3 = new Pessoa();
        p3.setCodigo(3);
        p3.setNome("Fulano Three");
        p3.setAltura(1.73f);

        Pessoa p4 = new Pessoa();
        p4.setCodigo(4);
        p4.setNome("Fulano Four");
        p4.setAltura(1.74f);

        Boolean expResult = Boolean.TRUE;
        Boolean result = null;
        try {
            result = instance.incluir(p1);

            assertEquals(expResult, result);

            result = instance.incluir(p2);
            assertEquals(expResult, result);

            result = instance.incluir(p3);
            assertEquals(expResult, result);

            result = instance.incluir(p4);
            assertEquals(expResult, result);
        } catch (AxisFault ex) {
            Logger.getLogger(ServicoPessoaTest.class.getName()).log(Level.SEVERE, null, ex);
            assertEquals(expResult, result);
        }
    }

    @Test
    public void testIncluirPlus4() {

        testInclui4();

        Pessoa p5 = new Pessoa();
        p5.setCodigo(5);
        p5.setNome("Fulano Five");
        p5.setAltura(1.75f);

        Boolean expResult = Boolean.TRUE;
        // erro, tem 4 já
        Boolean result = null;
        try {
            result = instance.incluir(p5);
            assertEquals(expResult, result);
        } catch (AxisFault ex) {
            Logger.getLogger(ServicoPessoaTest.class.getName()).log(Level.SEVERE, null, ex);
            assertNull(result);
        }

    }

    @Test
    public void testAlterar() {
        // insere a pessoa
        testIncluir();

        System.out.println("alterar");
        Integer codigo = 1;
        Pessoa p = new Pessoa();
        p.setCodigo(1);
        p.setNome("Fulano One");
        p.setAltura(1.70f);

        Boolean expResult = Boolean.TRUE;
        Boolean result = null;
        try {
            result = instance.alterar(codigo, p);
            assertEquals(expResult, result);
        } catch (AxisFault ex) {
            Logger.getLogger(ServicoPessoaTest.class.getName()).log(Level.SEVERE, null, ex);
            assertNull(result);
        }

    }

    @Test
    public void testAlterarInexistente() {
        // insere a pessoa
        testIncluir();

        System.out.println("alterar");
        // código errado
        Integer codigo = 9;
        Pessoa p = new Pessoa();
        p.setCodigo(1);
        p.setNome("Fulano One");
        p.setAltura(1.70f);

        Boolean expResult = Boolean.TRUE;
        Boolean result = null;
        try {
            result = instance.alterar(codigo, p);
            assertEquals(expResult, result);
        } catch (AxisFault ex) {
            Logger.getLogger(ServicoPessoaTest.class.getName()).log(Level.SEVERE, null, ex);
            assertNull(result);
        }

    }

    /**
     * Test of consultar method, of class ServicoPessoa.
     */
    @Test
    public void testConsultar() {
        // insere a pessoa
        testIncluir();

        System.out.println("consultar");
        Integer codigo = 1;

        Pessoa p = new Pessoa();
        p.setCodigo(1);
        p.setNome("Fulano");
        p.setAltura(1.70f);

        Pessoa expResult = p;
        Pessoa result = null;
        try {
            result = instance.consultar(codigo);
            assertEquals(expResult, result);
        } catch (AxisFault ex) {
            Logger.getLogger(ServicoPessoaTest.class.getName()).log(Level.SEVERE, null, ex);
            assertNull(result);
        }

    }

    @Test
    public void testConsultarInexistente() {
        // insere a pessoa
        testIncluir();

        System.out.println("consultar");
        // código inexistente
        Integer codigo = 77;

        Pessoa p = new Pessoa();
        p.setCodigo(1);
        p.setNome("Fulano");
        p.setAltura(1.70f);

        Pessoa expResult = p;
        Pessoa result = null;
        try {
            result = instance.consultar(codigo);
            assertEquals(expResult, result);
        } catch (AxisFault ex) {
            Logger.getLogger(ServicoPessoaTest.class.getName()).log(Level.SEVERE, null, ex);
            assertNull(result);
        }

    }

    /**
     * Test of excluir method, of class ServicoPessoa.
     */
    @Test
    public void testExcluir() {
        // incluir
        testIncluir();
        System.out.println("excluir");
        Pessoa p = new Pessoa();
        p.setCodigo(1);
        p.setNome("Fulano");
        p.setAltura(1.70f);

        Boolean expResult = Boolean.TRUE;
        Boolean result = null;
        try {
            result = instance.excluir(p);
            assertEquals(expResult, result);
        } catch (AxisFault ex) {
            Logger.getLogger(ServicoPessoaTest.class.getName()).log(Level.SEVERE, null, ex);
            assertNull(result);
        }

    }

    @Test
    public void testExcluirInexistente() {
        // incluir
        testIncluir();
        System.out.println("excluir");
        Pessoa p = new Pessoa();
        p.setCodigo(23);
        p.setNome("Fulano Twenty Three");
        p.setAltura(1.70f);

        Boolean expResult = Boolean.FALSE;
        Boolean result = null;
        try {
            result = instance.excluir(p);
            assertEquals(expResult, result);
        } catch (AxisFault ex) {
            Logger.getLogger(ServicoPessoaTest.class.getName()).log(Level.SEVERE, null, ex);
            assertNull(result);
        }

    }

    /**
     * Test of listar method, of class ServicoPessoa.
     */
    @Test
    public void testListar() {
        System.out.println("listar");
        try {
            testInclui4();
        } catch (Exception ex) {
            Logger.getLogger(ServicoPessoaTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        List<Pessoa> result = instance.listar();
        for (Pessoa pessoa : result) {
            System.out.println(pessoa);
        }
    }

    /**
     * Test of contar method, of class ServicoPessoa.
     */
    @Test
    public void testContar() {
        System.out.println("contar");
        try {
            testInclui4();
        } catch (Exception ex) {
            Logger.getLogger(ServicoPessoaTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        Integer expResult = 4;
        Integer result = instance.contar();
        assertEquals(expResult, result);
    }

}
