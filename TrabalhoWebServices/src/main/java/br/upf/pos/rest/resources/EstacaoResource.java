/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.rest.resources;

import br.upf.estacoes.model.beans.Estacao;
import br.upf.estacoes.model.services.GenericJpaServiceImpl;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;


/**
 *
 * @author Felipe
 */
@Path("estacaoRS")
public class EstacaoResource extends RestResource<Estacao> {
    
    @GET
    @Path("list")
    @Produces({"application/json"})
    public void list() throws Exception {
        //entidades = genericService.findObjetos(Boolean.TRUE);
    }
}
