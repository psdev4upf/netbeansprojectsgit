/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.rest.resources;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Variant;

/**
 *
 * @author Felipe
 */
public class RestResource<T> {
    final static List<Variant> supportedVariants =
                                    Variant.VariantListBuilder.newInstance().mediaTypes(MediaType.APPLICATION_XML_TYPE, MediaType.APPLICATION_JSON_TYPE).add().build();
    
    List<T> entidades = new ArrayList();
    
    @Context Request request;

    @GET
    public Response content() {
        if (entidades.isEmpty()) {
            return Response.noContent().build();
        }

        final Variant variant = request.selectVariant(supportedVariants);

        if (variant == null) {
            return Response.notAcceptable(supportedVariants).build();
        } else {
            return Response.ok(entidades, variant.getMediaType()).build();
        }
    }
}
