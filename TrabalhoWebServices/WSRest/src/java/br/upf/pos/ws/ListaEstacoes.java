/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.ws;

import br.upf.estacoes.model.beans.Estacao;
import br.upf.pos.ws.wrapper.PessoaWrapper;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

/**
 * REST Web Service
 *
 * @author benevenuti
 */
@Path("estacoes")
public class ListaEstacoes {

    @Context
    private UriInfo context;
   
    
    @Context Request request;

    /**
     * Creates a new instance of ListaEstacoes
     */
    public ListaEstacoes() {
    }

    /**
     * Retrieves representation of an instance of br.upf.pos.ws.ListaEstacoes
     * @return an instance of java.lang.String
     */
    @GET
    @Path("json/{idCidade}")
    @Produces(value = "application/json")
    public List<Estacao> getEstacoesPorCidade(@PathParam("idCidade") Long idCidade) {
        
        PessoaWrapper pw = new PessoaWrapper();
        
        List<Estacao> estacoes;
        
        estacoes = pw.findEstacoesCidade(idCidade);                
        
        return estacoes;
        
    }
    
    @GET
    @Path("xml/{idCidade}")
    @Produces(value = "application/xml")
    public List<Estacao> getEstacoesPorCidadeXml(@PathParam("idCidade") Long idCidade) {
        
        PessoaWrapper pw = new PessoaWrapper();
        
        List<Estacao> estacoes;
        
        estacoes = pw.findEstacoesCidade(idCidade);                
        
        return estacoes;
        
    }

}
