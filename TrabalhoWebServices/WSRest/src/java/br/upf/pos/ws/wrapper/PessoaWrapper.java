/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upf.pos.ws.wrapper;

import br.upf.estacoes.model.beans.Estacao;
import br.upf.estacoes.model.logic.EstacaoLogicImpl;
import br.upf.estacoes.model.logic.GenericLogicImpl;
import br.upf.estacoes.model.services.GenericJpaServiceImpl;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author benevenuti
 */
public class PessoaWrapper {

    public List<Estacao> findEstacoesCidade(Long idCidade) {

        EntityManager em;
        
        //GenericJpaServiceImpl<Estacao> dao;
        //dao = new GenericJpaServiceImpl(new GenericLogicImpl(), Estacao.class);
        
        em = Persistence.createEntityManagerFactory("PosWebPU").createEntityManager();

        Query q = em.createQuery("select o from Estacao o where o.cidade.id = :p0");

        q.setParameter("p0", idCidade);

        List<Estacao> list = q.getResultList();

        em.close();

        return list;

    }

}
