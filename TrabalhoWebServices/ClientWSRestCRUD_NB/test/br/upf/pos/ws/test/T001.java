/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upf.pos.ws.test;

import br.upf.estacoes.model.beans.Estado;
import br.upf.estacoes.model.enums.SiglaEstadoEnum;
import br.upf.pos.ws.client.ClientUser;
import org.junit.Test;

/**
 *
 * @author benevenuti
 */
public class T001 {

    public T001() {
    }

    @Test
    public void T01() {
        ClientUser user = new ClientUser();
        System.out.println(user.getEstado(1L));
    }

    @Test
    public void T02() {
        ClientUser user = new ClientUser();
        System.out.println(user.getEstados());
    }

    @Test
    public void T03() {
        ClientUser user = new ClientUser();
        Estado e = new Estado(3L, "Paraná", SiglaEstadoEnum.PR);
        user.addEstado(e);
    }

    @Test
    public void T04() {
        ClientUser user = new ClientUser();
        user.remEstado(3L);
    }

    @Test
    public void T05() {
        ClientUser user = new ClientUser();
        Estado e = new Estado(3L, "Paraná - ALTERADO", SiglaEstadoEnum.PR);
        user.alterEstado(e, 3L);
    }

}
