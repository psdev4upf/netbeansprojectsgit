/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upf.pos.ws.client;

import br.upf.estacoes.model.beans.Estado;
import java.util.List;

/**
 *
 * @author benevenuti
 */
public class ClientUser {

    public Estado getEstado(Long id) {
        Client c = new Client();
        Estado e = c.find_JSON(Estado.class, id.toString());
        return e;
    }

    public List<Estado> getEstados() {
        Client c = new Client();
        List<Estado> e = c.findAll_JSON(List.class);
        return e;
    }

    public void addEstado(Estado e) {
        Client c = new Client();
        c.create_JSON(e);
    }

    public void remEstado(Long id) {
        Client c = new Client();
        c.remove(id.toString());
    }

    public void alterEstado(Estado e, Long id) {
        Client c = new Client();
        c.edit_JSON(e, id.toString());
    }

}
