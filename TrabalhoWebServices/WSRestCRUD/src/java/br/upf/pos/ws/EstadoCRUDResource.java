/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upf.pos.ws;

import br.upf.estacoes.model.beans.Estado;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import static javax.ws.rs.HttpMethod.POST;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import javax.xml.bind.JAXBElement;

/**
 * REST Web Service
 *
 * @author benevenuti
 */
@Path("EstadoCRUD")
public class EstadoCRUDResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of EstadoCRUDResource
     */
    public EstadoCRUDResource() {
    }

    /**
     * Retrieves representation of an instance of
     * br.upf.pos.ws.EstadoCRUDResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Path("read/{idEstado}")
    @Produces(MediaType.APPLICATION_XML)
    public Estado readEstado(@PathParam("idEstado") Long idEstado) {
        return EstadoWrapper.findEstado(idEstado);
    }

    @GET
    @Path("readAll")
    @Produces(MediaType.APPLICATION_XML)
    public List<Estado> readEstados() {
        return EstadoWrapper.findEstados();
    }
    
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_XML)
    public Boolean createEstado(JAXBElement<Estado> estado) {
        Boolean res = Boolean.TRUE;
        try {
            EstadoWrapper.addEstado(estado.getValue());
        } catch (Exception ex) {
            Logger.getLogger(EstadoCRUDResource.class.getName()).log(Level.SEVERE, null, ex);
            res = Boolean.FALSE;
        }
        return res;
    }

    @PUT
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_XML)
    public Boolean updateEstado(@PathParam("idEstado") Long idEstado, @PathParam("estado") Estado estado) {
        Boolean res = Boolean.TRUE;
        try {
            EstadoWrapper.alterEstado(idEstado, estado);
        } catch (Exception ex) {
            Logger.getLogger(EstadoCRUDResource.class.getName()).log(Level.SEVERE, null, ex);
            res = Boolean.FALSE;
        }
        return res;
    }

    @DELETE    
    @Consumes(MediaType.APPLICATION_XML)
    public void deleteEstado(@PathParam("idEstado") Long idEstado) {
        Boolean res = Boolean.TRUE;
        try {
            EstadoWrapper.remEstado(idEstado);
        } catch (Exception ex) {
            Logger.getLogger(EstadoCRUDResource.class.getName()).log(Level.SEVERE, null, ex);
            res = Boolean.FALSE;
        }

    }

}
