package br.upf.pos.ws;

import br.upf.estacoes.model.beans.Estado;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author benevenuti
 */
public class EstadoWrapper {
    
    public static Estado findEstado(Long idEstado) {
        
        EntityManager em;
        
        em = Persistence.createEntityManagerFactory("PosWebPU").createEntityManager();
        
        Query q = em.createQuery("select o from Estado o where o.id = :p0");
        
        q.setParameter("p0", idEstado);
        
        Estado e = (Estado) q.getSingleResult();
        
        em.close();
        
        return e;
        
    }
    
    public static List<Estado> findEstados() {
        
        EntityManager em;
        
        em = Persistence.createEntityManagerFactory("PosWebPU").createEntityManager();
        
        Query q = em.createQuery("select o from Estado o ORDER BY o.nome");
        
        List<Estado> l = q.getResultList();
        
        em.close();
        
        return l;
        
    }
    
    public static void addEstado(Estado e) throws Exception {
        try {
            if (e.getId() != null)  {
                throw new Exception("O código do estado deve ser criado automaticamente. Não informar este campo.");                
            }
            EntityManager em;
            em = Persistence.createEntityManagerFactory("PosWebPU").createEntityManager();
            em.getTransaction().begin();
            em.persist(e);
            em.getTransaction().commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Não foi possível adicionar o Estado." + "\n" + ex.getLocalizedMessage());
        }
        
    }
    
    public static void remEstado(Long idEstado) throws Exception {
        try {
            Estado e = findEstado(idEstado);
            
            if (e == null) {
                throw new Exception("O código indicado não pertence a nenhum Estado.");
            } else {                
                EntityManager em;
                em = Persistence.createEntityManagerFactory("PosWebPU").createEntityManager();
                em.getTransaction().begin();
                em.remove(e);
                em.getTransaction().commit();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Não foi possível remover o Estado." + "\n" + ex.getLocalizedMessage());
        }
        
    }
    
    public static void alterEstado(Long idEstado, Estado eNovo) throws Exception {
        try {
            Estado e = findEstado(idEstado);
            
            if (e == null) {
                throw new Exception("O código indicado não pertence a nenhum Estado.");
            } else {                
                EntityManager em;
                em = Persistence.createEntityManagerFactory("PosWebPU").createEntityManager();
                em.getTransaction().begin();
                // id não troca
                // e.setId(eNovo.getId());
                e.setNome(eNovo.getNome());
                e.setSigla(eNovo.getSigla());
                
                em.merge(e);
                em.getTransaction().commit();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception("Não foi possível remover o Estado." + "\n" + ex.getLocalizedMessage());
        }
        
    }
    
}
