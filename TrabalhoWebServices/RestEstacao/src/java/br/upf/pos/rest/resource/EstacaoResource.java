/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.rest.resource;

import br.upf.estacoes.model.beans.Estacao;
import br.upf.estacoes.model.beans.Estado;
import br.upf.estacoes.model.logic.EstacaoLogicImpl;
import br.upf.estacoes.model.logic.EstadoLogicImpl;
import br.upf.estacoes.model.services.GenericJpaServiceImpl;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Variant;
import org.glassfish.jersey.jackson.JacksonFeature;
/**
 * REST Web Service
 *
 * @author fefurst
 */
@Path("estacao")
public class EstacaoResource {

    
    @Context
    private UriInfo context;
    
    @Context Request request;

    /**
     * Creates a new instance of ServiceResource
     */
    public EstacaoResource() {
        
    }

    GenericJpaServiceImpl<Estacao> dao = new GenericJpaServiceImpl(
                         new EstacaoLogicImpl<Estacao>(), Estacao.class); 
       
    @GET
    @Path("estacoes/{cdcidade}")
    @Produces("application/json")
    public List<Estacao> listarEstacoes(@PathParam("cdcidade") Long cdcidade) throws Exception{        
        
        List<Estacao> estacoes = dao.findObjetosFiltroCidade(false, cdcidade);
        
        return estacoes;
    }
    
    
    @GET
    @Path("estacoestxt/{cdcidade}")
    @Produces("text/plain")
    public String listarEstacoesTxt(@PathParam("cdcidade") Long cdcidade) throws Exception{        
        
        List<Estacao> estacoes = dao.findObjetosFiltroCidade(false, cdcidade);
        
        return estacoes.toString();
    }
}
