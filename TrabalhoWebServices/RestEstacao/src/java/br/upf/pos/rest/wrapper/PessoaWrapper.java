package br.upf.pos.rest.wrapper;

import br.upf.estacoes.model.beans.Estacao;
import br.upf.estacoes.model.util.FactorySingleton;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author benevenuti
 */
public class PessoaWrapper {

    public PessoaWrapper() {
    }
   
    public List<Estacao> findEstacoesCidade(Long idCidade) {
        
        EntityManager em;

        em = FactorySingleton.getInstance().getEntityManager();

        Query q = em.createQuery("select o from Estacao o where o.cidade.id = :p0");

        q.setParameter("p0", idCidade);

        List<Estacao> list = q.getResultList();

        em.close();

        return list;

    }

}
