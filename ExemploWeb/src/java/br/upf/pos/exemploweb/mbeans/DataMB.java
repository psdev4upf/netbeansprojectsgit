/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.exemploweb.mbeans;

import java.util.Calendar;
import java.util.Date;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Felipe
 */
@ManagedBean
@RequestScoped
public class DataMB {
    private Date data;
    
    /**
     * Creates a new instance of DataDB
     */
    public DataMB() {
        data = Calendar.getInstance().getTime();
    }
    
    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
    
}
