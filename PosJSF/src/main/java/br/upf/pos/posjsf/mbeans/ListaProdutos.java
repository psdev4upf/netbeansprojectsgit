/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upf.pos.posjsf.mbeans;

import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

/**
 *
 * @author pavan
 */
@ManagedBean
@SessionScoped
public class ListaProdutos {

    private String produto;
    private List<String> produtos;
    private HtmlInputText inputProduto;
    private HtmlCommandButton botaoAdicionar;
    private boolean renderizar = false;

    /**
     * Creates a new instance of ListaProdutos
     */
    public ListaProdutos() {
        produtos = new ArrayList<>();
    }

    public void adicionar() {
        FacesContext fc = FacesContext.getCurrentInstance();
        FacesMessage fm = null;
        if (produtos.contains(produto)) {
            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Produto já inserido", "");
        } else if (produtos.size() > 3) {
            inputProduto.setDisabled(true);
            botaoAdicionar.setDisabled(true);
            botaoAdicionar.setValue("Muitos Produtos Adicionados");
        } else {
            produtos.add(produto);
            fm = new FacesMessage(
                    FacesMessage.SEVERITY_INFO,
                    "Produto inserido com sucesso", "");
        }
        if(fm!=null) fc.addMessage(null, fm);
    }

    public String adicionar2() {
        produtos.add(produto);
        if (produtos.size() > 2) {
            return "cheia?faces-redirect=true";
        }
        return null;
    }

    public boolean isRenderizar() {
        return renderizar;
    }

    public void setRenderizar(boolean renderizar) {
        this.renderizar = renderizar;
    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public List<String> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<String> produtos) {
        this.produtos = produtos;
    }

    public HtmlInputText getInputProduto() {
        return inputProduto;
    }

    public void setInputProduto(HtmlInputText inputProduto) {
        this.inputProduto = inputProduto;
    }

    public HtmlCommandButton getBotaoAdicionar() {
        return botaoAdicionar;
    }

    public void setBotaoAdicionar(HtmlCommandButton botaoAdicionar) {
        this.botaoAdicionar = botaoAdicionar;
    }

}
