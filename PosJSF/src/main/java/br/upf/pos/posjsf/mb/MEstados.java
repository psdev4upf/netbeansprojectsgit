/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.upf.pos.posjsf.mb;

import br.upf.estacoes.model.beans.Estado;
import br.upf.estacoes.model.enumerator.SiglaEstadoEnum;
import br.upf.estacoes.model.logic.EstadoLogicImpl;
import br.upf.estacoes.model.services.GenericJpaServiceImpl;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author Filipe
 */
@ManagedBean
@SessionScoped
public class MEstados {

    private Estado estado;
    private List<Estado> estados;
    private GenericJpaServiceImpl<Estado> dao;

    /**
     * Creates a new instance of MEstados
     */
    public MEstados() {
        dao = new GenericJpaServiceImpl(new EstadoLogicImpl<Estado>(), Estado.class);
        estado = new Estado();
    }

    public SiglaEstadoEnum[] getSiglas() {
        return SiglaEstadoEnum.values();
    }

    public void adiciona() {
        try {
            dao.persist(estado);
            estado = new Estado();
        } catch (Exception ex) {
            Logger.getLogger(MEstados.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public List<Estado> getEstados() {
        try {
            estados = dao.findObjetos(Boolean.TRUE);
        } catch (Exception ex) {
            Logger.getLogger(MEstados.class.getName()).log(Level.SEVERE, null, ex);
        }
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

}
