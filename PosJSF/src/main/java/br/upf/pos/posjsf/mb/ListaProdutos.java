package br.upf.pos.posjsf.mb;

import com.sun.faces.el.FacesCompositeELResolver;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.html.HtmlCommandButton;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;

/**
 *
 * @author Filipe
 */
@ManagedBean
@SessionScoped
public class ListaProdutos {

    private String produto;
    private List<String> listProd;
    private HtmlInputText inputProdutos;
    private HtmlCommandButton btnAdd;
    private Boolean render = Boolean.TRUE;

    public ListaProdutos() {
        this.listProd = new ArrayList<>();
    }

    public void adicionar() {
        FacesContext fc = FacesContext.getCurrentInstance();
        FacesMessage fm;
        if (listProd.contains(produto)) {
            fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Produto existente", "O produto " + produto + " já está na lista");
        } else {

            listProd.add(produto);

            if (listProd.size() > 5) {
                btnAdd.setDisabled(true);
                btnAdd.setValue("Muitos produtos adicionados...");
            }

            fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Produto inserido com sucesso", "O produto " + produto + " foi adicionado na lista");
        }
        fc.addMessage(produto, fm);
    }

    public String adicionar2() {

        listProd.add(produto);

        if (listProd.size() > 2) {
            return "cheia?faces-redirect=true";
        } else {
            return null;
        }

    }

    public String getProduto() {
        return produto;
    }

    public void setProduto(String produto) {
        this.produto = produto;
    }

    public HtmlInputText getInputProdutos() {
        return inputProdutos;
    }

    public void setInputProdutos(HtmlInputText inputProdutos) {
        this.inputProdutos = inputProdutos;
    }

    public HtmlCommandButton getBtnAdd() {
        return btnAdd;
    }

    public void setBtnAdd(HtmlCommandButton btnAdd) {
        this.btnAdd = btnAdd;
    }

    public List<String> getListProd() {
        return listProd;
    }

    public void setListProd(List<String> listProd) {
        this.listProd = listProd;
    }

    public Boolean getRender() {
        return render;
    }

    public void setRender(Boolean render) {
        this.render = render;
    }

}
