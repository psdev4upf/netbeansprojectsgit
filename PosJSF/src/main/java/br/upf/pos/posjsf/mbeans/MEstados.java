/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.posjsf.mbeans;

import br.upf.estacoes.model.beans.Estado;
import br.upf.estacoes.model.enumerator.SiglaEstadoEnum;
import br.upf.estacoes.model.logic.EstadoLogicImpl;
import br.upf.estacoes.model.services.GenericJpaServiceImpl;
import br.upf.estacoes.model.util.FactorySingleton;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

/**
 *
 * @author pavan
 */
@ManagedBean
@SessionScoped
public class MEstados {
    private List<Estado> estados;
    private Estado estado;
    private GenericJpaServiceImpl<Estado> dao;
    public MEstados() {
        dao = new GenericJpaServiceImpl(
                new EstadoLogicImpl<Estado>(),Estado.class);
        estado = new Estado();
    }
    public String salvar() {
        try {
            dao.merge(estado);
            estado = new Estado();
            return "listEstado?faces-redirect=true";
        } catch (Exception ex) {
            FacesMessage fm = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, 
                    "Erro ao salvar", ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, fm);

            Logger.getLogger(MEstados.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public String cancelar() {
        return "listEstado?faces-redirect=true";
    }
    public String novo() {
        estado = new Estado();
        return "formEstado?faces-redirect=true";
    }
    public String editar(Estado e) {
        estado = e;
        return "formEstado?faces-redirect=true";
    }
    public void excluir(Estado e) {
        try {
            EntityManager em = FactorySingleton.getInstance().getEntityManager();
            e = em.find(Estado.class, e.getId());            
            em.getTransaction().begin();
            em.remove(e);
            em.getTransaction().commit();            
        } catch (Exception ex) {
            FacesMessage fm = new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, 
                    "Erro ao excluir estado.", ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, fm);
            Logger.getLogger(MEstados.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public SiglaEstadoEnum[] getSiglas() {
        return SiglaEstadoEnum.values();
    }
    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public List<Estado> getEstados() {
        try {
            estados = dao.findObjetos(Boolean.TRUE);
        } catch (Exception ex) {
            Logger.getLogger(MEstados.class.getName()).log(Level.SEVERE, null, ex);
        }
        return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }
    
    
}
