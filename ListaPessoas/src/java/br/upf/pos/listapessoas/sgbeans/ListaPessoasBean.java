/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.listapessoas.sgbeans;

import br.upf.pos.listapessoas.modelo.Pessoa;
import java.util.HashSet;
import java.util.Set;
import javax.ejb.Singleton;

/**
 *
 * @author Felipe
 */
@Singleton
public class ListaPessoasBean {
    private final Set<Pessoa> pessoas = new HashSet<>();
    private Integer crianca=0, adolecente=0, adulto=0;
    
    public void adiciona(String nome, Integer idade) {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome(nome);
        pessoa.setIdade(idade);
        pessoas.add(pessoa);
        
        if(idade<13) {
            crianca++;
        }
        else if(idade >=13 && idade <= 19) {
            adolecente++;
        }
        else if(idade >19) {
            adulto++;
        }
        
    }
    
    public void remove(Pessoa pessoa) {
        
        if(pessoa.getIdade()<13) {
            crianca--;
        }
        else if(pessoa.getIdade() >=13 && pessoa.getIdade() <= 19) {
            adolecente--;
        }
        else if(pessoa.getIdade() >19) {
            adulto--;
        }
        
        pessoas.remove(pessoa);
    }
    
    public Set<Pessoa> getPessoas() {
        return pessoas;
    }

    public Integer getCrianca() {
        return crianca;
    }

    public Integer getAdolecente() {
        return adolecente;
    }

    public Integer getAdulto() {
        return adulto;
    }   
}
