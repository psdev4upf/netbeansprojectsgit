/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.listapessoas.mbeans;

import br.upf.pos.listapessoas.modelo.Pessoa;
import br.upf.pos.listapessoas.sgbeans.ListaPessoasBean;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Felipe
 */
@ManagedBean
@RequestScoped
public class ListaPessoasMB {

    @EJB
    private ListaPessoasBean listaPessoasBean;
    private String nome;
    private Integer idade;
    
    /**
     * Creates a new instance of ListaPessoasMB
     */
    public ListaPessoasMB() {
    }
    
    public List<Pessoa> getPessoas() {
        return new ArrayList<>(listaPessoasBean.getPessoas());
    }
    
    public void adiciona() {
        listaPessoasBean.adiciona(nome, idade);
    }
    
    public void remove(Pessoa pessoa) {
        listaPessoasBean.remove(pessoa);
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public int getQtdCriancas() {
        return listaPessoasBean.getCrianca();
    }

    public int getQtdAdolecentes() {
        return listaPessoasBean.getAdolecente();
    }

    public int getQtdAdultos() {
        return listaPessoasBean.getAdulto();
    } 
    
    
    
}
