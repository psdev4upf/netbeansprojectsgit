/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.calculadoraejb.mbeans;

import br.upf.pos.calculadoraejb.slbeans.Calculadora;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Felipe
 */
@ManagedBean
@RequestScoped
public class CalculadoraMB {

    @EJB
    private Calculadora calculadora;    
    private double a, b, resultado;
    
    
    /**
     * Creates a new instance of CalculadoraMB
     */
    public CalculadoraMB() {
    }

    public void soma() {
        resultado = calculadora.soma(a, b);
    }
    
    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }
    
    
    
}
