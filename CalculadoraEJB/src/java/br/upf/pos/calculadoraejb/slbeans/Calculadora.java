/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package br.upf.pos.calculadoraejb.slbeans;

import javax.ejb.Remote;
import javax.ejb.Stateless;

/**
 * Classe Calculadora (EJB Stateless)
 * @author Felipe
 */
@Stateless
public class Calculadora {
    /**
     * Método para calcular a soma de vois valores do tipo double
     * @param a primeiro valor double
     * @param b segundo valor double
     * @return retorna a soma dos dois valores
     */
    public double soma(double a, double b) {
        return a + b;
    }
}
